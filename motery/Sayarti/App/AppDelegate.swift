//
//  AppDelegate.swift
//  Sayarti
//
//  Created by Mina Malak on 2/28/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import CoreData
import Localize_Swift
import IQKeyboardManagerSwift
import OneSignal
import Firebase


//com.vavisa.motery.OneSignalNotificationServiceExtension

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, OSSubscriptionObserver {

    var window: UIWindow?
    var strPlayerId = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        sleep(3)
        self.configOneSignal(launchOptions: launchOptions)
        self.configKeyboard()
        self.configureLandingView()
        FirebaseApp.configure()
        OneSignal.add(self as OSSubscriptionObserver)
        
        
        return true
    }

//MARK:- ################### CONFIGURE APPLICATION BASE ###################

    private func configOneSignal(launchOptions: [UIApplication.LaunchOptionsKey: Any]?){
        //START OneSignal initialization code
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]

        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: Constants.ONE_SIGNAL_KEY, handleNotificationAction: nil, settings: onesignalInitSettings)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;

        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
    }
    
    // Add this new method
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
       if !stateChanges.from.subscribed && stateChanges.to.subscribed {
          print("Subscribed for OneSignal push notifications!")
          // get player ID
        self.strPlayerId = stateChanges.to.userId ?? ""
        print("Player_id" , stateChanges.to.userId ?? "")
       }
        print("SubscriptionStateChange: \n\(stateChanges!)")
    }
    
    private func configKeyboard(){
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    func configureLandingView(){
        
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        self.strPlayerId = status.subscriptionStatus.userId ?? ""
        print("Player_id = \(self.strPlayerId)")
        
        window?.backgroundColor = UIColor(red: 250, green: 250, blue: 250, alpha: 1.0)
        UINavigationBar.appearance().shadowImage = UIImage()
        //        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        let backImage = UIImage(named: "BackButton")
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        UITabBar.appearance().layer.borderWidth = 0.0
        UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().unselectedItemTintColor = UIColor.white
        
        if (Constants.bIsAppLaunched() == false) {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let nav = storyboard.instantiateViewController(withIdentifier: "selectLanguageNavigationBar") as! UINavigationController
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "selectLanguageViewController")
            nav.viewControllers = [initialViewController]
            self.window?.rootViewController = nav
            self.window?.makeKeyAndVisible()
            Constants.setValueInUserDefault(value: true, key: Constants.KEY_APP_LAUNCH)
        }
        
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = .white
            self.window?.rootViewController?.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.window?.rootViewController?.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
            window?.overrideUserInterfaceStyle = .light
        }
    
        self.handleUserInterfaceForLanguage()
    }
    
    func configureViewAfterLogout(){
        
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        self.strPlayerId = status.subscriptionStatus.userId ?? ""
        print("Player_id = \(self.strPlayerId)")
        
        window?.backgroundColor = UIColor(red: 250, green: 250, blue: 250, alpha: 1.0)
        UINavigationBar.appearance().shadowImage = UIImage()
        //        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: .default)
        let backImage = UIImage(named: "BackButton")
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        UITabBar.appearance().layer.borderWidth = 0.0
        UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().unselectedItemTintColor = UIColor.white
        
       
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let nav = storyboard.instantiateViewController(withIdentifier: "selectLanguageNavigationBar") as! UINavigationController
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "selectLanguageViewController")
        nav.viewControllers = [initialViewController]
        self.window?.rootViewController = nav
        self.window?.makeKeyAndVisible()
        Constants.setValueInUserDefault(value: true, key: Constants.KEY_APP_LAUNCH)
        
        
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = .white
            self.window?.rootViewController?.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.window?.rootViewController?.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
            window?.overrideUserInterfaceStyle = .light
        }
    
        self.handleUserInterfaceForLanguage()
    }
    
    func handleUserInterfaceForLanguage(){
        if (Localize.currentLanguage() == "en"){
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
            UICollectionView.appearance().semanticContentAttribute = .forceLeftToRight
            UITextField.appearance().semanticContentAttribute = .forceLeftToRight
            UITextView.appearance().semanticContentAttribute = .forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute = .forceLeftToRight
            UITabBar.appearance().semanticContentAttribute = .forceLeftToRight
            UITableView.appearance().semanticContentAttribute = .forceLeftToRight
            UILabel.appearance().semanticContentAttribute = .forceLeftToRight
            UISegmentedControl.appearance().semanticContentAttribute = .forceLeftToRight
            UISearchBar.appearance().semanticContentAttribute = .forceLeftToRight
        }
        else {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute = .forceRightToLeft
            UITextField.appearance().semanticContentAttribute = .forceRightToLeft
            UITabBar.appearance().semanticContentAttribute = .forceRightToLeft
            UITableView.appearance().semanticContentAttribute = .forceRightToLeft
            UILabel.appearance().semanticContentAttribute = .forceRightToLeft
            UISegmentedControl.appearance().semanticContentAttribute = .forceRightToLeft
            UISearchBar.appearance().semanticContentAttribute = .forceRightToLeft
            UITextView.appearance().semanticContentAttribute = .forceRightToLeft
        }
    }
    
//MARK: ################### END ###################
    
      func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let firebaseAuth = Auth.auth()
        firebaseAuth.setAPNSToken(deviceToken, type: .unknown)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let firebaseAuth = Auth.auth()
        if (firebaseAuth.canHandleNotification(userInfo)){
            print(userInfo)
            return
            
        }
    }
    
    func setTabbarAsRoot() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let tabBar = storyboard.instantiateViewController(withIdentifier: "TabBarController") as? TabBarController {
            window?.rootViewController = tabBar
            window?.makeKeyAndVisible()
        }
    }

    func setLogInRoot() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let logInVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            let nav = UINavigationController(rootViewController: logInVC)
            nav.isNavigationBarHidden = true
            window?.rootViewController = nav
            window?.makeKeyAndVisible()
        }
    }
   
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Sayarti")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func setupUTL(semanticContentAttribute : UISemanticContentAttribute, goToHome: Bool){
        
        UIView.appearance().semanticContentAttribute = semanticContentAttribute
        UITextField.appearance().semanticContentAttribute = semanticContentAttribute
        UITabBar.appearance().semanticContentAttribute = semanticContentAttribute
        UICollectionView.appearance().semanticContentAttribute = semanticContentAttribute
        UITableView.appearance().semanticContentAttribute = semanticContentAttribute
        UILabel.appearance().semanticContentAttribute = semanticContentAttribute
        UISegmentedControl.appearance().semanticContentAttribute = semanticContentAttribute
        UINavigationBar.appearance().semanticContentAttribute = semanticContentAttribute
        UISearchBar.appearance().semanticContentAttribute = semanticContentAttribute
        UITextView.appearance().semanticContentAttribute = semanticContentAttribute
        if (goToHome){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBarController")
            self.window?.rootViewController = initialViewController
        }
    }

}

