//
//  PopUpViewController.swift
//  BLE DEMO
//
//  Created by Malhar on 22/10/19.
//  Copyright © 2019 Malhar. All rights reserved.
//

import UIKit

protocol popupDelegate {
    func onBtnDismiss(selectedIndex: Int)
}

class PopUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   

    @IBOutlet weak var viewBlur: UIButton!
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet weak var btnContinue: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    
    var customDelegate     : popupDelegate?
    var arrTitles          = [String]()
    var selectedIndex      = 0
    var strMainTitle       = "Select payment method : "
    var arrImages          = [String]()
    
//MARK:- VIEW DELEGATES
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.showBlurView()
    }
    
    private func setupView(){
        let nib = UINib.init(nibName: "RadioButtomViewCell", bundle: nil)
        self.tblView.register(nib, forCellReuseIdentifier: "RadioButtomViewCell")
        self.tblView.layer.cornerRadius = 5.0
        self.btnContinue.layer.cornerRadius = 5.0
        self.btnCancel.layer.cornerRadius = 5.0
        self.containerView.layer.cornerRadius = 5.0
    }
    
    private func showBlurView(){
        UIView.animate(withDuration: 0.3, animations: {
            self.viewBlur.alpha = 0.3
        })
    }
    
    
//MARK:- TABLE VIEW DELEGATES

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RadioButtomViewCell", for: indexPath) as! RadioButtomViewCell
        cell.lblTitle.text  = self.arrTitles[indexPath.row]
        
        if self.arrImages.count > 0{
            cell.imgRadio.image = nil
            if self.selectedIndex == indexPath.row{
                cell.imgRadio.image = #imageLiteral(resourceName: "radioButtonChecked")
            }else{
//                if let imgURL = URL.init(string: self.arrImages[indexPath.row]){
//                    cell.imgRadio.af_setImage(withURL: imgURL)
//                }
                cell.imgRadio.setImageOfApp(withUrl: self.arrImages[indexPath.row])
            }
        }else{
            cell.imgRadio.image = (self.selectedIndex == indexPath.row) ? #imageLiteral(resourceName: "radioButtonChecked") : #imageLiteral(resourceName: "closeIcon")
        }
       
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width , height: 44.0))
        let lblTitle = UILabel.init(frame: CGRect.init(x: 20, y: 0, width: self.view.frame.size.width - 20 , height: 44.0))
        lblTitle.text = self.strMainTitle
        lblTitle.font = UIFont.boldSystemFont(ofSize: 17)
        lblTitle.textColor = .black
        headerView.addSubview(lblTitle)
        headerView.backgroundColor = .white
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        self.tblView.reloadData()
    }

    @IBAction func onBtnDismiss(_ sender: Any) {
        
        UIView.animate(withDuration: 0.0, animations: {
            self.viewBlur.alpha = 0.0
            self.customDelegate?.onBtnDismiss(selectedIndex: self.selectedIndex)
            self.dismiss(animated: true)
        })
    }
    
    @IBAction func onBtnCancel(_ sender: Any) {
        UIView.animate(withDuration: 0.0, animations: {
            self.viewBlur.alpha = 0.0
            self.dismiss(animated: true)
        })
    }
    
    
    
}
