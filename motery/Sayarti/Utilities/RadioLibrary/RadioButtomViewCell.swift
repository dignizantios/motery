//
//  RadioButtomViewCell.swift
//  BLE DEMO
//
//  Created by Malhar on 22/10/19.
//  Copyright © 2019 Malhar. All rights reserved.
//

import UIKit

class RadioButtomViewCell: UITableViewCell {
    
    @IBOutlet weak var imgRadio: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
