//
//  Extenstions.swift
//  Sayarti
//
//  Created by Vasim Ajmeri on 29/09/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

//TODO: Vishal
var isProtectionWashCar = false
var isCashPaymnetSelected = true


extension UIApplication {
    
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
            
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
}


extension UIViewController {
    
    func showDefaultAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: { action in })
        alert.addAction(ok)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    
    func showLoader(){
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.custom)
        SVProgressHUD.setDefaultMaskType(.clear)
        SVProgressHUD.show()
    }
    
    func hideLoader(){
        SVProgressHUD.dismiss()
    }
    
    //TODO: - Yash Changes
    func numberArabicToEnglish(stringValue: String) -> String {
        let value = stringValue.replacingOccurrences(of: "١", with: "1").replacingOccurrences(of: "٢", with: "2").replacingOccurrences(of: "٣", with: "3").replacingOccurrences(of: "٤", with: "4").replacingOccurrences(of: "٥", with: "5").replacingOccurrences(of: "٦", with: "6").replacingOccurrences(of: "٧", with: "7").replacingOccurrences(of: "٨", with: "8").replacingOccurrences(of: "٩", with: "9").replacingOccurrences(of: "٠", with: "0")
        return value
    }


    func numberEnglishToArabic(stringValue: String) -> String {
        let value = stringValue.replacingOccurrences(of: "1", with: "١").replacingOccurrences(of: "2", with: "٢").replacingOccurrences(of: "3", with: "٣").replacingOccurrences(of: "4", with: "٤").replacingOccurrences(of: "5", with: "٥").replacingOccurrences(of: "6", with: "٦").replacingOccurrences(of: "7", with: "٧").replacingOccurrences(of: "8", with: "٨").replacingOccurrences(of: "9", with: "٩").replacingOccurrences(of: "0", with: "٠")
        return value
    }

}


class AlwaysPresentAsPopover : NSObject, UIPopoverPresentationControllerDelegate {
    
    // `sharedInstance` because the delegate property is weak - the delegate instance needs to be retained.
    private static let sharedInstance = AlwaysPresentAsPopover()
    
    private override init() {
        super.init()
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    static func configurePresentation(forController controller : UIViewController) -> UIPopoverPresentationController {
        controller.modalPresentationStyle = .popover
        let presentationController = controller.presentationController as! UIPopoverPresentationController
        presentationController.delegate = AlwaysPresentAsPopover.sharedInstance
        return presentationController
    }
    
}


extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}

extension UIImageView {
    func setImageForCar(withUrl url : String?) {
        let _url = URL(string: url ?? "")
        if _url == nil {
            self.image = #imageLiteral(resourceName: "car_placeholder")
        }else{
            self.af_setImage(withURL: _url!, placeholderImage: #imageLiteral(resourceName: "car_placeholder"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: nil)
        }
    }
    
    func setImageOfApp(withUrl url : String?) {
        let _url = URL(string: url ?? "")
        if _url == nil {
            self.image = #imageLiteral(resourceName: "ic_placeholder")
        }else{
            self.af_setImage(withURL: _url!, placeholderImage: #imageLiteral(resourceName: "ic_placeholder"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: nil)
        }
    }
    
    func setImageOfCarType(withUrl url : String?) {
        let _url = URL(string: url ?? "")
        if _url == nil {
            self.image = #imageLiteral(resourceName: "washphoto2")
        }else{
            self.contentMode = .scaleAspectFit
            self.af_setImage(withURL: _url!, placeholderImage: #imageLiteral(resourceName: "washphoto2"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: nil)
        }
    }
}


extension UIColor {
    
    // MARK: - Initialization
    
    convenience init?(hex: String) {
        var hexSanitized = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        hexSanitized = hexSanitized.replacingOccurrences(of: "#", with: "")
        
        var rgb: UInt32 = 0
        
        var r: CGFloat = 0.0
        var g: CGFloat = 0.0
        var b: CGFloat = 0.0
        var a: CGFloat = 1.0
        
        let length = hexSanitized.count
        
        guard Scanner(string: hexSanitized).scanHexInt32(&rgb) else { return nil }
        
        if length == 6 {
            r = CGFloat((rgb & 0xFF0000) >> 16) / 255.0
            g = CGFloat((rgb & 0x00FF00) >> 8) / 255.0
            b = CGFloat(rgb & 0x0000FF) / 255.0
            
        } else if length == 8 {
            r = CGFloat((rgb & 0xFF000000) >> 24) / 255.0
            g = CGFloat((rgb & 0x00FF0000) >> 16) / 255.0
            b = CGFloat((rgb & 0x0000FF00) >> 8) / 255.0
            a = CGFloat(rgb & 0x000000FF) / 255.0
            
        } else {
            return nil
        }
        
        self.init(red: r, green: g, blue: b, alpha: a)
    }
    
    // MARK: - Computed Properties
    
    var toHex: String? {
        return toHex()
    }
    
    // MARK: - From UIColor to String
    
    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }
        
        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }
    
}



extension UIViewController {
    
    func checkLoginStatus(complition : @escaping (Bool) -> ()) {
        if Constants.bIsUserLoggedIn() {
            complition(true)
        }else{
            let alert = UIAlertController(title: "Log in required".localized(), message: "Please login to proceed further.".localized(), preferredStyle: UIAlertController.Style.alert)
            
            let loginAction = UIAlertAction(title: "Log in".localized(), style: .default) { (_) in
                alert.dismiss(animated: true, completion: nil)
                
//                let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
//                loginVC.modalPresentationStyle = .fullScreen
//                loginVC.bIsForGuestUser = true
//                self.tabBarController?.navigationController?.pushViewController(loginVC, animated: true)
                
                Constants.APP_DELEGATE.setLogInRoot()
                complition(false)
            }
            
            let dismissAction = UIAlertAction(title: "Dismiss".localized(), style: .cancel) { (_) in
                alert.dismiss(animated: true, completion: nil)
                complition(false)
            }
            
            alert.addAction(loginAction)
            alert.addAction(dismissAction)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func showAlert(message : String , status: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { action in
            DispatchQueue.main.async {
                alert.dismiss(animated: true, completion: nil)
                self.hideLoader()
                
                //                if (message == "Unauthorized user"){
                //
                //                    UserDefaults.standard.set(nil, forKey: "userId")
                //                    UserDefaults.standard.set(nil, forKey: "userName")
                //                    UserDefaults.standard.set(nil, forKey: "userEmail")
                //                    UserDefaults.standard.set(false, forKey: "didLogin")
                //
                //                    let selectLoginOrRegisterViewController = self.storyboard?.instantiateViewController(withIdentifier: "SelectLoginOrRegisterViewController") as! SelectLoginOrRegisterViewController
                //                    let navigationController = self.storyboard?.instantiateViewController(withIdentifier: "SelectLoginOrRegisterNavigationController") as! UINavigationController
                //                    let appdelegate = UIApplication.shared.delegate as! AppDelegate
                //                    navigationController.viewControllers = [selectLoginOrRegisterViewController]
                //                    appdelegate.window!.rootViewController = navigationController
                //                }
                
                if (status == "back") {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }))
        alert.view.tintColor = .black
        self.present(alert, animated: true, completion: nil)
    }
}

extension String {
    
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let regex = try! NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
    
    func removingWhitespaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
}

extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
}

extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}

extension UIColor {
    
    public class var appColor: UIColor{
        return UIColor(red: 200.0/255.0, green: 170.0/255.0, blue: 110.0/255.0, alpha: 1.0)
    }
}

extension UIImage
{
    class func imageWithColor(color: UIColor, size: CGSize) -> UIImage
    {
        let rect: CGRect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension String {
    func toDouble() -> Double? {
        return NumberFormatter().number(from: self)?.doubleValue
    }
}

extension UIImage {
    func toBase64() -> String? {
        guard let imageData = self.jpegData(compressionQuality: 0.5) else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
}
