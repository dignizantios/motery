//
//  washCompanyCollectionViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/2/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class WashCompanyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var companyNameLabel: UILabel!
    
}
