//
//  HomeSecondTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 2/28/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class HomeSecondTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    
    var category: HomeCategory! {
        didSet{
            itemNameLabel.text = category.name
            itemImageView.setImageForCar(withUrl: category.image)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        itemImageView.clipsToBounds = true
        itemImageView.contentMode = .scaleAspectFill
        let colorTop =  UIColor.clear.cgColor
        let colorBottom = UIColor.black.cgColor

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.5, 1.0]
        gradientLayer.frame = itemImageView.bounds

        itemImageView.layer.insertSublayer(gradientLayer, at: 1)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
  
    
}
