//
//  categoryCollectionViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/3/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var classImageView: UIImageView!
    @IBOutlet weak var classNameLabel: UILabel!
    @IBOutlet weak var classYearLabel: UILabel!
    
    override func awakeFromNib() {
        
    }
    
}
