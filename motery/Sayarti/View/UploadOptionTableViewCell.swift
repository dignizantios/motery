//
//  uploadOptionTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/5/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

protocol UploadPhotoDelegate {
    func uploadPhoto(index: Int)
}

class UploadOptionTableViewCell: UITableViewCell {
    
    var index: Int! = 0
    var delegate: UploadPhotoDelegate!
    @IBOutlet weak var optionImageView: UIImageView!
    @IBOutlet weak var optionLabel: UILabel!
    @IBOutlet weak var uploadButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        optionImageView.layer.cornerRadius = 10.0
        uploadButton.layer.cornerRadius = 10.0
        
        self.uploadButton.setTitle("Upload".localized(), for: .normal)
        // Initialization code
    }
    
    @IBAction func uploadAction(_ sender: UIButton) {
        delegate.uploadPhoto(index: index)
    }
}
