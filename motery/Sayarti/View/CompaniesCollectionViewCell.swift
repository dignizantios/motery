//
//  companiesCollectionViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/2/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class CompaniesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var brandImageView: UIImageView!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var agentImageView: UIImageView!
    @IBOutlet weak var mainView: UIView!
    
}
