//
//  ViewPhotoCollectionViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/24/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class ViewPhotoCollectionViewCell: UICollectionViewCell , UIScrollViewDelegate {
    
//    var size: CGSize!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var photoImageView: UIImageView!{
        didSet{
            image = photoImageView.image 
            setZoomScaleFor(image: image)
            recenterImage()
        }
    }
    @IBOutlet weak var imageViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTrailingConstraint: NSLayoutConstraint!
    
    var image: UIImage! {
        didSet{
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainScrollView.delegate = self
        mainScrollView.minimumZoomScale = 1
        mainScrollView.maximumZoomScale = 5
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(maxZoomImage(gesture:)))
        tapGesture.numberOfTapsRequired = 2
        photoImageView.addGestureRecognizer(tapGesture)
    }
    
    @objc func maxZoomImage (gesture: UIPinchGestureRecognizer){
        
        if (mainScrollView.zoomScale == 5){
            mainScrollView.setZoomScale(1, animated: true)
        }
        else {
            mainScrollView.setZoomScale(5, animated: true)
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        
        return photoImageView
    }
    
    func setZoomScaleFor(image: UIImage) {
        
        let imageSize = image.size
        let screenSize = UIScreen.main.bounds.size
        
        let widthScale =  screenSize.width / imageSize.width
        let heightScale = screenSize.height / imageSize.height
        
        let minScale = min(widthScale, heightScale)
        
        mainScrollView.minimumZoomScale = minScale
        mainScrollView.maximumZoomScale = minScale * 5
        mainScrollView.zoomScale = minScale
        
    }
    
    func recenterImage() {
        let scale = mainScrollView.minimumZoomScale
        let screenSize = CGSize(width: UIScreen.main.bounds.width - 20,
                                height: UIScreen.main.bounds.height - 20)
        let imageSize = CGSize(width: image.size.width * scale ,
                               height: image.size.height * scale)
        
        let horizontalSpace = imageSize.width < screenSize.width ?  ( screenSize.width - imageSize.width ) / 2 : 0
        let verticalSpace = imageSize.height < screenSize.height ?  ( screenSize.height - imageSize.height ) / 2 : 0
        
        mainScrollView.contentInset = UIEdgeInsets(top: verticalSpace, left: horizontalSpace, bottom: verticalSpace, right: horizontalSpace)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        recenterImage()
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        recenterImage()
    }
}
