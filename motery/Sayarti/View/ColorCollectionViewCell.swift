//
//  ColorCollectionViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 4/8/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class ColorCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var colorView: UIView!
}
