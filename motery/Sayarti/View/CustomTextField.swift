//
//  customTextField.swift
//  Sayarti
//
//  Created by Mina Malak on 3/14/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
//import Localize_Swift

@IBDesignable
class CustomTextField: UITextField {
    
    @IBInspectable var leftImage: UIImage?{
        didSet {
            updateLeftView()
        }
    }
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet {
            updateLeftView()
        }
    }
    
    @IBInspectable var leftSpace: CGFloat = 0 {
        didSet {
            updateLeftView()
        }
    }
    
    @IBInspectable var rightImage: UIImage?{
        didSet {
            updateRightView()
        }
    }
    
    @IBInspectable var leftImageTintColor: UIColor?{
        didSet {
            updateLeftView()
        }
    }
    
    @IBInspectable var ImageHeight: CGFloat = 20{
        didSet {
            updateLeftView()
        }
    }
    
    @IBInspectable var ImageWidth: CGFloat = 20{
        didSet {
            updateLeftView()
        }
    }
    
    func updateLeftView(){
        if let image = leftImage {
            leftViewMode = .always
            var imageView = UIImageView()
            //            if (Localize.currentLanguage() == "en"){
            self.textAlignment = .left
            imageView = UIImageView(frame: CGRect(x: leftPadding, y: 0, width: ImageWidth , height: ImageHeight))
            //            }
            //            else {
            //                self.textAlignment = .right
            //                imageView = UIImageView(frame: CGRect(x: 10, y: 0, width: ImageWidth , height: ImageHeight))
            //            }
            
            imageView.image = image
            imageView.contentMode = .scaleAspectFit
            imageView.tintColor = leftImageTintColor
            let width = leftPadding + ImageWidth
            //            if (Localize.currentLanguage() == "en"){
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width + leftSpace , height: 20))
            view.addSubview(imageView)
            leftView = view
            //            }
            //            else {
            //                let view = UIView(frame: CGRect(x: 0, y: 0, width: width + 10 , height: 20))
            //                view.addSubview(imageView)
            //                leftView = view
            //            }
            
        } else {
            leftViewMode = .never
        }
    }
    
    func updateRightView(){
        if let image = rightImage {
            rightViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 17.5, width: 10 , height: 10))
            imageView.image = image
            imageView.contentMode = .scaleAspectFill
            let view = UIView(frame: CGRect(x: 10, y: 0, width: 20, height: 45))
            view.addSubview(imageView)
            
            rightView = view
            
        } else {
            rightViewMode = .never
        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return action == #selector(UIResponderStandardEditActions.cut) || action == #selector(UIResponderStandardEditActions.copy)
    }
    
}
