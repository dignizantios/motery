//
//  YearCollectionViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 4/1/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class YearCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var yearLabel: UILabel!
}
