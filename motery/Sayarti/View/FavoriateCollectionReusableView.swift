//
//  favoriateCollectionReusableView.swift
//  Sayarti
//
//  Created by Mina Malak on 3/12/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class FavoriateCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var sectionTitleLabel: UILabel!
    
}
