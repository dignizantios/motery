//
//  serviceTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/4/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class ServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var serviceTitleLabel: UILabel!
    @IBOutlet weak var serviceDescribtionLabel: UILabel!
    @IBOutlet weak var radioImageView: UIImageView!
    @IBOutlet weak var btnRadioSelection: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnReadMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnRadioSelection.isUserInteractionEnabled = false
        btnReadMore.setTitle("Read more".localized(), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
