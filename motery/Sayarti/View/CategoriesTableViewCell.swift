//
//  categoriesTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/3/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

protocol CategoriesTableViewCellDelegate {
    func selectClass(car: ResellerCar)
    func selectRentClass(rentCar: RentCar)
}


class CategoriesTableViewCell: UITableViewCell {
    
    var delegate: CategoriesTableViewCellDelegate!
    var category = Category()
    var bIsCarRent = false
    var bIsUsedCar = false

    @IBOutlet weak var mainCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

extension CategoriesTableViewCell : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.bIsCarRent || self.bIsUsedCar{
            return category.rentCars.count
        }
        return category.resellerCars.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell

        if(self.bIsCarRent || self.bIsUsedCar){
//            if let imgURL = URL.init(string: category.rentCars[indexPath.row].car_img ?? ""){
//                cell.classImageView.af_setImage(withURL: imgURL)
//            }
            
            cell.classImageView.setImageForCar(withUrl: category.rentCars[indexPath.row].car_img)
            cell.classNameLabel.text = category.rentCars[indexPath.row].name
        }else{
//            if let imgURL = URL.init(string: category.resellerCars[indexPath.row].image ?? ""){
//                cell.classImageView.af_setImage(withURL: imgURL)
//            }
            cell.classImageView.setImageForCar(withUrl: category.resellerCars[indexPath.row].image)
            cell.classNameLabel.text = category.resellerCars[indexPath.row].name ?? ""
            cell.classYearLabel.text = "\(category.resellerCars[indexPath.row].year ?? 2019)"
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewWidth = collectionView.bounds.width - 150
        return CGSize(width: collectionViewWidth, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.bIsCarRent || self.bIsUsedCar{
            delegate.selectRentClass(rentCar: category.rentCars[indexPath.row])
        }else{
            delegate.selectClass(car: category.resellerCars[indexPath.row])
        }
    }
}
