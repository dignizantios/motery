//
//  orderTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/17/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var orderIdLabel: UILabel!
    @IBOutlet weak var servicesSLabel: UILabel!
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var companySLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var dateSLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.servicesSLabel.text = "Services".localized()
        self.companySLabel.text = "Company".localized()
        self.dateSLabel.text = "Date".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
