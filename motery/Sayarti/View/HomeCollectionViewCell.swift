//
//  HomeCollectionViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 2/28/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit


class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    
    var ad: Ad! {
        didSet{
            imageView.setImageForCar(withUrl: ad.image)
        }
    }
}
