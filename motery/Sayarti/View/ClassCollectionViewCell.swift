//
//  classCollectionViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/4/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class ClassCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var classImageView: UIImageView!
}
