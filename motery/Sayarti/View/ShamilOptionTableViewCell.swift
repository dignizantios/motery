//
//  shamilOptionTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/11/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class ShamilOptionTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var optionNameLabel: UILabel!
    @IBOutlet weak var optionDescriptionLabel: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var imgGoIcon: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
