//
//  washServiceTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/9/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class WashServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var serniceLabel: UILabel!
    @IBOutlet weak var serviceDescriptionLabel: UILabel!
    @IBOutlet weak var radioImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var btnRadioGroup: UIButton!
    @IBOutlet weak var btnReadMore: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnRadioGroup.isUserInteractionEnabled = false
        btnReadMore.setTitle("Read more".localized(), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
