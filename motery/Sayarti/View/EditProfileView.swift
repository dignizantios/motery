//
//  editProfileView.swift
//  Sayarti
//
//  Created by Mina Malak on 3/14/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

protocol EditProfilePictureDelegate {
    func dismissView()
    func addProfilePicture()
    func updateProfile()
}

class EditProfileView: UIView {
    
    var delegate: EditProfilePictureDelegate!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var detailsLabel: UILabel!
    @IBOutlet weak var userProfilePictureImageView: UIImageView!
    @IBOutlet weak var addProfilePictureButton: UIButton!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userMobileTextField: UITextField!
    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    func dismissKeyboard(){
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        mainView.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func dismissButton(_ sender: UIButton) {
        delegate.dismissView()
        self.removeFromSuperview()
    }
    
    @IBAction func addProfilePicture(_ sender: UIButton) {
        
//        self.removeFromSuperview()
//        delegate.dismissView()
        delegate.addProfilePicture()
    }
    
    @IBAction func updateProfileDetails(_ sender: UIButton) {
        delegate.updateProfile()
    }
    
    @IBAction func dismissKeyboard(gesture: UITapGestureRecognizer)
    {
        self.endEditing(true)
    }
}

