//
//  customTableView.swift
//  Sayarti
//
//  Created by Mina Malak on 3/5/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class CustomTableView: UITableView {
    
    override var contentSize:CGSize {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}

