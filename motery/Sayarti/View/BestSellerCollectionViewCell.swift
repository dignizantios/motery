//
//  BestSellerCollectionViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 4/10/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class BestSellerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var offerImageView: UIImageView!
//    @IBOutlet weak var textView: UIView!
  //  @IBOutlet weak var titleLabel: UILabel!
  //  @IBOutlet weak var descriptionLabel: UILabel!
    
    var offer: Offer!{
        didSet{
//            if let url = URL(string: offer.image ?? ""){
//                offerImageView.af_setImage(withURL: url)
//            }
            offerImageView.setImageForCar(withUrl: offer.image)
            //OFFER PERCENTAGE
         //   descriptionLabel.text = "" //"\(offer.percentage ?? 0)%"
         //   titleLabel.text       = offer.name
          //  titleLabel.textColor = .black
        }
    }
}
