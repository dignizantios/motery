//
//  CalculatorTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 4/8/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class CalculatorTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
