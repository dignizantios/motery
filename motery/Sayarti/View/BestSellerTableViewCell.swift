//
//  BestSellerTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 4/10/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

protocol BestSellerTableViewCellDelegate {
    func goToDetails(index: Int)
}

class BestSellerTableViewCell: UITableViewCell {

    var delegate: BestSellerTableViewCellDelegate!
    var offers = [Offer]()
    @IBOutlet weak var lblOffers: UILabel!
    
    @IBOutlet weak var bestSellerCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bestSellerCollectionView.dataSource = self
        bestSellerCollectionView.delegate = self
        lblOffers.text = "Offers".localized()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension BestSellerTableViewCell: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return offers.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BestSellerCell", for: indexPath) as! BestSellerCollectionViewCell
        cell.offerImageView.layer.cornerRadius = 9.0
        cell.offerImageView.layer.masksToBounds = true
        cell.offer = offers[indexPath.row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 3, bottom: 0, right: 3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
//        return CGSize(width: 280, height: collectionView.bounds.height)
        
        print("Height : \(collectionView.bounds.height)")
        
        return CGSize(width: collectionView.bounds.width * 0.8, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        delegate.goToDetails(index: indexPath.row)
    }
}
