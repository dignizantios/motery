//
//  carResellerOrderDetailsTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/31/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class CarResellerOrderDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
