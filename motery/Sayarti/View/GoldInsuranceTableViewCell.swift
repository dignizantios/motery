//
//  goldInsuranceTableViewCell.swift
//  Sayarti
//
//  Created by Mina Malak on 3/12/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class GoldInsuranceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var serviceImageView: UIImageView!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var radioImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
}
