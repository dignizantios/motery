//
//  TabBarController.swift
//  Sayarti
//
//  Created by Mina Malak on 4/15/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController , UITabBarControllerDelegate {
    
    let tabBarItemsNames = ["Home","Calculator","Favorites","Profile"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setup()
    }
    
    func setup(){
        
        var x = 0
        for item in tabBar.items!
        {
            item.title = tabBarItemsNames[x].localized()
            x = x + 1
        }
    }
    
}
