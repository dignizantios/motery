//
//  washCompaniesViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/2/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import AlamofireImage


class WashCompaniesViewController: UIViewController , UISearchBarDelegate {
//MARK:- == OUTLETS ==
    @IBOutlet weak var washCompaniesCollectionView: UICollectionView!
    
//MARK:- == VARIABLES ==
    private var arrCarWashCompanies = [Companies]()
    private var selectedCompanyIndex = 0
    var bIsCarRent = false
    var strTitle = ""
    
    var arrCarWashSearchCompines = [Companies]()
    var bIsSearch = false

    
//MARK:- == VIEW DELEGATES ==

    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchCompanies()
        self.navigationItem.title = strTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
//MARK:- == FETCH COMPANIES ==

    private func fetchCompanies(){
        self.showLoader()
        let strAPI = self.bIsCarRent ? Constants.GET_ALL_RENT_COMPANIES : Constants.GET_ALL_WASH_STORE

        APIManager.shared.fetchRequestData(strURL: strAPI, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {
                    self?.arrCarWashCompanies = try JSONDecoder().decode([Companies].self, from: jsonData)
                    self?.arrCarWashSearchCompines = self?.arrCarWashCompanies ?? []
                    self?.washCompaniesCollectionView.reloadData()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }
    
    
    
    @IBAction func beginSearch(_ sender: UIBarButtonItem) {
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        searchController.hidesNavigationBarDuringPresentation = false
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Cancel".localized()

       // searchController.searchBar.setValue("Cancel", forKey: "_cancelButtonText")
        searchController.searchBar.tintColor = UIColor.black
        searchController.searchBar.barTintColor = Constants.grayColor
        searchController.searchBar.placeholder = "Search".localized()
        self.present(searchController,animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        dismiss(animated: true, completion: nil)
        bIsSearch = true
        let filterArray = self.arrCarWashSearchCompines.filter({ ($0.name ?? "").contains(searchBar.text ?? "") })
        if filterArray.count > 0{
            self.arrCarWashCompanies = filterArray
            self.washCompaniesCollectionView.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentString = (searchBar.text!.lowercased() as NSString).replacingCharacters(in: range, with: text)
        
        if currentString.count > 0 {
            self.bIsSearch = true
            let filterArray = self.arrCarWashSearchCompines.filter({ (($0.name ?? "").lowercased()).contains(currentString)})
            
            if filterArray.count > 0{
                self.arrCarWashCompanies = filterArray
                self.washCompaniesCollectionView.reloadData()
            }
            else{
                self.arrCarWashCompanies.removeAll()
                self.washCompaniesCollectionView.reloadData()
            }
        }
        else {
            self.arrCarWashCompanies = self.arrCarWashSearchCompines
            self.washCompaniesCollectionView.reloadData()
        }
        
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        bIsSearch = false
        self.arrCarWashCompanies = self.arrCarWashSearchCompines
        self.washCompaniesCollectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        bIsSearch = false
        self.arrCarWashCompanies = self.arrCarWashSearchCompines
        self.washCompaniesCollectionView.reloadData()
    }
    
}

//MARK:- == EXTENSTION ==

extension WashCompaniesViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrCarWashCompanies.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "washCell", for: indexPath) as! WashCompanyCollectionViewCell
        let washCompany = self.arrCarWashCompanies[indexPath.row]
        
        cell.companyNameLabel.text = washCompany.name ?? ""
        
//        if let imgPath = washCompany.image {
//            if let imgUrl = URL.init(string: imgPath){
//                cell.companyImageView.af_setImage(withURL: imgUrl)
//            }else{
//                cell.companyImageView.image = #imageLiteral(resourceName: "wash1")
//            }
//        }
        cell.companyImageView.setImageOfApp(withUrl: washCompany.image)
        cell.mainView.layer.cornerRadius = 10.0
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewWidth = collectionView.bounds.width / 2
        return CGSize(width: collectionViewWidth, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedCompanyIndex = indexPath.row
        self.performSegue(withIdentifier: "showDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails"{
            
            if let destinationVC = segue.destination as? WashCompanyDetailsViewController{
                let selectedStore     = self.arrCarWashCompanies[self.selectedCompanyIndex]
                destinationVC.storeId = selectedStore.id ?? 0
                destinationVC.title   = selectedStore.name ?? ""
                destinationVC.strImgLogo = selectedStore.image ?? ""
            }
            
        }
        
    }
    
}
