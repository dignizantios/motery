//
//  washCompanyDetailsViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/9/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Lightbox
import Localize_Swift

class WashCompanyDetailsViewController: UIViewController , UIScrollViewDelegate, UITextFieldDelegate {
    
    var index: Int = 0
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var downloadButton: UIButton!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var carTybeDescription: UILabel!
    @IBOutlet weak var carTypeCollectionView: UICollectionView!
    @IBOutlet weak var photosLabel: UILabel!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var servicesDescriptionLabel: UILabel!
    @IBOutlet weak var servicesTableView: CustomTableView!
    @IBOutlet weak var packagesLabel: UILabel!
    @IBOutlet weak var packagesDescriptionLabel: UILabel!
    @IBOutlet weak var packagesTableView: CustomTableView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var serviceDateTextField: UITextField!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var tblDescription: UITableView!
    @IBOutlet weak var tblDescriptionHeightCostraint: NSLayoutConstraint!
    
    
    //image literal
    var startingFrame  = CGRect()
    let carImages      = [#imageLiteral(resourceName: "image-2"),#imageLiteral(resourceName: "image-3"),#imageLiteral(resourceName: "image-1")]
    var arrCarImages   = [String]()
    var companyDetails : CompanyDetail?
    var arrServices    = [Service]()
    var arrPackages    = [Package]()
    let datePickerView = UIDatePicker()
    var storeId        = 0
    var bIsFavorite    = false
    private var conts_animation_duration = 0.3
    var serviceSelectedIndex : Int?
    var packageSelectedIndex : Int?
    var arrCarTypes = [CarType]()
    private let imageView = UIImageView(image: UIImage(named: "wash1"))
    var selectedCarTypeIndex = 0
    var strImgLogo = ""
    
    var selectedPackge = Package()
    var arrayCategoryDetail = [ResellerDetailCategory]()
    
    //MARK:- VIEW DELEGATES
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainScrollView.isHidden = true
        self.setupView()
        self.fetchCompanyDetails()
        registerXib()
        addObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
        self.setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        imageView.removeFromSuperview()
    }
    
    private func setupView(){
        
        print("self.str")
        self.imageView.setImageForCar(withUrl: self.strImgLogo)
        
        self.setupCompanyDetails()
        self.setupServices()
        self.setupPackages()
        
        servicesTableView.tableFooterView = UIView()
        packagesTableView.tableFooterView = UIView()
        continueButton.layer.cornerRadius = 9.0
        optionView.layer.cornerRadius = 25.0
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = Date()
        serviceDateTextField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
        let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        numberToolbar.barStyle = .default
        numberToolbar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(actionKeyboardDoneButton))]
        numberToolbar.tintColor = .black
        numberToolbar.sizeToFit()
        serviceDateTextField.inputAccessoryView = numberToolbar
    }
    
    private func setupUI() {
        
        guard let navigationBar = self.navigationController?.navigationBar else { return }
        
        navigationBar.addSubview(imageView)
        //imageView.layer.cornerRadius = Const.ImageSizeForLargeState / 2
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor, constant: -10),
            imageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Const.ImageBottomMarginForLargeState),
            imageView.heightAnchor.constraint(equalToConstant: Const.ImageSizeForLargeState),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor)
        ])
        
        
        if (Localize.currentLanguage() == "ar"){
            serviceDateTextField.textAlignment = .right
        }
        
        carTypeLabel.text = "Car Type".localized()
        carTybeDescription.text = "Please choose your car type".localized()
        photosLabel.text = "Photos".localized()
        lblDate.text = "Date".localized()
        serviceDateTextField.placeholder = "Choose Service Date".localized()
        servicesLabel.text = "Services".localized()
        servicesDescriptionLabel.text = "Choose services that sutes your needs".localized()
        
        packagesLabel.text = "Packages".localized()
        packagesDescriptionLabel.text = "multiple services with discounted price".localized()
        continueButton.setTitle("Continue".localized(), for: .normal)
        
    }
    
    func registerXib() {
        self.tblDescription.register(UINib(nibName: "DetailCategoryTableCell", bundle: nil), forCellReuseIdentifier: "DetailCategoryTableCell")
    }
    
    private func setupCompanyDetails(){
        
        UIView.animate(withDuration: conts_animation_duration) {
            self.title = self.companyDetails?.store?.name ?? ""
            self.descriptionLabel.text = self.companyDetails?.store?.desc ?? ""
            
            if let arrImages = self.companyDetails?.store_imgs{
                if arrImages.count > 0{
                    self.photosCollectionView.reloadData()
                }else{
                    self.photosLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
                    self.photosCollectionView.heightAnchor.constraint(equalToConstant: 0).isActive = true
                }
                
                print("arraImages:\(arrImages)")
            }
            
            if let bIsFavorite = self.companyDetails?.store?.is_favorite{
                self.bIsFavorite = bIsFavorite
                self.setupFavoritesImage()
            }else{
                self.bIsFavorite = false
                self.favoriteButton.setImage(#imageLiteral(resourceName: "Favorite") , for: .normal)
            }
            self.view.layoutIfNeeded()
        }
    }
    
    private func setupFavoritesImage(){
        if(self.bIsFavorite){
            self.favoriteButton.setImage(#imageLiteral(resourceName: "addToFavorite") , for: .normal)
        }else{
            self.favoriteButton.setImage(#imageLiteral(resourceName: "Favorite") , for: .normal)
        }
    }
    
    private func setupServices(){
        UIView.animate(withDuration: conts_animation_duration) {
            self.servicesTableView.reloadData()
        }
    }
    
    private func setupPackages(){
        UIView.animate(withDuration: conts_animation_duration) {
            self.packagesTableView.reloadData()
        }
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        serviceDateTextField.text = dateFormatter.string(from: sender.date)
    }
    
    @objc func actionKeyboardDoneButton() {
        self.view.endEditing(true)
    }
    
    //MARK:- == FETCH COMPANY DETAILS ==
    
    private func fetchCompanyDetails(){
        self.showLoader()
        
        let storeDetailUrl = Constants.GET_WASH_STORE_DETAILS + "\(self.storeId)"
        
        APIManager.shared.fetchRequestData(strURL: storeDetailUrl, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {
                    self?.companyDetails = try JSONDecoder().decode(CompanyDetail.self, from: jsonData)
                    
                    var dicDetailCategory: [ResellerDetailCategory]
                    if let detailCategory = self?.companyDetails?.store?.detail_category {
                        dicDetailCategory = detailCategory
                        for i in 0..<dicDetailCategory.count {
                            dicDetailCategory[i].isSelected = 0
                        }
//                        print("Data: ", dicDetailCategory)
                        self?.companyDetails?.store?.detail_category = dicDetailCategory
                        self?.arrayCategoryDetail = dicDetailCategory
                        self?.tblDescription.reloadData()
                    }
//                    print("Array: \(self?.arrayCategoryDetail)")
                    self?.setupCompanyDetails()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.fetchCarTypes()
        }
    }
    
    //MARK:- == FETCH CAR TYPE ==
    
    private func fetchCarTypes(){
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_CAR_TYPES, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {
                    self?.arrCarTypes = try JSONDecoder().decode([CarType].self, from: jsonData)
                    self?.carTypeCollectionView.reloadData()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.fetchCompanySevices()
        }
    }
    
    //MARK:- == FETCH SERVICES ==
    
    private func fetchCompanySevices(){
        
        let storeServiceUrl = Constants.GET_WASH_STORE_SERVICES + "\(self.storeId)"
        
        APIManager.shared.fetchRequestData(strURL: storeServiceUrl, bIsPostRequest: false, bIsShowAlert: false, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {
                    self?.arrServices = try JSONDecoder().decode([Service].self, from: jsonData)
                    if self?.arrServices.count == 0{
                        self?.hideServiceLables()
                    }else{
                        self?.setupServices()
                    }
                }catch{
                    self?.hideServiceLables()
                    print("Error : \(error.localizedDescription)")
                }
            }else{
                self?.hideServiceLables()
            }
            self?.fetchCompanyPackages()
        }
    }
    
    private func hideServiceLables(){
        UIView.animate(withDuration: conts_animation_duration) {
            self.servicesLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.servicesDescriptionLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK:- == FETCH PACKAGES ==
    
    private func fetchCompanyPackages(){
        
        let storePackageUrl = Constants.GET_WASH_PACKAGES + "\(self.storeId)"
        
        APIManager.shared.fetchRequestData(strURL: storePackageUrl, bIsPostRequest: false, bIsShowAlert: false, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {
                    self?.arrPackages = try JSONDecoder().decode([Package].self, from: jsonData)
                    if self?.arrPackages.count == 0{
                        self?.hidePackageLables()
                    }else{
                        self?.setupPackages()
                    }
                }catch{
                    self?.hidePackageLables()
                    print("Error : \(error.localizedDescription)")
                }
            }else{
                self?.hidePackageLables()
            }
            self?.hideLoader()
            self?.mainScrollView.isHidden = false
        }
    }
    
    
    private func hidePackageLables(){
        UIView.animate(withDuration: conts_animation_duration) {
            self.packagesLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.packagesDescriptionLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.view.layoutIfNeeded()
        }
        
    }
    
    private func setCompanyFavorite(){
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_WASH_FAVORITES, bIsPostRequest: true, bIsShowAlert: true, parameter: ["store_id":"\(self.storeId)"]) { [weak self] (responseData) in
            
            if let jsonData = responseData {
                print("JSON : ",jsonData.debugDescription)
                self?.bIsFavorite = !(self?.bIsFavorite ?? false)
                self?.setupFavoritesImage()
            }
            
            self?.hideLoader()
        }
    }
    
    //    private func setupUI() {
    //
    //        // Initial setup for image for Large NavBar state since the the screen always has Large NavBar once it gets opened
    //        guard let navigationBar = self.navigationController?.navigationBar else { return }
    //
    //        navigationBar.addSubview(imageView)
    //        imageView.layer.cornerRadius = Const.ImageSizeForLargeState / 2
    //        imageView.clipsToBounds = true
    //        imageView.translatesAutoresizingMaskIntoConstraints = false
    //        NSLayoutConstraint.activate([
    //            imageView.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor, constant: -Const.ImageRightMargin),
    //            imageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Const.ImageBottomMarginForLargeState),
    //            imageView.heightAnchor.constraint(equalToConstant: Const.ImageSizeForLargeState),
    //            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor)
    //            ])
    //    }
    
    private func moveAndResizeImageForPortrait() {
        guard let height = navigationController?.navigationBar.frame.height else { return }
        
        let coeff: CGFloat = {
            let delta = height - Const.NavBarHeightSmallState
            let heightDifferenceBetweenStates = (Const.NavBarHeightLargeState - Const.NavBarHeightSmallState)
            return delta / heightDifferenceBetweenStates
        }()
        
        let factor = Const.ImageSizeForSmallState / Const.ImageSizeForLargeState
        
        let scale: CGFloat = {
            let sizeAddendumFactor = coeff * (1.0 - factor)
            return min(1.0, sizeAddendumFactor + factor)
        }()
        
        // Value of difference between icons for large and small states
        let sizeDiff = Const.ImageSizeForLargeState * (1.0 - factor) // 8.0
        
        let yTranslation: CGFloat = {
            /// This value = 14. It equals to difference of 12 and 6 (bottom margin for large and small states). Also it adds 8.0 (size difference when the image gets smaller size)
            let maxYTranslation = Const.ImageBottomMarginForLargeState - Const.ImageBottomMarginForSmallState + sizeDiff
            return max(0, min(maxYTranslation, (maxYTranslation - coeff * (Const.ImageBottomMarginForSmallState + sizeDiff))))
        }()
        
        let xTranslation = max(0, sizeDiff - coeff * sizeDiff)
        
        imageView.transform = CGAffineTransform.identity
            .scaledBy(x: scale, y: scale)
            .translatedBy(x: xTranslation, y: yTranslation)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        moveAndResizeImageForPortrait()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showImages"){
            if let cell = self.photosCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? HomeCollectionViewCell{
                let vc = segue.destination as! ViewPhotosViewController
                
                if let image = cell.imageView.image {
                    vc.photos = [image]//car.photos
                    vc.index = 0
                    vc.firstTime = true
                }
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if self.serviceDateTextField.text == ""{
            self.setUpDateText()
        }
    }
    
    func setUpDateText(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        serviceDateTextField.text = dateFormatter.string(from: self.datePickerView.date)
    }
    
    //Vishal
    func showAlertAction() {
        
        let dialogMessage = UIAlertController(title: "Confirm".localized(), message: "Are you sure you want to contact with this dealer?".localized(), preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: { (action) -> Void in
            self.handleCallAction()
            print("Ok")
        })
        
        let cancel = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action) -> Void in }
        
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        self.present(dialogMessage, animated: true, completion: nil)
    }
}

// MARK:- Actions

extension WashCompanyDetailsViewController{
    
    @IBAction func shareAction(_ sender: UIButton) {
        
        let vc = UIActivityViewController(activityItems: ["Mercedes"], applicationActivities: [])
        self.present(vc, animated: true)
    }
    
    @IBAction func addToFavoriateAcion(_ sender: UIButton) {
        if Constants.bIsUserLoggedIn() == false{
            self.showAlert(message: "Please login to your account".localized(), status: "Login".localized())
            return
        }
        self.setCompanyFavorite()
    }
    
    @IBAction func showLocationAction(_ sender: UIButton) {
        
        let longitude : Double = 47.991876
        let latitude : Double = 29.380192
        
        if let url = URL(string: self.companyDetails?.store?.location ?? "") {
            
            UIApplication.shared.open(url)
        }
        else if let url2 = URL(string: "http://maps.apple.com/maps?saddr=\((latitude)),\((longitude))") {
            UIApplication.shared.open(url2)
        }
    }
    
    @IBAction func callAction(_ sender: UIButton) {
        
        self.showAlertAction()
        
        //Yash
//        handleCallAction()
        
        //Old
        /*if let url = URL(string: "tel://\(self.companyDetails?.store?.contact ?? "")") {
         UIApplication.shared.open(url, options: [:], completionHandler: nil)
         }*/
    }
    
    @IBAction func continueAction(_ sender: UIButton) {
        self.handleContinueAction()
    }
    
    private func handleContinueAction(){
        //        if self.serviceDateTextField.text == ""{
        //            self.showDefaultAlert(title: "Choose Date", message: "Please choose service date!")
        //            return
        //        }
        
        //TODO: - Yash Changes
        
        checkLoginStatus { (isLoggedIn) in
            if isLoggedIn {
//                if self.arrServices.count > 0 && self.arrPackages.count > 0 {
                if self.arrServices.count > 0 {
                    
                    let selectService = self.arrServices.filter { (data) -> Bool in
                        let select = data.selectedService == 1 ? true : false
                        return select
                    }
                    
                    if selectService.count == 0 {
                        self.showDefaultAlert(title: "Service".localized(), message: "Please select atleast one service".localized())
                        return
                    }
                    
                    
                    //                    if self.serviceSelectedIndex == nil && self.packageSelectedIndex == nil{
//                    if self.serviceSelectedIndex == nil && self.packageSelectedIndex == nil{
//                        self.showDefaultAlert(title: "Service or Package".localized(), message: "Please choose atleast one service or package!".localized())
//                        return
//                    }
                }
                
                var dictData = [String:Any]()
                
                if self.serviceDateTextField.text != ""{
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    let strDate = dateFormatter.string(from: self.datePickerView.date)
                    dictData["booking_date"] = strDate
                }else{
                    dictData["booking_date"] = ""
                }
                
                dictData["store_id"] = "\(self.storeId)"
                dictData["car_type_id"] = "\(self.arrCarTypes[self.selectedCarTypeIndex].id ?? 0)"
                
                if let serviceIndex = self.serviceSelectedIndex{
                    dictData["service_id"]   = self.arrServices[serviceIndex].id ?? 0
                    dictData["service_info"] = self.arrServices[serviceIndex]
                }
                if let packageIndex = self.packageSelectedIndex{
                    dictData["package_id"]   = self.arrPackages[packageIndex].id ?? 0
                    dictData["package_info"] = self.arrPackages[packageIndex]
                }
                
                dictData["car_image"] = self.strImgLogo
                dictData["car_name"] = self.title
                
                let arrayServices = self.arrServices.filter({ (packages) -> Bool in
                    return packages.selectedService == 1
                })
                
                var arraySelectedServiceId = [Int]()
                
                for i in 0..<arrayServices.count{
                    arraySelectedServiceId.append(arrayServices[i].id ?? 0)
                }
                
                let attachMentVC = self.storyboard?.instantiateViewController(withIdentifier: "UploadOptionsViewController") as! UploadOptionsViewController
                attachMentVC.selectedId = arraySelectedServiceId
                attachMentVC.arraySelectedServicesForCarWash = arrayServices
                attachMentVC.dataDict = dictData
                attachMentVC.selectedPackgeForCarWash = self.selectedPackge
                self.navigationController?.pushViewController(attachMentVC, animated: true)
            }
        }
        
    }
    
}

//TODO:- Jaydeep Changes

extension WashCompanyDetailsViewController {
    private func handleCallAction(){
        self.showLoader()
        var StoreId = Int()
        
        var strURL = String()
        
        StoreId = self.companyDetails?.store?.id ?? 0
        
        strURL = Constants.POST_WASH_REQUEST_CALL
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: true, bIsShowAlert: true, parameter: ["store_id":"\(StoreId)"]) { [weak self] (responseData) in
            
            if let jsonData = responseData {
                print("JSON : ",jsonData.debugDescription)
                do {
                    if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String : Any]
                    {
                        self?.showDefaultAlert(title: "", message: json["message"] as! String)
                    }
                }
                catch {
                    
                }
            }
            self?.hideLoader()
        }
    }
}

//MARK:- Tablview Delegate

extension WashCompanyDetailsViewController: UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (tableView == servicesTableView){
            
            return self.arrServices.count
        }
        else if tableView == self.tblDescription {
            
            return self.arrayCategoryDetail.count
        }
        else {
            
            return self.arrPackages.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (tableView == servicesTableView){
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! WashServiceTableViewCell
            
            let service = self.arrServices[indexPath.row]
            cell.serniceLabel.text = service.name ?? ""
            cell.serviceDescriptionLabel.text = service.desc ?? ""
            //            cell.priceLabel.text   = String.init(format: "%.2f KD", (service.price ?? 0.0))
            if let price = service.price {
                cell.priceLabel.text = price <= 0 ? "No price".localized() : String(format: "%.2f KD", price)
            } else {
                //               cell.priceLabel.text = "No price".localized()
                cell.priceLabel.text = ""
            }
            cell.mainView.layer.cornerRadius = 9.0
            
            if service.selectedService ?? 0 == 0{
                cell.mainView.layer.borderColor = UIColor.clear.cgColor
                cell.mainView.layer.borderWidth = CGFloat(0)
                cell.radioImageView.image = nil
                cell.radioImageView.image = UIImage(named: "radioButton")
            }
            else{
                cell.mainView.layer.borderColor = Constants.appColor.cgColor
                cell.mainView.layer.borderWidth = CGFloat(2)
                cell.radioImageView.image = UIImage(named: "radioButtonChecked")
                
            }
            cell.btnReadMore.tag = indexPath.row
            cell.btnReadMore.addTarget(self, action: #selector(btnReadMoreServiceTableAction(_:)), for: .touchUpInside)
            //            cell.btnRadioGroup.tag = indexPath.row
            //            cell.btnRadioGroup.addTarget(self, action: #selector(btnRadioServicesTableSelction(_:)), for: .touchUpInside)
            
            return cell
        }
        else if tableView == self.tblDescription {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCategoryTableCell", for: indexPath) as! DetailCategoryTableCell
            
            let dict = self.arrayCategoryDetail[indexPath.row]
            cell.lblHeader.text = dict.category_name ?? ""
            cell.lblDescription.text = dict.detail_description ?? ""
            
            if dict.isSelected == 0 {
                cell.btnSelection.isSelected = true
                cell.vwDescription.isHidden = true
            }
            else {
                cell.btnSelection.setImage(UIImage(named: "ic_minus"), for: .normal)
                cell.btnSelection.isSelected = false
                cell.vwDescription.isHidden = false
            }
            cell.btnSelection.tag = indexPath.row
            cell.btnSelection.addTarget(self, action: #selector(btnAddAction(_:)), for: .touchUpInside)
            
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "packageCell", for: indexPath) as! WashServiceTableViewCell
            let package = self.arrPackages[indexPath.row]
            cell.serniceLabel.text = package.name ?? ""
            cell.serviceDescriptionLabel.text = package.desc ?? ""
            //            cell.priceLabel.text   = String.init(format: "%.2f KD", (package.price ?? 0.0))
            if let price = package.price {
                cell.priceLabel.text = price <= 0 ? "No price".localized() : String(format: "%.2f KD", price)
            } else {
                //               cell.priceLabel.text = "No price".localized()
                cell.priceLabel.text = ""
            }
            cell.mainView.layer.cornerRadius = 9.0
            
            if package.selectedService ?? 0 == 0{
                cell.mainView.layer.borderColor = UIColor.clear.cgColor
                cell.mainView.layer.borderWidth = CGFloat(0)
                cell.radioImageView.image = nil
                cell.radioImageView.image = UIImage(named: "radioButton")
            }
            else{
                cell.mainView.layer.borderColor = Constants.appColor.cgColor
                cell.mainView.layer.borderWidth = CGFloat(2)
                cell.radioImageView.image = UIImage(named: "radioButtonChecked")
                
            }
            cell.btnReadMore.tag = indexPath.row
            cell.btnReadMore.addTarget(self, action: #selector(btnReadMoreProviderTableAction(_:)), for: .touchUpInside)
            //            cell.btnRadioGroup.tag = indexPath.row
            //            cell.btnRadioGroup.addTarget(self, action: #selector(btnRadioProductTableSelction(_:)), for: .touchUpInside)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if tableView == self.tblDescription {
         
            return UITableView.automaticDimension
        }
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
         let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServiceDescriptionVC") as! ServiceDescriptionVC
         
         if tableView == servicesTableView {
         let data = self.arrServices[indexPath.row]
         vc.serviceData.name = data.name
         vc.serviceData.description = data.desc
         vc.serviceData.price = data.price
         print("Data: ", self.arrServices[indexPath.row])
         self.present(vc, animated: true, completion: nil)
         }
         else {
         let data = self.arrPackages[indexPath.row]
         vc.serviceData.name = data.name
         vc.serviceData.description = data.desc
         vc.serviceData.price = data.price
         print("Data: ", self.arrPackages[indexPath.row])
         self.present(vc, animated: true, completion: nil)
         }*/
        
        //TODO: Vishal Setup
        if tableView == servicesTableView {
            let index = IndexPath(row: indexPath.row, section: 0)
            if self.arrServices[indexPath.row].selectedService == 1{
                self.arrServices[indexPath.row].selectedService = 0
            }else{
                self.arrServices[indexPath.row].selectedService = 1
            }
            servicesTableView.reloadData()
        }
        else if tableView == packagesTableView {
            self.packageSelectedIndex = nil
            for i in 0..<self.arrPackages.count {
                self.arrPackages[i].selectedService = 0
            }
            
            self.arrPackages[indexPath.row].selectedService = 1
            self.packageSelectedIndex = indexPath.row
            self.selectedPackge = self.arrPackages[indexPath.row]
            packagesTableView.reloadData()
        }
        
        
        //TODO:- OLD Setup
        /*
         let cell = tableView.cellForRow(at: indexPath) as! WashServiceTableViewCell
         
         
         if (tableView == servicesTableView){
         
         if self.arrServices[indexPath.row].selectedService == 1{
         self.arrServices[indexPath.row].selectedService = 0
         }else{
         self.arrServices[indexPath.row].selectedService = 1
         }
         
         tableView.reloadData()
         
         /*if self.serviceSelectedIndex == indexPath.row{
         self.serviceSelectedIndex = nil
         cell.radioImageView.image = nil
         selectedColor = UIColor.clear.cgColor
         selectedBorder = 0.0
         }else{
         self.serviceSelectedIndex = indexPath.row
         }
         
         */
         }else{
         
         cell.radioImageView.image = UIImage(named: "radioButtonChecked")
         var selectedColor = Constants.appColor.cgColor
         var selectedBorder = 2.0
         
         if self.packageSelectedIndex == indexPath.row{
         self.packageSelectedIndex = nil
         cell.radioImageView.image = nil
         selectedColor = UIColor.clear.cgColor
         selectedBorder = 0.0
         }else{
         self.packageSelectedIndex = indexPath.row
         self.selectedPackge = self.arrPackages[indexPath.row]
         }
         
         cell.mainView.layer.borderColor = selectedColor
         cell.mainView.layer.borderWidth = CGFloat(selectedBorder)
         }
         */
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        //        let cell = tableView.cellForRow(at: indexPath) as! WashServiceTableViewCell
        //        cell.radioImageView.image = UIImage(named: "radioButton")
        //        if (tableView == servicesTableView){
        //
        //
        //        }
        //        else {
        //            cell.mainView.layer.borderColor = UIColor.clear.cgColor
        //            cell.mainView.layer.borderWidth = 0
        //        }
    }
    
    @objc func btnReadMoreServiceTableAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServiceDescriptionVC") as! ServiceDescriptionVC
        
        let data = self.arrServices[sender.tag]
        vc.serviceData.name = data.name
        vc.serviceData.description = data.desc
        vc.serviceData.price = data.price
        //        print("Data: ", self.arrServices[sender.tag])
        self.present(vc, animated: true, completion: nil)
        
    }
    
    @objc func btnReadMoreProviderTableAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ServiceDescriptionVC") as! ServiceDescriptionVC
        
        let data = self.arrPackages[sender.tag]
        vc.serviceData.name = data.name
        vc.serviceData.description = data.desc
        vc.serviceData.price = data.price
        //        print("Data: ", self.arrPackages[sender.tag])
        self.present(vc, animated: true, completion: nil)
    }
    
    
    /*
     @objc func btnRadioServicesTableSelction(_ sender: UIButton) {
     
     let index = IndexPath(row: sender.tag, section: 0)
     
     let cell = servicesTableView.cellForRow(at: index) as! WashServiceTableViewCell
     
     
     if self.arrServices[sender.tag].selectedService == 1{
     self.arrServices[sender.tag].selectedService = 0
     }else{
     self.arrServices[sender.tag].selectedService = 1
     }
     
     servicesTableView.reloadData()
     
     /*if self.serviceSelectedIndex == indexPath.row{
     self.serviceSelectedIndex = nil
     cell.radioImageView.image = nil
     selectedColor = UIColor.clear.cgColor
     selectedBorder = 0.0
     }else{
     self.serviceSelectedIndex = indexPath.row
     }
     
     */
     }
     
     @objc func btnRadioProductTableSelction(_ sender: UIButton) {
     
     /*
     let index = IndexPath(row: sender.tag, section: 0)
     
     let cell = packagesTableView.cellForRow(at: index) as! WashServiceTableViewCell
     cell.radioImageView.image = UIImage(named: "radioButtonChecked")
     var selectedColor = Constants.appColor.cgColor
     var selectedBorder = 2.0
     
     if self.packageSelectedIndex == sender.tag{
     self.packageSelectedIndex = nil
     cell.radioImageView.image = nil
     selectedColor = UIColor.clear.cgColor
     selectedBorder = 0.0
     }else{
     self.packageSelectedIndex = sender.tag
     self.selectedPackge = self.arrPackages[sender.tag]
     }
     
     cell.mainView.layer.borderColor = selectedColor
     cell.mainView.layer.borderWidth = CGFloat(selectedBorder)
     */
     //        packagesTableView.reloadData()
     
     self.packageSelectedIndex = nil
     for i in 0..<self.arrPackages.count {
     self.arrPackages[i].selectedService = 0
     }
     
     self.arrPackages[sender.tag].selectedService = 1
     self.packageSelectedIndex = sender.tag
     self.selectedPackge = self.arrPackages[sender.tag]
     packagesTableView.reloadData()
     }
     */
}

extension WashCompanyDetailsViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if (collectionView == carTypeCollectionView) {
            
            return self.arrCarTypes.count
        }
        else {
            if let arrImages = self.companyDetails?.store_imgs {
                
                print("arraImages:\(arrImages)")
                return arrImages.count
                
            }
            
            return 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if (collectionView == carTypeCollectionView){
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "carTypeCell", for: indexPath) as! CompaniesCollectionViewCell
            cell.mainView.layer.cornerRadius = 9.0
            let carType = self.arrCarTypes[indexPath.row]
            cell.brandNameLabel.text = carType.type_name ?? ""
            cell.brandImageView.setImageOfCarType(withUrl: carType.type_img ?? "")
            
            if self.selectedCarTypeIndex == indexPath.row{
                cell.agentImageView.image = UIImage(named: "radioButtonChecked")
                cell.mainView.layer.borderWidth = 2.0
                cell.mainView.layer.borderColor = Constants.appColor.cgColor
            }else{
                cell.agentImageView.image = nil
                cell.mainView.layer.borderWidth = 0.0
                cell.mainView.layer.borderColor = Constants.appColor.cgColor
            }
            
            return cell
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! HomeCollectionViewCell
            cell.imageView.image = nil
            
            if let arrImages = self.companyDetails?.store_imgs{
                cell.imageView.setImageOfApp(withUrl: arrImages[indexPath.row].store_img)
            }
            cell.imageView.layer.cornerRadius = 9.0
            cell.imageView.alpha = 1
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (collectionView == carTypeCollectionView){
            
            return CGSize(width: 200, height: 90)
        }
        else {
            return CGSize(width: 300, height: 150)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if (collectionView == carTypeCollectionView){
            self.selectedCarTypeIndex = indexPath.row
            self.carTypeCollectionView.reloadData()
            
        }else {
            index = indexPath.row
            
            var images = [LightboxImage]()
            
            if let arrImages = self.companyDetails?.store_imgs{
                for imgObj in arrImages{
                    if let imgURL = URL.init(string: imgObj.store_img ?? ""){
                        images.append(LightboxImage(imageURL: imgURL))
                    }
                }
                
                // Create an instance of LightboxController.
                let controller = LightboxController(images: images)
                controller.dynamicBackground = true
                controller.goTo(index, animated: true)
                
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.isNavigationBarHidden = true
                self.present(navigationController, animated: true, completion: nil)
            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if (collectionView == carTypeCollectionView){
            
            let cell = collectionView.cellForItem(at: indexPath) as! CompaniesCollectionViewCell
            cell.agentImageView.image = UIImage(named: "radioButton")
            cell.mainView.layer.borderWidth = 0
            cell.mainView.layer.borderColor = UIColor.clear.cgColor
        }
    }
}


//MARK:- Overide Method
//Vishal Setup
extension WashCompanyDetailsViewController {
    
    @objc func btnAddAction(_ sender: UIButton) {
        
        for i in 0..<self.arrayCategoryDetail.count {
            self.arrayCategoryDetail[i].isSelected = 0
        }
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        let cell = self.tblDescription.cellForRow(at: indexPath) as! DetailCategoryTableCell
        
        if cell.btnSelection.isSelected == true {
            self.arrayCategoryDetail[sender.tag].isSelected = 1
        }
        print("Tag: \(sender.tag)")
        print("Data: \(self.arrayCategoryDetail)")
        self.tblDescription.reloadData()
        
    }
    
    func addObserver() {
        [tblDescription].forEach { (tbl) in
            tbl?.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
            
            self.tblDescriptionHeightCostraint.constant = self.tblDescription.contentSize.height
        }
    }
}
