//
//  goldInsuranceViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/12/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class GoldInsuranceViewController: UIViewController ,UITextFieldDelegate {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var carTypeInformationLabel: UILabel!
    @IBOutlet weak var carTypeCollectionView: UICollectionView!
    @IBOutlet weak var carInfoLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var brandtextField: UITextField!
    @IBOutlet weak var modelLabel: UILabel!
    @IBOutlet weak var modelTextField: UITextField!
    @IBOutlet weak var manufacturingYearLabel: UILabel!
    @IBOutlet weak var manufacturingYearTextField: UITextField!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var insuranceYearLabel: UILabel!
    @IBOutlet weak var insuranceYearTextField: UITextField!
    @IBOutlet weak var additionalServiceLabel: UILabel!
    @IBOutlet weak var additionalServiceDescriptionLabel: UILabel!
    @IBOutlet weak var additionalServiceTableView: CustomTableView!
    @IBOutlet weak var totalAmountSLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tblDescription: UITableView!
    @IBOutlet weak var tblDescriptionHeightConstraint: NSLayoutConstraint!
    
    var companyId = 0
    var arrAddServices = [InsuranceServices]()
    var dataDict = [String : Any]()
    var carTypes = [CarType]()
    var selectedCarType = -1
    var selectedService = -1
    var packagePrice    = 0.0
    var arrayCategory : RentCarDetails?
    var arrayCategoryDetail = [ResellerDetailCategory]()
    
    
//MARK:- FETCH ADDITIONAL SERVICES

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        continueButton.layer.cornerRadius = 9.0
        self.fetchCarTypes()
        self.totalAmountLabel.text = String(format: "%.2f KD", self.packagePrice)
        
        //TODO: - Yash changes
        self.title = "\(self.dataDict["pack_title"] ?? "")"
        self.continueButton.setTitle("Continue".localized(), for: .normal)
        self.descriptionLabel.text = dataDict["description"] as? String ?? ""
        carTypeLabel.text = "Car type".localized()
        carTypeInformationLabel.text = "Choose insurance type that sutes your needs".localized()
        carInfoLabel.text = "Car information".localized()
        brandLabel.text = "Brand".localized()
        modelLabel.text = "Model".localized()
        manufacturingYearLabel.text = "Manufacturing year".localized()
        priceLabel.text = "Price in market".localized()
        additionalServiceLabel.text = "Additional Serice".localized()
        additionalServiceDescriptionLabel.text = "Choose insurance type that sutes your needs".localized()
        totalAmountSLabel.text = "Total Amount".localized()
        
        self.registerXib()
    }
    
    
//MARK:- FETCH ADDITIONAL SERVICES
    
    private func fetchCarTypes(){
        
        self.showLoader()
        
        let strURL = Constants.GET_CAR_TYPES
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {
                    self?.carTypes = try JSONDecoder().decode([CarType].self, from: jsonData)
                    self?.carTypeCollectionView.reloadData()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.fetchAdditionalServices()
        }
    }
    
    private func fetchAdditionalServices(){
        
        let strURL = Constants.GET_INSURANCE_ADD_SERVICES + "\(self.companyId)"
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {
                    self?.arrAddServices = try JSONDecoder().decode([InsuranceServices].self, from: jsonData)
                    self?.additionalServiceTableView.reloadData()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.getDetailDataAPI()
        }
    }
    
    private func getDetailDataAPI() {
        
        let strURL = Constants.GET_INSTANCE_PACKAGE_DETAIL+"\(self.dataDict["ins_pack_id"] ?? 0)"
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {

                    self?.arrayCategory = try JSONDecoder().decode(RentCarDetails.self, from: jsonData)
                    
                    var dicDetailCategory: [ResellerDetailCategory]
                    if let detailCategory = self?.arrayCategory?.car?.detail_category {
                        dicDetailCategory = detailCategory
                        for i in 0..<dicDetailCategory.count {
                            dicDetailCategory[i].isSelected = 0
                        }
                        self?.arrayCategory?.car?.detail_category = dicDetailCategory
                        self?.arrayCategoryDetail = dicDetailCategory
                        self?.tblDescription.reloadData()
                    }
                    
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}

// MARK: - Actions
extension GoldInsuranceViewController {
    
    @IBAction func continueAction(_ sender: UIButton) {
        self.handleValidation()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? UploadOptionsViewController{
            vc.bIsInsurance = true
    
            self.dataDict["add_serv_id"] = ["\(self.arrAddServices[selectedService].id ?? 0)"]
            self.dataDict["car_type_id"] = "\(self.carTypes[selectedCarType].id ?? 0)"
            self.dataDict["brand"]       = "\(self.brandtextField.text ?? "")"
            self.dataDict["model"]       = "\(self.modelTextField.text ?? "")"
            self.dataDict["insurance_years"]  = "\(self.insuranceYearTextField.text ?? "")"
            self.dataDict["price_in_market"]  = "\(self.priceTextField.text ?? "")"
            self.dataDict["manufacture_year"] = "\(self.manufacturingYearTextField.text ?? "")"
            self.dataDict["service_name"]     = "\(self.arrAddServices[selectedService].serv_name ?? "")"
            self.dataDict["service_price"]    = String(format: "%.2f KD", self.arrAddServices[selectedService].serv_charge ?? 0.0)
            self.dataDict["total_price"]    = self.packagePrice + (self.arrAddServices[selectedService].serv_charge ?? 0.0)
            vc.dataDict     = self.dataDict
        }
    }
    
    private func handleValidation(){
        if self.selectedCarType == -1{
            self.showDefaultAlert(title: "Car Type".localized(), message: "Please select car type".localized())
            return
        }
        
        if self.brandtextField.text == ""{
            self.showDefaultAlert(title: "Brand".localized(), message: "Please enter brand name".localized())
            return
        }
        
        if self.modelTextField.text == ""{
            self.showDefaultAlert(title: "Model".localized(), message: "Please enter model name".localized())
            return
        }
        
        if self.manufacturingYearTextField.text == ""{
            self.showDefaultAlert(title: "Manufacture".localized(), message: "Please enter manufacturing year".localized())
            return
        }
        
        if self.priceTextField.text == ""{
            self.showDefaultAlert(title: "Price".localized(), message: "Please enter price in market".localized())
            return
        }
        
        if self.insuranceYearTextField.text == ""{
            self.showDefaultAlert(title: "Insurance".localized(), message: "Please enter insurance years".localized())
            return
        }
        
        if self.selectedService == -1{
            self.showDefaultAlert(title: "Service".localized(), message: "Please select atleast one service".localized())
            return
        }
        
        if Constants.bIsUserLoggedIn() == false{
            self.showAlert(message: "Please login to your account".localized(), status: "Login".localized())
                   return
               }
        self.performSegue(withIdentifier: "showUpload", sender: self)

    }
}

extension GoldInsuranceViewController: UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tblDescription {
            return self.arrayCategoryDetail.count
        }
        return self.arrAddServices.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tblDescription {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCategoryTableCell", for: indexPath) as! DetailCategoryTableCell
            
            let dict = self.arrayCategoryDetail[indexPath.row]
            cell.lblHeader.text = dict.category_name ?? ""
            cell.lblDescription.text = dict.detail_description ?? ""
            
            if dict.isSelected == 0 {
                cell.btnSelection.isSelected = true
                cell.vwDescription.isHidden = true
            }
            else {
                cell.btnSelection.setImage(UIImage(named: "ic_minus"), for: .normal)
                cell.btnSelection.isSelected = false
                cell.vwDescription.isHidden = false
            }
            cell.btnSelection.tag = indexPath.row
            cell.btnSelection.addTarget(self, action: #selector(btnAddAction(_:)), for: .touchUpInside)
            
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! GoldInsuranceTableViewCell
        let serviceObj = self.arrAddServices[indexPath.row]
        cell.serviceNameLabel.text = serviceObj.serv_name ?? ""
        cell.descriptionLabel.text = serviceObj.serv_desc ?? ""
        if let price = serviceObj.serv_charge {
            cell.priceLabel.text = price <= 0 ? "No price".localized() : String(format: "%.2f KD", price)
        } else {
//           cell.priceLabel.text = "No price".localized()
            cell.priceLabel.text = ""
        }
        
        cell.mainView.layer.cornerRadius = 9.0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == self.tblDescription {
            return UITableView.automaticDimension
        }
        return 120.0
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.additionalServiceTableView {
            
            let cell = tableView.cellForRow(at: indexPath) as! GoldInsuranceTableViewCell
            cell.radioImageView.image = UIImage(named: "radioButtonChecked")
            cell.mainView.layer.borderColor = Constants.appColor.cgColor
            cell.mainView.layer.borderWidth = 2.0
            if self.additionalServiceTableView == tableView{
                self.selectedService = indexPath.row
                self.totalAmountLabel.text = String(format: "%.2f KD", self.packagePrice + (self.arrAddServices[indexPath.row].serv_charge ?? 0.0))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if tableView == self.additionalServiceTableView {
            let cell = tableView.cellForRow(at: indexPath) as! GoldInsuranceTableViewCell
            cell.radioImageView.image = UIImage(named: "radioButton")
            cell.mainView.layer.borderColor = UIColor.clear.cgColor
            cell.mainView.layer.borderWidth = 0
        }
    }
}

extension GoldInsuranceViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.carTypes.count
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "carTypeCell", for: indexPath) as! CompaniesCollectionViewCell
        let carObj = self.carTypes[indexPath.row]
        cell.brandNameLabel.text = carObj.type_name
        
        cell.mainView.layer.cornerRadius = 9.0
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 200, height: 90)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! CompaniesCollectionViewCell
        cell.agentImageView.image = UIImage(named: "radioButtonChecked")
        cell.mainView.layer.borderWidth = 2.0
        cell.mainView.layer.borderColor = Constants.appColor.cgColor
        if self.carTypeCollectionView == collectionView{
            self.selectedCarType = indexPath.row
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CompaniesCollectionViewCell
        cell.agentImageView.image = UIImage(named: "radioButton")
        cell.mainView.layer.borderWidth = 0
        cell.mainView.layer.borderColor = UIColor.clear.cgColor
    }
    
}

//MARK:- Overide Method
//Vishal Setup
extension GoldInsuranceViewController {
    
    func registerXib() {
        self.tblDescription.register(UINib(nibName: "DetailCategoryTableCell", bundle: nil), forCellReuseIdentifier: "DetailCategoryTableCell")
        addObserver()
    }
    
    @objc func btnAddAction(_ sender: UIButton) {
        
        for i in 0..<self.arrayCategoryDetail.count {
            self.arrayCategoryDetail[i].isSelected = 0
        }
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        let cell = self.tblDescription.cellForRow(at: indexPath) as! DetailCategoryTableCell
        
        if cell.btnSelection.isSelected == true {
            self.arrayCategoryDetail[sender.tag].isSelected = 1
        }
        print("Tag: \(sender.tag)")
        print("Data: \(self.arrayCategoryDetail)")
        self.tblDescription.reloadData()
        
    }
    
    func addObserver() {
        [tblDescription].forEach { (tbl) in
            tbl?.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
            
            self.tblDescriptionHeightConstraint.constant = self.tblDescription.contentSize.height
        }
    }
}


