//
//  insuranceCompaniesViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/2/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class InsuranceCompaniesViewController: UIViewController , UISearchBarDelegate {
    
    
    @IBOutlet weak var insuranceCompaniesCollectionView: UICollectionView!
    var logos = [#imageLiteral(resourceName: "loader"),#imageLiteral(resourceName: "gig-Group-ENG-sm"),#imageLiteral(resourceName: "unnamed"),#imageLiteral(resourceName: "282_Warba-Insurance-Company-Logo_-_Qu80_RT1600x1024-_OS500x500-_RD500x500-"),#imageLiteral(resourceName: "4b9a5913-89a4-4203-ab0b-c3d9ee49f7cb_"),#imageLiteral(resourceName: "unnamed (4)")]
    var titles = ["Kuwait insurance", "GIG", "Al Ahlia", "Warba", "Aman", "Bni"]
    var arrCompanies = [InsuranceCompany]()
    var arrSearchCompanies = [InsuranceCompany]()
    var strTitle = ""
    var bIsSearch = false
    
//MARK : VIEW DELEGATES
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.getInsuranceCompanies()
        self.navigationItem.title = strTitle
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    //MARK: GET INSURANCE COMPANIES
    private func getInsuranceCompanies(){
        self.showLoader()
        APIManager.shared.fetchRequestData(strURL: Constants.GET_INSURANCE_COMPANIES, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData)  in
            if let jsonData = responseData {
                do {
                    self?.arrCompanies =  try JSONDecoder().decode([InsuranceCompany].self, from: jsonData)
                    self?.arrSearchCompanies = self?.arrCompanies ?? []
                    self?.insuranceCompaniesCollectionView.reloadData()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }
    
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Cancel".localized()
        // searchController.searchBar.setValue("Cancel", forKey: "_cancelButtonText")
        searchController.searchBar.tintColor = UIColor.black
        searchController.searchBar.barTintColor = Constants.grayColor
        searchController.searchBar.placeholder = "Search".localized()
        searchController.hidesNavigationBarDuringPresentation = false
        self.present(searchController,animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        dismiss(animated: true, completion: nil)
        bIsSearch = true
        let filterArray = self.arrSearchCompanies.filter({ ($0.name ?? "").contains(searchBar.text ?? "") })
        
        if filterArray.count > 0{
            self.arrCompanies = filterArray
            self.insuranceCompaniesCollectionView.reloadData()
        }
    }
    
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentString = (searchBar.text!.lowercased() as NSString).replacingCharacters(in: range, with: text)
        
        if currentString.count > 0 {
            self.bIsSearch = true
            let filterArray = self.arrSearchCompanies.filter({ (($0.name ?? "").lowercased()).contains(currentString)})
            
            if filterArray.count > 0{
                self.arrCompanies = filterArray
                self.insuranceCompaniesCollectionView.reloadData()
            }
            else{
                self.arrCompanies.removeAll()
                self.insuranceCompaniesCollectionView.reloadData()
            }
        }
        else {
            self.arrCompanies = self.arrSearchCompanies
            self.insuranceCompaniesCollectionView.reloadData()
        }
        
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        bIsSearch = false
        self.arrCompanies = self.arrSearchCompanies
        self.insuranceCompaniesCollectionView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        bIsSearch = false
        self.arrCompanies = self.arrSearchCompanies
        self.insuranceCompaniesCollectionView.reloadData()
    }
    
}

extension InsuranceCompaniesViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrCompanies.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "companyCell", for: indexPath) as! WashCompanyCollectionViewCell
        cell.mainView.layer.cornerRadius = 10.0
        cell.companyImageView.setImageOfApp(withUrl: arrCompanies[indexPath.row].image)
        cell.companyNameLabel.text = self.arrCompanies[indexPath.row].name
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewWidth = collectionView.bounds.width / 2
        return CGSize(width: collectionViewWidth, height: 200)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showDetails", sender: indexPath)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetails"{
            guard let indexPath = sender as? IndexPath else {return}
            let vc = segue.destination as! InsuranceCompanyDetailsViewController
            
            //TODO: - Yash changes
          //  vc.companyImage = logos[indexPath.row]
            vc.strCompanyImageURL = arrCompanies[indexPath.row].image ?? ""
            
            vc.insuranceCompany = self.arrCompanies[indexPath.row]
//            vc.compantName = self.arrCompanies[indexPath.row].name ?? ""
//            vc.companyDesc = self.arrCompanies[indexPath.row].description ?? ""
//            vc.companyId = self.arrCompanies[indexPath.row].ins_comp_id ?? 0
        }
    }
}
