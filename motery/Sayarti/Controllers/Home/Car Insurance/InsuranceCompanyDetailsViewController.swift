//
//  insuranceCompanyDetailsViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/9/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class InsuranceCompanyDetailsViewController: UIViewController , UIScrollViewDelegate {
    
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var insuranceTypeLabel: UILabel!
    @IBOutlet weak var insuranceDescriptionLabel: UILabel!
    @IBOutlet weak var insuranceColletionView: UICollectionView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var btnFavorites: UIButton!
    
    var type: String = ""
    let types = ["Shamil" , "Regular"]
    var insuranceType = [InsuranceType]()
    var companyImage  = UIImage()
    var strCompanyImageURL = ""
//    var compantName   = ""
//    var companyId     = 0
//    var companyDesc   = ""
    var insuranceCompany = InsuranceCompany()
    private var imageView = UIImageView()
    var selectedInsuranceType = -1

    
    
//MARK :- VIEW DELEGATES
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        imageView.removeFromSuperview()
    }
    
    private func setupData(){
        self.fetchInsuranceTypes()
        navigationItem.title = self.insuranceCompany.name ?? ""
        //TODO: - Yash changes
       // imageView.image = companyImage
        imageView.setImageOfApp(withUrl: strCompanyImageURL)
        
        self.descriptionLabel.text = self.insuranceCompany.description ?? ""
        
        continueButton.layer.cornerRadius = 9.0
        optionView.layer.cornerRadius = 25.0
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.insuranceTypeLabel.text = "Insurance type".localized()
        self.insuranceDescriptionLabel.text = "Choose insurance type that sutes your needs".localized()
        self.continueButton.setTitle("Continue".localized(), for: .normal)
        
    }
    
    private func setupUI() {
        
        // Initial setup for image for Large NavBar state since the the screen always has Large NavBar once it gets opened
        guard let navigationBar = self.navigationController?.navigationBar else { return }
        
        navigationBar.addSubview(imageView)
        //imageView.layer.cornerRadius = Const.ImageSizeForLargeState / 2
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor, constant: -Const.ImageRightMargin),
            imageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Const.ImageBottomMarginForLargeState),
            imageView.heightAnchor.constraint(equalToConstant: Const.ImageSizeForLargeState),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor)
            ])
    }
    
    private func moveAndResizeImageForPortrait() {
        guard let height = navigationController?.navigationBar.frame.height else { return }
        
        let coeff: CGFloat = {
            let delta = height - Const.NavBarHeightSmallState
            let heightDifferenceBetweenStates = (Const.NavBarHeightLargeState - Const.NavBarHeightSmallState)
            return delta / heightDifferenceBetweenStates
        }()
        
        let factor = Const.ImageSizeForSmallState / Const.ImageSizeForLargeState
        
        let scale: CGFloat = {
            let sizeAddendumFactor = coeff * (1.0 - factor)
            return min(1.0, sizeAddendumFactor + factor)
        }()
        
        // Value of difference between icons for large and small states
        let sizeDiff = Const.ImageSizeForLargeState * (1.0 - factor) // 8.0
        
        let yTranslation: CGFloat = {
            /// This value = 14. It equals to difference of 12 and 6 (bottom margin for large and small states). Also it adds 8.0 (size difference when the image gets smaller size)
            let maxYTranslation = Const.ImageBottomMarginForLargeState - Const.ImageBottomMarginForSmallState + sizeDiff
            return max(0, min(maxYTranslation, (maxYTranslation - coeff * (Const.ImageBottomMarginForSmallState + sizeDiff))))
        }()
        
        let xTranslation = max(0, sizeDiff - coeff * sizeDiff)
        
        imageView.transform = CGAffineTransform.identity
            .scaledBy(x: scale, y: scale)
            .translatedBy(x: xTranslation, y: yTranslation)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        moveAndResizeImageForPortrait()
    }
    

//MARK:- FETCH INSURANCE TYPES

    private func fetchInsuranceTypes(){
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_INSURANCE_TYPES, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {
                    self?.insuranceType = try JSONDecoder().decode([InsuranceType].self, from: jsonData)
                    self?.insuranceColletionView.reloadData()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.fetchInsuranceCompanyDetails()
        }
        
    }
    
    private func fetchInsuranceCompanyDetails(){
        
        let strURL = Constants.GET_INSURANCE_DETAILS + "\(self.insuranceCompany.ins_comp_id ?? 0)"
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {
                    self?.insuranceCompany = try JSONDecoder().decode(InsuranceCompany.self, from: jsonData)
                    self?.navigationItem.title = self?.insuranceCompany.name ?? ""
                    self?.btnFavorites.setImage(self?.insuranceCompany.favorite ?? false ? #imageLiteral(resourceName: "addToFavorite") : #imageLiteral(resourceName: "Favorite"), for: .normal)
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }

    
    private func setCompanyFavorite(){
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_INSURANCE_FAVORITES , bIsPostRequest: true, bIsShowAlert: true, parameter: ["ins_comp_id":"\(self.insuranceCompany.ins_comp_id ?? 0)"]) { [weak self] (responseData) in
            
            if let _ = responseData {
                self?.insuranceCompany.favorite = !(self?.insuranceCompany.favorite ?? false)
                self?.btnFavorites.setImage(self?.insuranceCompany.favorite ?? false ? #imageLiteral(resourceName: "addToFavorite") : #imageLiteral(resourceName: "Favorite"), for: .normal)
            }
            self?.hideLoader()
        }
    }
    
    private func setCallRequest(){
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_INSURANCE_CALL , bIsPostRequest: true, bIsShowAlert: true, parameter: ["company_id":"\(self.insuranceCompany.ins_comp_id ?? 0)"]) { [weak self] (responseData) in
            
            if let jsonData = responseData {
                print("JSON : ",jsonData.debugDescription)
                do {
                    if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String : Any]
                    {
                        self?.showDefaultAlert(title: "", message: json["message"] as! String)
                    }
                }
                catch {
                    
                }
            }
            self?.hideLoader()
        }
    }
    
    //Vishal
    func showAlertAction() {
        
        let dialogMessage = UIAlertController(title: "Confirm".localized(), message: "Are you sure you want to contact with this dealer?".localized(), preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: { (action) -> Void in
            
            self.setCallRequest()
            print("Ok")
        })
        
        let cancel = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action) -> Void in }
        
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
}

// MARK:- Action
extension InsuranceCompanyDetailsViewController {
    
    @IBAction func shareAction(_ sender: UIButton) {
        
        let vc = UIActivityViewController(activityItems: ["Mercedes"], applicationActivities: [])
        self.present(vc, animated: true)
    }
    
    @IBAction func favoriteAction(_ sender: UIButton) {
        if Constants.bIsUserLoggedIn() == false{
            self.showAlert(message: "Please login to your account".localized(), status: "Login".localized())
                   return
               }
        self.setCompanyFavorite()
    }
    
    @IBAction func locationAction(_ sender: UIButton) {
        
        let longitude : Double = Double(self.insuranceCompany.longtitude ?? "0.0") ?? 47.991876
        let latitude : Double = Double(self.insuranceCompany.latitude ?? "0.0") ?? 29.380192
        
        if let url = URL(string: "http://maps.google.com/maps?saddr=\(latitude),\(longitude)") {
            
            UIApplication.shared.open(url)
        }
        else if let url2 = URL(string: "http://maps.apple.com/maps?saddr=\((latitude)),\((longitude))") {
            UIApplication.shared.open(url2)
        }
    }
    
    @IBAction func callAction(_ sender: UIButton) {
        
        self.showAlertAction()
        
        //OLD
//        if let url = URL(string: "tel://\(self.insuranceCompany.contact ?? "")") {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        }
        
        //YASH
//        self.setCallRequest()
        
    }
    
    @IBAction func continueAction(_ sender: UIButton) {
        // TODO: - Yash changes
        
        if type != ""{
            self.performSegue(withIdentifier: "showShamil", sender: self)
        }else{
            self.showDefaultAlert(title: "", message: "Please choose atleast one insurance company".localized())
        }
        
         
        //if (type == "Shaamil"){
            //Yash comment:  "showShamil" is only uncommented , "showRegular" alreadt commited
            
        //}else{
          //  self.performSegue(withIdentifier: "showRegular", sender: self)
       // }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if let shamilVC = segue.destination as? ShamilInstanceViewController{
            shamilVC.strTitle  = self.insuranceType[self.selectedInsuranceType].name ?? ""
            shamilVC.compnayId = self.insuranceCompany.ins_comp_id ?? 0
            shamilVC.insuranceImage = self.strCompanyImageURL
            
            let dataDict = ["ins_comp_id": "\(self.insuranceCompany.ins_comp_id ?? 0)","insurance_type": "\(self.insuranceType[self.selectedInsuranceType].id ?? 0)","agency_name": "\(self.insuranceCompany.name ?? "")", "car_name" : "\(self.insuranceType[self.selectedInsuranceType].name ?? "")"]
               
            shamilVC.dataDict  = dataDict
        }
    }
    
}

extension InsuranceCompanyDetailsViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.insuranceType.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "insuranceCell", for: indexPath) as! CategoryCollectionViewCell
        cell.classNameLabel.text = self.insuranceType[indexPath.row].name ?? ""
        cell.classYearLabel.text = "Do not warry anymore".localized()
        cell.mainView.layer.cornerRadius = 9.0
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewWidth = collectionView.bounds.width / 2
        return CGSize(width: collectionViewWidth , height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! CategoryCollectionViewCell
        cell.classImageView.image = UIImage(named: "radioButtonChecked")
        type = cell.classNameLabel.text!
        cell.mainView.layer.borderWidth = 2.0
        cell.mainView.layer.borderColor = Constants.appColor.cgColor
        self.selectedInsuranceType = indexPath.row
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! CategoryCollectionViewCell
        cell.classImageView.image = UIImage(named: "radioButton")
        cell.mainView.layer.borderWidth = 0
        cell.mainView.layer.borderColor = UIColor.clear.cgColor
    }
}
