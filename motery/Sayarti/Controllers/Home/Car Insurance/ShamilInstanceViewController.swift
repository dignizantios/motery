//
//  shamilInstanceViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/11/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class ShamilInstanceViewController: UIViewController {
    
    @IBOutlet weak var shamilLabel: UILabel!
    @IBOutlet weak var optionsTableView: CustomTableView!
    @IBOutlet weak var continueButton: UIButton!
    
    var arrShamils = [InsuranceShamil]()
    var selectedShamil = -1
    var compnayId = 0
    var dataDict = [String : Any]()
    var strTitle = ""
    var insuranceImage = ""
    
//MARK:- VIEW DELEGATES

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.strTitle
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        continueButton.layer.cornerRadius = 10.0
        self.continueButton.setTitle("Continue".localized(), for: .normal)
        self.fetchShamils()
    }
    
    
    //MARK:- FETCH SHAMIL
    
    private func fetchShamils(){
        
        self.showLoader()
        
        //TODO: - Yash Changes
        let id = self.dataDict["insurance_type"] as! String
        print("id : \(id)")
        let strURL = Constants.GET_INSURANCE_PACKAGES + "\(self.compnayId)/\(id)"
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData)  in
            
            if let jsonData = responseData {
                do {
                    self?.arrShamils = try JSONDecoder().decode([InsuranceShamil].self, from: jsonData)
                    self?.optionsTableView.reloadData()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }
    
    
    
}


// MARK: - Actions

extension ShamilInstanceViewController {
    
    @IBAction func continueAction(_ sender: UIButton) {
        
        self.checkLoginStatus { (isLoggedIn) in
            if isLoggedIn {
                if self.selectedShamil >= 0{
                    self.performSegue(withIdentifier: "showGoldInsurance", sender: self)
                }else{
                    self.showDefaultAlert(title: "Package".localized(), message: "Please select atleast one package".localized())
                }
            }
        }
       
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showGoldInsurance"{
            if let goldVC = segue.destination as? GoldInsuranceViewController{
                self.dataDict["ins_pack_id"] = "\(self.arrShamils[self.selectedShamil].ins_pack_id ?? 0)"
                self.dataDict["description"] = "\(self.arrShamils[self.selectedShamil].pack_desc_lg ?? "")"
                self.dataDict["car_image"] = self.insuranceImage
                self.dataDict["pack_title"] = "\(self.arrShamils[self.selectedShamil].pack_title ?? "")"
                goldVC.companyId    = self.compnayId
                
                print("dataDict : \(self.dataDict)")
                
                goldVC.dataDict     = self.dataDict
                goldVC.packagePrice = self.arrShamils[self.selectedShamil].price ?? 0.0
            }
        }
    }
    
    
}

extension ShamilInstanceViewController: UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrShamils.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "shamilCell", for: indexPath) as! ShamilOptionTableViewCell
        cell.optionNameLabel.text = self.arrShamils[indexPath.row].pack_title ?? ""
        cell.optionDescriptionLabel.text = self.arrShamils[indexPath.row].pack_desc_sm ?? ""
        cell.lblPrice.text = String(format: "%.2f KD", self.arrShamils[indexPath.row].price ?? 0.0)
        cell.mainView.layer.cornerRadius = 10.0
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! ShamilOptionTableViewCell
        cell.mainView.layer.borderColor = Constants.appColor.cgColor
        cell.mainView.layer.borderWidth = 2.0
      //  cell.radioImageView.image = UIImage(named: "radioButtonChecked")
        self.selectedShamil = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! ShamilOptionTableViewCell
//cell.radioImageView.image = UIImage(named: "radioButton")
        cell.mainView.layer.borderColor = UIColor.clear.cgColor
        cell.mainView.layer.borderWidth = 0
    }
}
