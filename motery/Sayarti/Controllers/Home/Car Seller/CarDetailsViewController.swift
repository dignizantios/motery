//
//  classViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/4/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Lightbox

class CarDetailsViewController: UIViewController {
    
    var animate: Bool = true
    var index: Int = 0
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var describtionLabel: UILabel!
    @IBOutlet weak var classImageView: UIImageView!
    @IBOutlet weak var attributesCollectionView: UICollectionView!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var serviceTableView: CustomTableView!
    @IBOutlet weak var serviceDescribtionLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var colorCollectionView: UICollectionView!
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var txtToDate: UITextField!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnFinanceOutlet: UIButton!
    @IBOutlet weak var lblPriceTitle: UILabel!
    @IBOutlet weak var lblPriceValue: UILabel!
    @IBOutlet weak var tblDetailCategory: UITableView!
    @IBOutlet weak var tblDetailCategoryHeightConstraint: NSLayoutConstraint!
    
    
    var startingFrame      = CGRect()
    var car                = Car()
    var bIsCarRent         = false
    var bIsUsedCar         = false
    var rentCar            = RentCar()
    var rentCarDetails     : RentCarDetails?
    let fromDatePickerView = UIDatePicker()
    let toDatePickerView   = UIDatePicker()
    var strFromDate        = ""
    var strToDate          = ""
    var strCarId           = ""
    var agencyName         = ""
    var resellerCar        = ResellerCar()
    var bIsReseller        = false
    var resellerSelectedColor   : ResellerColor?
    var resellerSelectedService : ResellerService?
    var resellerAgencyId      = ""

    var resellerCarImages  = [ResellerImage]()
    var nColorSelectedIndex = 0
    
   
    let carImages = [#imageLiteral(resourceName: "image-2"),#imageLiteral(resourceName: "image-3"),#imageLiteral(resourceName: "image-1")]
    var attributes = [String]() //["4 seats", "Automatic", "120 hp", "260 km/h"]
    let attributesImages = [#imageLiteral(resourceName: "Seats"),#imageLiteral(resourceName: "Gears"),#imageLiteral(resourceName: "HP"),#imageLiteral(resourceName: "Speed")]
    let services = ["Request test drive", "Request car repair", "book this car", "Request car book"]
    let servicesImages = [#imageLiteral(resourceName: "service1"),#imageLiteral(resourceName: "Tire"),#imageLiteral(resourceName: "class1"),#imageLiteral(resourceName: "Book")]
    
    let carCollors = [#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1),#colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1),#colorLiteral(red: 0.9994240403, green: 0.9855536819, blue: 0, alpha: 1),#colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1),#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1),#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1)]
    
    var arrayCategoryDetail = [ResellerDetailCategory]()
    
    
//MARK:- VIEW DELEGATES
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.fetchCarDetails()
        self.fetchResellerCarDetails()
        
        self.btnFinanceOutlet.setTitle("Finance".localized(), for: .normal)
        if self.bIsReseller{
            self.submitButton.setTitle("Continue".localized(), for: .normal)
//            self.submitButton.isHidden = true
        }else if self.bIsUsedCar{
            self.colorCollectionView.heightAnchor.constraint(equalToConstant: 0.0).isActive = true
        }
        self.registerXib()
        self.addObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        [tblDetailCategory].forEach { (tbl) in
//            tbl?.removeObserver(self, forKeyPath: "contentSize")
        }
    }
    
    func registerXib() {
        self.tblDetailCategory.register(UINib(nibName: "DetailCategoryTableCell", bundle: nil), forCellReuseIdentifier: "DetailCategoryTableCell")
    }
    
    private func setupView(){
        
        lblPriceTitle.text = "Price : ".localized()
        photoLabel.text = "Photos".localized()
        self.servicesLabel.text = "Services".localized()
        self.serviceDescribtionLabel.text = "Choose desired request from the list".localized()

        self.title = (self.bIsCarRent || self.bIsUsedCar) ? self.rentCar.name ?? "" : resellerCar.name
        classImageView.image = car.image
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        submitButton.layer.cornerRadius = 8.0
        btnFinanceOutlet.layer.cornerRadius = 8.0
        
        if self.bIsReseller{
            self.lblPrice.text = "" //String.init(format: "%.2f KD", (resellerCar.price ?? 0.0))
            describtionLabel.text = resellerCar.description
        }else{
            describtionLabel.text = rentCar.car_desc
            serviceTableView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            servicesLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
            serviceDescribtionLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
            classImageView.heightAnchor.constraint(equalToConstant: 0.0).isActive = true
        }
        
        if(self.bIsCarRent){
            colorCollectionView.heightAnchor.constraint(equalToConstant: 0.0).isActive = true
            fromDatePickerView.datePickerMode = .date
            fromDatePickerView.minimumDate = Date()
            toDatePickerView.datePickerMode = .date
            toDatePickerView.minimumDate = Date()
            txtFromDate.inputView = fromDatePickerView
            txtToDate.inputView = toDatePickerView
            
            fromDatePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            toDatePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
            
            let numberToolbar = UIToolbar(frame:CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
            numberToolbar.barStyle = .default
            numberToolbar.items = [
                UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
                UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(actionKeyboardDoneButton))]
            numberToolbar.tintColor = .black
            numberToolbar.sizeToFit()
            txtFromDate.inputAccessoryView = numberToolbar
            txtToDate.inputAccessoryView   = numberToolbar
        }
        
        if(self.bIsUsedCar || self.bIsReseller){
            txtFromDate.heightAnchor.constraint(equalToConstant: 0.0).isActive = true
            txtToDate.heightAnchor.constraint(equalToConstant: 0.0).isActive = true
            lblDate.heightAnchor.constraint(equalToConstant: 0.0).isActive = true
        }
        
        //Vishal
        [lblPriceTitle, lblPriceValue].forEach { (lbl) in
            lbl?.textColor = UIColor.black
            lbl?.layer.shadowColor = UIColor.lightGray.cgColor
            lbl?.layer.shadowColor = UIColor.black.cgColor
            lbl?.layer.shadowRadius = 1.0
            lbl?.layer.shadowOpacity = 1.0
            lbl?.layer.shadowOffset = CGSize(width: 1, height: 1)
            lbl?.layer.masksToBounds = false
        }
       
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        if sender == self.fromDatePickerView{
            self.setUpFromDateText()
        }else{
            self.setUpToDateText()
        }
    }
    
    @objc func actionKeyboardDoneButton() {
        self.view.endEditing(true)
    }
    
    func animateHome(){
        
        UIView.animate(withDuration: 1.5, delay: 0, usingSpringWithDamping: 0.7 , initialSpringVelocity: 0 ,options: .curveEaseInOut, animations: {
            
            self.classImageView.transform = .identity
        }, completion: nil)
    }
    
//MARK:- FETCH RESELLER CAR DETAILS

    private func fetchResellerCarDetails(){
        if !self.bIsReseller {return}
        self.showLoader()

        let strURL = Constants.GET_RESELLER_CAR + "\(self.resellerCar.id ?? 0)"
        
        APIManager.shared.fetchRequestData(strURL: strURL , bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self](responseData) in
            
            if let jsonData = responseData{
                do {
                    self?.resellerCar = try JSONDecoder().decode(ResellerCar.self, from: jsonData)
                    print("Services: ", self?.resellerCar.services?.count)
                    print("Service value: ", self?.resellerCar.services?[0])
                    
                    var dicDetailCategory: [ResellerDetailCategory]
                    if let detailCategory = self?.resellerCar.detail_category {
                        dicDetailCategory = detailCategory
                        for i in 0..<dicDetailCategory.count {
                            dicDetailCategory[i].isSelected = 0
                        }
                        print("Data: ", dicDetailCategory)
                        self?.resellerCar.detail_category = dicDetailCategory
                        self?.arrayCategoryDetail = dicDetailCategory
                        self?.tblDetailCategory.reloadData()
                    }
                }
                catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.fetchResellerCarImages()
        }
    }
    
    private func fetchResellerCarImages(){
        if !self.bIsReseller {return}
        self.showLoader()
        
        let strURL = Constants.GET_RESELLER_CAR_IMAGES + "\(self.resellerCar.id ?? 0)"
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self](responseData) in
            
            if let jsonData = responseData{
                do {
                    self?.resellerCarImages = try JSONDecoder().decode([ResellerImage].self, from: jsonData)
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.setupResellerCarAttributes()
            self?.hideLoader()
        }
    }
    
    private func setupResellerCarAttributes(){

        if let imgURL = URL.init(string: self.resellerCar.image ?? ""){
            self.classImageView.af_setImage(withURL: imgURL, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main , imageTransition: UIImageView.ImageTransition.noTransition , runImageTransitionIfCached: true) { [weak self](_) in
                if (self?.animate ?? true){
                    self?.animate = false
                    self?.classImageView.transform = .init(translationX: self!.view.frame.width, y: 0)
                    self?.animateHome()
                }
            }
        }
        
        self.attributes.append("\(self.resellerCar.seats ?? 0) " + "seats".localized())
        if let strAuto = self.resellerCar.automatic{
            if strAuto == 1{
                self.attributes.append("Automatic".localized())
            }else{
                self.attributes.append("Manual".localized())
            }
        }
        
        if let nPower = self.resellerCar.power{
            self.attributes.append("\(nPower) " + "hp".localized())
        }
        
        if let maxSpeed = self.resellerCar.max_speed{
            if maxSpeed > 0{
                self.attributes.append("\(maxSpeed) " + "km/h".localized())
            }
        }
        self.attributesCollectionView.reloadData()
        
        if(self.resellerCarImages.count == 0){
            self.photoLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.photosCollectionView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }else{
            self.photosCollectionView.reloadData()
        }
        
        if(self.resellerCar.services?.count == 0){
            self.servicesLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.serviceTableView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.serviceDescribtionLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }else{
            self.serviceTableView.reloadData()
        }
        
        if self.resellerCar.colors?.count == 0 {
            self.colorCollectionView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }else{
            self.colorCollectionView.reloadData()
        }
         if self.resellerCar.colors?.count ?? 0 > 0{
             self.resellerSelectedColor = self.resellerCar.colors?[0]
        }
        
        //TODO:- Change by JD - Vishal
        
        if let price = self.resellerCar.price {
            self.lblPriceValue.text = price <= 0 ? "No price".localized() : String(format: "%.2f KD", price)
        } else {
//            self.lblPriceValue.text = "No price".localized()
            self.lblPriceValue.text = "".localized()
        }
    }
    
    
    
//MARK:- FETCH CAR DETAILS
    
    private func fetchCarDetails(){
        if !self.bIsCarRent && !self.bIsUsedCar{return}
        
        self.showLoader()
        
        let strURL = (self.bIsCarRent ? Constants.GET_RENT_CAR_DETAIL : Constants.GET_USED_CAR_DETAIL) + "\(self.rentCar.id ?? 0)"
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self](responseData) in
            
            if let jsonData = responseData{
                do {
                    self?.rentCarDetails = try JSONDecoder().decode(RentCarDetails.self, from: jsonData)
                    
                    
                    var dicDetailCategory: [ResellerDetailCategory]
                    if let detailCategory = self?.rentCarDetails?.car?.detail_category {
                        dicDetailCategory = detailCategory
                        for i in 0..<dicDetailCategory.count {
                            dicDetailCategory[i].isSelected = 0
                        }
                        print("Data: ", dicDetailCategory)
                        self?.resellerCar.detail_category = dicDetailCategory
                        self?.arrayCategoryDetail = dicDetailCategory
                        self?.tblDetailCategory.reloadData()
                    }
                    
                    self?.handleCarDetails()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
        
    }
    
    private func handleCarDetails(){
        self.setupCarAttributes()
        if(self.rentCarDetails?.car_imgs?.count ?? 0 == 0){
            self.photoLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.photosCollectionView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }else{
            self.photosCollectionView.reloadData()
        }
        
    }
    
    private func setupCarAttributes(){
        //["4 seats", "Automatic", "120 hp", "260 km/h"]
        self.attributes.append("\(self.rentCarDetails?.car?.seats ?? 0) " + "seats".localized())
        if let strAuto = self.rentCarDetails?.car?.automatic{
            if strAuto == "1"{
                self.attributes.append("Automatic".localized())
            }else{
                self.attributes.append("Manual".localized())
            }
        }
        
        if let nPower = self.rentCarDetails?.car?.power{
            self.attributes.append("\(nPower) " + "hp".localized())
        }
    
        if let maxSpeed = self.rentCarDetails?.car?.max_speed{
            self.attributes.append("\(maxSpeed) " + "km/h".localized())
        }

        self.attributesCollectionView.reloadData()
        
        //TODO:- Change by JD
        
        if let price = self.rentCarDetails?.car?.price {
            self.lblPriceValue.text = price <= 0 ? "No price".localized() : String(format: "%.2f KD", price)
        } else {
//            self.lblPriceValue.text = "No price".localized()
            self.lblPriceValue.text = "".localized()
        }
    }

    
    
//MARK:- ACTION METHODS
    @IBAction func btnFinanceAction(_ sender: UIButton) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CalculatorViewController") as! CalculatorViewController
        if self.bIsReseller {
            if let price = self.resellerCar.price {
                obj.strPrice = price > 0 ? "\(price)" : ""
            }
        } else {
            if let price = self.rentCarDetails?.car?.price {
                obj.strPrice = price > 0 ? "\(price)" : ""
            }
        }
        self.navigationController?.pushViewController(obj, animated: false)
    }
    
    @IBAction func submitAction(_ sender: UIButton) {
        checkLoginStatus { (isLoggedIn) in
            if isLoggedIn {
                
                if(self.bIsReseller){
                    if self.resellerSelectedColor == nil{
                        self.showDefaultAlert(title: "Color".localized(), message: "Please select car color")
                        return
                    }
                    
                     let arrayServices = self.resellerCar.services?.filter({ (packages) -> Bool in
                        return packages.selectedService == 1
                    })
                    
                    if arrayServices?.count == 0{
                        self.showDefaultAlert(title: "Service".localized(), message: "Please select services".localized())
                        return
                    }
                    
//                    if self.resellerSelectedService == nil{
//                        self.showDefaultAlert(title: "Service".localized(), message: "Please select services".localized())
//                        return
//                    }
                }else{
                    if(self.bIsCarRent){
                        if (self.txtFromDate.text?.count ?? 0) == 0 {
                            self.showDefaultAlert(title: "From date".localized(), message: "Please choose from date".localized())
                            return
                        }
                        
                        if (self.txtToDate.text?.count ?? 0) == 0 {
                            self.showDefaultAlert(title: "To date".localized(), message: "Please choose to date".localized())
                            return
                        }
                        
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd"
                        
                        self.strFromDate = dateFormatter.string(from: self.fromDatePickerView.date)
                        self.strToDate   = dateFormatter.string(from: self.toDatePickerView.date)
                    }
                    
                    if let carId = self.rentCarDetails?.car?.id{
                        self.strCarId = "\(carId)"
                    }
                }
             
                self.performSegue(withIdentifier: "showUpload", sender: self)
            }
        }
    }
}

extension CarDetailsViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.txtFromDate{
            if textField.text == ""{
               self.setUpFromDateText()
            }
            self.txtToDate.text = ""
        }
        
        if textField == self.txtToDate{
            self.toDatePickerView.minimumDate = Calendar.current.date(byAdding: .day, value: 1, to: self.fromDatePickerView.date)//

            if textField.text == ""{
                self.setUpToDateText()
            }
        }
    }
    
    func setUpFromDateText(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.txtFromDate.text = dateFormatter.string(from: self.fromDatePickerView.date)
    }
    
    
    func setUpToDateText(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        self.txtToDate.text = dateFormatter.string(from: self.toDatePickerView.date)
    }
    
}

extension CarDetailsViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case photosCollectionView:
            if self.bIsCarRent || self.bIsUsedCar { return self.rentCarDetails?.car_imgs?.count ?? 0}
            return self.resellerCarImages.count
        case attributesCollectionView:
            return attributes.count
        case colorCollectionView:
            if self.bIsReseller { return self.resellerCar.colors?.count ?? 00 }
            return carCollors.count
        default:
            return 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        switch collectionView {
            case photosCollectionView:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "classCell", for: indexPath) as! ClassCollectionViewCell
                cell.classImageView.layer.cornerRadius = 10.0
                cell.classImageView.clipsToBounds = true
                cell.classImageView.alpha = 1
                cell.classImageView.image = nil
                
                if(self.bIsUsedCar || self.bIsCarRent){
                    if let carImgs = self.rentCarDetails?.car_imgs{
                        //                        if let imgURL = URL.init(string: carImgs[indexPath.row].car_img ?? ""){
                        //                            cell.classImageView.af_setImage(withURL: imgURL)
                        //                        }
                        
                        cell.classImageView.setImageForCar(withUrl: carImgs[indexPath.row].car_img)
                    }
                }else{
                    //                    if let imgURL = URL.init(string: self.resellerCarImages[indexPath.row].image ?? ""){
                    //                        cell.classImageView.af_setImage(withURL: imgURL)
                    //                    }
                    cell.classImageView.setImageForCar(withUrl: self.resellerCarImages[indexPath.row].image)
                }
                return cell
            case attributesCollectionView:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "attributeCell", for: indexPath) as! WashCompanyCollectionViewCell
                cell.mainView.layer.cornerRadius = 10.0
                cell.companyNameLabel.text = attributes[indexPath.row]
                cell.companyImageView.image = attributesImages[indexPath.row]
                return cell
            case colorCollectionView:
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorCell", for: indexPath) as! ColorCollectionViewCell
                cell.colorView.layer.cornerRadius = 20
                
                if self.nColorSelectedIndex == indexPath.row{
                    cell.colorView.layer.borderWidth = 1.5
                    cell.colorView.layer.borderColor = UIColor.black.cgColor
                }else{
                    cell.colorView.layer.borderWidth = 0.0
                    cell.colorView.layer.borderColor = UIColor.clear.cgColor
                }
                
                
                cell.colorView.backgroundColor = self.bIsReseller ? UIColor.init(hex: self.resellerCar.colors![indexPath.row].code ?? "") : carCollors[indexPath.row]
                return cell
            default:
                return UICollectionViewCell()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView == photosCollectionView{
            return UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 8)
        }
            
        else if collectionView == colorCollectionView {
            
            if (carCollors.count < 8){
                
                let totalCellWidth = 50 * carCollors.count
                let totalSpacingWidth = 0 * (carCollors.count - 1)
                
                let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
                let rightInset = leftInset
                
                return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
            }
            else {
                return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
            }
        }
        
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (collectionView == photosCollectionView){
            
            return CGSize(width: 300, height: 150)
        }
            
        else if collectionView == colorCollectionView {
            return CGSize(width: 50, height: 40)
        }
            
        else {
            
            return CGSize(width: 120, height: 130)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == photosCollectionView{
            return 8
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == photosCollectionView{
            return 8
        }
        return 0
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if (collectionView == photosCollectionView){
            
            index = indexPath.row
            // Create an array of images.
            var images = [LightboxImage]()
            for imgObj in self.resellerCarImages{
                if let imgURL = URL.init(string: imgObj.image ?? ""){
                    images.append(LightboxImage(imageURL: imgURL))
                }
            }
            
            // Create an instance of LightboxController.
            let controller = LightboxController(images: images)
            controller.dynamicBackground = true
            controller.goTo(index, animated: true)
           
            let navigationController = UINavigationController(rootViewController: controller)
            navigationController.isNavigationBarHidden = true
            self.present(navigationController, animated: true, completion: nil)
            
        }
        else if collectionView == colorCollectionView {
            let cell = collectionView.cellForItem(at: indexPath) as! ColorCollectionViewCell
            cell.colorView.layer.borderWidth = 1.5
            cell.colorView.layer.borderColor = UIColor.black.cgColor
            self.nColorSelectedIndex = indexPath.row
            self.resellerSelectedColor = self.resellerCar.colors?[indexPath.row]
            self.classImageView.image = nil
            
            if let imgURL = URL.init(string: self.resellerSelectedColor?.image ?? ""){
                self.classImageView.af_setImage(withURL: imgURL, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main , imageTransition: UIImageView.ImageTransition.noTransition , runImageTransitionIfCached: true) { [weak self](_) in
                   if (self!.animate){
                       self?.animate = false
                       self?.classImageView.transform = .init(translationX: self!.view.frame.width, y: 0)
                       self?.animateHome()
                   }
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if collectionView == colorCollectionView {
            let cell = collectionView.cellForItem(at: indexPath) as! ColorCollectionViewCell
            cell.colorView.layer.borderWidth = 0
            cell.colorView.layer.borderColor = UIColor.clear.cgColor
            self.resellerSelectedColor = nil
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showImages"){
            
            let vc = segue.destination as! ViewPhotosViewController
            if let cell = self.photosCollectionView.cellForItem(at: IndexPath.init(row: index, section: 0)) as? ClassCollectionViewCell{
                
                var imgs = [String]()
                for carImage in self.resellerCarImages{
                    imgs.append(carImage.image ?? "")
                }
                                
               // if let image = cell.classImageView.image{
                    vc.images = imgs
                    vc.index = 0
                    vc.firstTime = true
                    vc.bIsUrls = true
                //}
            }
        }
        
        if (segue.identifier == "showUpload"){
            let vc = segue.destination as! UploadOptionsViewController

            if(self.bIsCarRent || self.bIsUsedCar){
                vc.bIsCarRent = self.bIsCarRent
                vc.bIsUsedCar = self.bIsUsedCar
                var dataDict = ["from_date": "\(self.strFromDate)","to_date": "\(self.strToDate)", "car_id": "\(self.strCarId)","car_name": self.rentCarDetails?.car?.name ?? "", "car_image": self.rentCarDetails?.car?.car_img ?? "", "agency_name" : self.agencyName]
                vc.carRentPrice = self.rentCarDetails?.car?.price ?? 0.0
                if(self.bIsUsedCar){
                    dataDict.removeValue(forKey: "from_date")
                    dataDict.removeValue(forKey: "to_date")
                }
                vc.dataDict  = dataDict
            }else if(self.bIsReseller){
                let selectedColor   = self.resellerSelectedColor?.id ?? 0
                let selectedService = self.resellerSelectedService
                
                let arrayServices = self.resellerCar.services?.filter({ (packages) -> Bool in
                    return packages.selectedService == 1
                })
                
                var arraySelectedServiceId = [Int]()
                
                for i in 0..<arrayServices!.count{
                    arraySelectedServiceId.append(arrayServices?[i].id ?? 0)
                }
                
                print("arraySelectedServiceID : \(arraySelectedServiceId)")

                let dataDict = ["color_id": "\(selectedColor)","agency_id": "\(self.resellerAgencyId)", "car_id": "\(self.resellerCar.id ?? 0)","car_image": "\(self.resellerCar.image ?? "")","car_name": self.resellerCar.name ?? "", "service_id": "\(self.resellerCar.id ?? 0)", "service_name": "\(selectedService?.name ?? "")","service_price": String.init(format: "%.2f KD", (selectedService?.price ?? 0.0)), "agency_name" : self.agencyName]
                
                vc.arraySelectedServices = arrayServices ?? []
                vc.selectedId = arraySelectedServiceId
                vc.dataDict  = dataDict
                vc.bIsReseller = true
            }
           
        }
        
    }
}

extension CarDetailsViewController: UITableViewDataSource ,  UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == serviceTableView {
            return 120
        }
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.tblDetailCategory {
            
            return self.arrayCategoryDetail.count
        }
        return  (self.bIsCarRent || self.bIsUsedCar) ? 0 : self.resellerCar.services?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == self.tblDetailCategory {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailCategoryTableCell", for: indexPath) as! DetailCategoryTableCell
            
            let dict = self.arrayCategoryDetail[indexPath.row]
            cell.lblHeader.text = dict.category_name ?? ""
            cell.lblDescription.text = dict.detail_description ?? ""
            
            if dict.isSelected == 0 {
                cell.btnSelection.isSelected = true
                cell.vwDescription.isHidden = true
            }
            else {
                cell.btnSelection.setImage(UIImage(named: "ic_minus"), for: .normal)
                cell.btnSelection.isSelected = false
                cell.vwDescription.isHidden = false
            }
            cell.btnSelection.tag = indexPath.row
            cell.btnSelection.addTarget(self, action: #selector(btnAddAction(_:)), for: .touchUpInside)
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! ServiceTableViewCell
        cell.mainView.layer.cornerRadius = 10.0
        cell.serviceTitleLabel.textColor = Constants.appColor
        cell.serviceDescribtionLabel.textColor = UIColor.darkGray
        if let servicePrice = self.resellerCar.services?[indexPath.row].price{
            if servicePrice > 0{
                cell.lblPrice.text = String(format: "%.2f KD", servicePrice)
            }else{
//                cell.lblPrice.text = "FREE".localized()
                //TODO:- Change Jaydeep
//                cell.lblPrice.text = "No price".localized()
                cell.lblPrice.text = ""
            }
        }else{
//            cell.lblPrice.text = "FREE".localized()
            //TODO:- Change Jaydeep
//            cell.lblPrice.text = "No price".localized()
            cell.lblPrice.text = ""
        }
       
//        if let imgURL = URL.init(string: self.resellerCar.services?[indexPath.row].image ?? ""){
//            cell.serviceImageView.af_setImage(withURL: imgURL)
//        }
        cell.serviceImageView.setImageOfApp(withUrl: self.resellerCar.services?[indexPath.row].image)
        
        //cell.serviceImageView.image = servicesImages[indexPath.row]
        cell.serviceTitleLabel.text = self.resellerCar.services?[indexPath.row].name ?? ""
        
        if self.resellerCar.services?[indexPath.row].selectedService ?? 0 == 0{
            cell.mainView.layer.borderColor = UIColor.clear.cgColor
           cell.mainView.layer.borderWidth = 0.0
           cell.radioImageView.image = UIImage(named: "radioButton")
        }else{
            cell.mainView.layer.borderColor = Constants.appColor.cgColor
            cell.mainView.layer.borderWidth = 2.0
            cell.radioImageView.image = UIImage(named: "radioButtonChecked")
        }
        
        cell.btnReadMore.tag = indexPath.row
        cell.btnReadMore.addTarget(self, action: #selector(btnReadMoreAction(_:)), for: .touchUpInside)
//        cell.btnRadioSelection.tag = indexPath.row
//        cell.btnRadioSelection.addTarget(self, action: #selector(btnRadioSelectionAction(_:)), for: .touchUpInside)
        
        cell.serviceDescribtionLabel.text = self.resellerCar.services?[indexPath.row].description ?? ""
        if let imgURL = URL.init(string: self.resellerCar.services?[indexPath.row].image ?? ""){
            cell.serviceImageView.af_setImage(withURL: imgURL)
        }
        //cell.serviceImageView.image = servicesImages[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == serviceTableView {
            let indexPath = IndexPath(row: indexPath.row, section: 0)
            let cell = serviceTableView.cellForRow(at: indexPath) as! ServiceTableViewCell
            
            if self.resellerCar.services?[indexPath.row].selectedService == 1{
                self.resellerCar.services?[indexPath.row].selectedService = 0
            }else{
                self.resellerCar.services?[indexPath.row].selectedService = 1
            }
            
            serviceTableView.reloadData()
            self.resellerSelectedService = self.resellerCar.services?[indexPath.row]
            if Constants.bIsUserLoggedIn() == false{
                self.showAlert(message: "Please login to your account".localized(), status: "Login".localized())
                return
            }
        }
        
        /*
        //TODO: Vishal
        let data = self.resellerCar.services?[indexPath.row]
        let serviceDescriptionC = self.storyboard?.instantiateViewController(withIdentifier: "ServiceDescriptionVC") as! ServiceDescriptionVC
        serviceDescriptionC.serviceData = data!
        self.present(serviceDescriptionC, animated: true, completion: nil)
//        print("Data: ", self.resellerCar.services?[indexPath.row])
        // self.performSegue(withIdentifier: "showUpload", sender: self)
        */
    }
    
    
    @objc func btnReadMoreAction(_ sender: UIButton) {
        
        let data = self.resellerCar.services?[sender.tag]
        let serviceDescriptionC = self.storyboard?.instantiateViewController(withIdentifier: "ServiceDescriptionVC") as! ServiceDescriptionVC
        serviceDescriptionC.serviceData = data!
        self.present(serviceDescriptionC, animated: true, completion: nil)
    }
    
    @objc func btnAddAction(_ sender: UIButton) {
        
        for i in 0..<self.arrayCategoryDetail.count {
            self.arrayCategoryDetail[i].isSelected = 0
        }
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        
        let cell = self.tblDetailCategory.cellForRow(at: indexPath) as! DetailCategoryTableCell
        
        if cell.btnSelection.isSelected == true {
            self.arrayCategoryDetail[sender.tag].isSelected = 1
        }
        print("Tag: \(sender.tag)")
        print("Data: \(self.arrayCategoryDetail)")
        self.tblDetailCategory.reloadData()
        
    }
    /*
    //TODO: Vishal
    @objc func btnRadioSelectionAction(_ sender: UIButton) {
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = serviceTableView.cellForRow(at: indexPath) as! ServiceTableViewCell
        
        if self.resellerCar.services?[sender.tag].selectedService == 1{
            self.resellerCar.services?[sender.tag].selectedService = 0
        }else{
            self.resellerCar.services?[sender.tag].selectedService = 1
        }
        
        serviceTableView.reloadData()
        self.resellerSelectedService = self.resellerCar.services?[sender.tag]
        if Constants.bIsUserLoggedIn() == false{
            self.showAlert(message: "Please login to your account".localized(), status: "Login".localized())
            return
        }
    }
    */
}

//MARK:- Overide Method
extension CarDetailsViewController {
    
    func addObserver() {
        [tblDetailCategory].forEach { (tbl) in
            tbl?.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        }
    }
    
    //Vishal Setup
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
            
            self.tblDetailCategoryHeightConstraint.constant = self.tblDetailCategory.contentSize.height
        }
    }
}

