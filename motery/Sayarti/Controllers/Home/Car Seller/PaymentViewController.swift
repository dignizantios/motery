//
//  PaymentViewController.swift
//  Sayarti
//
//  Created by Vasim Ajmeri on 22/10/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import WebKit

class PaymentViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet var webView: WKWebView!
    var strURL = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = false
        self.title = "Payment"
        self.showLoader()

        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        
        if let url = URL(string: strURL){
            webView.load(URLRequest(url: url))
            webView.navigationDelegate = self
            webView.allowsBackForwardNavigationGestures = false
            webView = WKWebView(frame: view.bounds, configuration: configuration)
        }
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.showLoader()
    }
    
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideLoader()
        webView.evaluateJavaScript("document.getElementById('is_captured').value") { (response, error) in
            
            print("response : \(response)")
            
            if let error = error{
                print(error.localizedDescription)
                return
            }

            if let response = response{
                if let strCaptured = response as? String{
                    if strCaptured == "1"{
                        print("payment final success ")
                        self.paymentSuccess()
                    }else{
                        print("payment final failed ")
                        self.paymentFailed()
                    }
                }
            }
        }
    }
    
    private func paymentSuccess(){
        let successVC = self.storyboard?.instantiateViewController(withIdentifier: "SuccessViewController") as! SuccessViewController
        self.navigationController?.pushViewController(successVC, animated: true)
    }
    
    private func paymentFailed(){
        self.navigationController?.popViewController(animated: true)
    }
}
