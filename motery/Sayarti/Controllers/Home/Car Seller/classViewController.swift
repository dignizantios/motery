//
//  classViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/4/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class CarDetailsViewController: UIViewController {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var describtionLabel: UILabel!
    @IBOutlet weak var classImageView: UIImageView!
    @IBOutlet weak var attributesCollectionView: UICollectionView!
    @IBOutlet weak var photoLabel: UILabel!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var serviceTableView: customTableView!
    @IBOutlet weak var serviceDescribtionLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    
    var startingFrame = CGRect()
    let zoomImageView:UIImageView = {
        
        let mv = UIImageView()
        mv.contentMode = .scaleAspectFill
        mv.clipsToBounds = true
        return mv
    }()
    
    let blackBackgroundView:UIView =
    {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.isUserInteractionEnabled = true
        v.backgroundColor = UIColor.black
        v.alpha = 0
        
        
        return v
    }()
    
    let navBarCoverView = UIView()
    private let imageView = UIImageView(image: UIImage(named: "class1"))
    let carImages = [#imageLiteral(resourceName: "image-2"),#imageLiteral(resourceName: "image-3"),#imageLiteral(resourceName: "image-1")]
    let attributes = ["4 seats", "Automatic", "120 hp", "260 km/h"]
    let attributesImages = [#imageLiteral(resourceName: "Seats"),#imageLiteral(resourceName: "Gears"),#imageLiteral(resourceName: "HP"),#imageLiteral(resourceName: "Speed")]
    let services = ["Request test drive", "Request car repair", "book this car", "Request car book"]
    let servicesImages = [#imageLiteral(resourceName: "service1"),#imageLiteral(resourceName: "Tire"),#imageLiteral(resourceName: "class1"),#imageLiteral(resourceName: "Book")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "A-Class 2019"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        submitButton.layer.cornerRadius = 8.0
        
        let text = "he new A-communication is as youthful and dynamic as ever, but grown-up and comfortable like never before. It completely redefines modern luxury in the compact class, and revolutionises interior design. Technologically the new A-Class not only takes first place thanks to MBUX – Mercedes-Benz User Experience: it also offers a number of functions that were previously the preserve of the luxury class. And although Mercedes-Benz has retained the sporty appearance, the utility value has increased. The new A-Class can be ordered from March onwards, and the market launch commences in the spring."
        let text2 = text.components(separatedBy: " ")
        var desc: String = ""
        var index = 0
        
        for _ in 0...5 {
            for _ in 0...2 {
                desc = desc + text2[index] + " "
                index = index + 1
            }
            desc = desc + "\n"
        }
        
        for _ in index...text2.count-1 {
            desc = desc + text2[index] + " "
            index = index + 1
        }
        describtionLabel.text = desc
        
        setupViews()
        // Do any additional setup after loading the view.
    }
    
    func setupViews()
    {
        view.addSubview(blackBackgroundView)
        view.addSubview(zoomImageView)
        NSLayoutConstraint.activate([
            blackBackgroundView.topAnchor.constraint(equalTo: view.topAnchor),
            blackBackgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            blackBackgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            blackBackgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            ])
        blackBackgroundView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(closeImage(gesture:))))
    }
    
    
    @IBAction func submitAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "showUpload", sender: self)
    }
    
    @IBAction func closeImage (gesture: UIPanGestureRecognizer){
        
        let pan = gesture.translation(in: view)
        
//        if (pan.y < -20 || pan.y > 20)
//        {
            if (gesture.state == .changed){
//                if (pan.y > 0){
//                    zoomImageView.transform = CGAffineTransform.init(translationX: pan.x, y: pan.y - 20)
//                }
//                else {
                    zoomImageView.transform = CGAffineTransform.init(translationX: pan.x, y: pan.y )
//                }
                
                UIView.animate(withDuration: 0.3) {
                    self.navBarCoverView.alpha = 0
                    self.blackBackgroundView.alpha = 0
                }
            }
            
            if (gesture.state == .ended){
                UIView.animate(withDuration: 0.3, animations: {
                    self.zoomImageView.frame = self.startingFrame
                    self.blackBackgroundView.alpha = 0
                    self.navBarCoverView.alpha = 0
                }, completion: {(didComplete) -> Void in
                    self.zoomImageView.image = nil
                    self.zoomImageView.frame = .zero
                    self.blackBackgroundView.transform = .identity
                    self.navBarCoverView.removeFromSuperview()
                    self.photosCollectionView.reloadData()
                })
            }
//        }
    }
}

extension CarDetailsViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch collectionView {
        case photosCollectionView:
            return 3
        case attributesCollectionView:
            return attributes.count
        default:
            return 0
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        switch collectionView {
        case photosCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "classCell", for: indexPath) as! classCollectionViewCell
            cell.classImageView.layer.cornerRadius = 10.0
            cell.classImageView.clipsToBounds = true
            cell.classImageView.alpha = 1
            cell.classImageView.image = carImages[indexPath.row]
            return cell
        case attributesCollectionView:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "attributeCell", for: indexPath) as! washCompanyCollectionViewCell
            cell.mainView.layer.cornerRadius = 10.0
            cell.companyNameLabel.text = attributes[indexPath.row]
            cell.companyImageView.image = attributesImages[indexPath.row]
            return cell
        default:
            return UICollectionViewCell()
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if collectionView == photosCollectionView{
            return UIEdgeInsets.init(top: 0, left: 8, bottom: 0, right: 8)
        }
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if (collectionView == photosCollectionView){
            
            return CGSize(width: 250, height: 120)
        }
        else {
            
            return CGSize(width: 120, height: 130)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == photosCollectionView{
            return 8
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == photosCollectionView{
            return 8
        }
        return 0
    }
    
    
    
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            if (collectionView == photosCollectionView){
                
                let cell = collectionView.cellForItem(at: indexPath) as! classCollectionViewCell
                
                startingFrame = (cell.classImageView.superview?.convert(cell.classImageView.frame,to: nil))!
                zoomImageView.frame = startingFrame
                zoomImageView.image = cell.classImageView.image
                
                navBarCoverView.backgroundColor = UIColor.black
                navBarCoverView.frame = CGRect(x: 0, y: 0, width: (self.navigationController?.navigationBar.frame.width)!, height: (self.navigationController?.navigationBar.frame.height)! + 20)
                navBarCoverView.alpha = 0
                
                if let keyWindow = UIApplication.shared.keyWindow {
                    keyWindow.addSubview(navBarCoverView)
                }
                
                let height = (self.view.frame.width / self.startingFrame.width) * self.startingFrame.height
                let y = ((view.frame.height/2) - (height/2))
                cell.classImageView.alpha = 0
                UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 0.5 , options: .curveEaseOut, animations: {() -> Void in
                    self.zoomImageView.frame = CGRect(x: 0, y: y, width: self.view.frame.width, height: height)
                    self.blackBackgroundView.alpha = 1
                    self.navBarCoverView.alpha = 1
                }, completion: { (_) in
                    
                })
            }
            else {
                
            }
        }
}

extension CarDetailsViewController: UITableViewDataSource ,  UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! serviceTableViewCell
        cell.mainView.layer.cornerRadius = 10.0
        cell.serviceTitleLabel.textColor = Constants.appColor
        cell.serviceDescribtionLabel.textColor = UIColor.darkGray
        cell.serviceImageView.image = servicesImages[indexPath.row]
        cell.serviceTitleLabel.text = services[indexPath.row]
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! serviceTableViewCell
        cell.mainView.layer.borderColor = Constants.appColor.cgColor
        cell.mainView.layer.borderWidth = 2.0
        cell.radioImageView.image = UIImage(named: "radioButtonChecked")
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! serviceTableViewCell
        cell.mainView.layer.borderColor = UIColor.clear.cgColor
        cell.mainView.layer.borderWidth = 0.0
        cell.radioImageView.image = UIImage(named: "radioButton")
    }
}
