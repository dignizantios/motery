//
//  companiesViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/2/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class CompaniesViewController: UIViewController , UISearchBarDelegate , UIScrollViewDelegate,UIPopoverPresentationControllerDelegate {
    
    @IBOutlet weak var companiesCollectionView: UICollectionView!
    
    @IBOutlet var btnSearch: UIBarButtonItem!
    @IBOutlet var btnFilter: UIBarButtonItem!
    @IBOutlet var btnSort: UIBarButtonItem!
    
    var companies = [RentCompany]()
    var arrayMainCompanies = [RentCompany]()
    var arrReSellerCompanies = [ResellerCompany]()
    var bIsCarRent = false
    var bIsUsedCar = false
    var bIsReSeller = false
    var strTitle    = ""
    
    var bIsSearch = false
    var arrSearchReseller = [ResellerCompany]()
    var arrSearchCompanies = [RentCompany]()

 
    var nBrandIndex = 0
    var buyCompanies = [DummyData.shared.AustonMartin(),DummyData.shared.BMW(),DummyData.shared.Chevrolete(), DummyData.shared.Ferrari(), DummyData.shared.GMC()]
    
    var selectedFilterID = -1 // For All selection in filter
    
    
//MARK:- VIEW DELEGATES
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //TODO: - Yash changes
//        if bIsCarRent{
//            self.navigationItem.rightBarButtonItems = nil
//            self.navigationItem.rightBarButtonItems = [btnSearch,btnSort]
//        }
        
        self.fetchCompaniesList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = strTitle
        if #available(iOS 13.0, *) {
            let app = UINavigationBarAppearance()
            app.backgroundColor = .white
            self.navigationController?.navigationBar.scrollEdgeAppearance = app
        }
        
    }
    
    //MARK:- FETCH COMPANIES
    
    private func fetchCompaniesList(){
        if !self.bIsCarRent && !self.bIsUsedCar && !self.bIsReSeller { return }
        
        self.showLoader()
        var strURL = ""
        
        if self.bIsCarRent { strURL =  Constants.GET_ALL_RENT_COMPANIES }
        else if self.bIsUsedCar { strURL =  Constants.GET_USED_CAR_COMPANIES }
        else if self.bIsReSeller { strURL =  Constants.GET_RESELLER_COMPANIES }
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseDict) in
            
            if let jsonData = responseDict{
                do {
                    print("jsonData:\(jsonData)")
                    
                    if self!.bIsReSeller{
                        self?.arrReSellerCompanies = try JSONDecoder().decode([ResellerCompany].self, from: jsonData)
                        self?.arrSearchReseller = Array.init(self!.arrReSellerCompanies)
                        
                    }else{
                        self?.companies = try JSONDecoder().decode([RentCompany].self, from: jsonData)
                        self?.arrayMainCompanies = Array.init(self!.companies)
                        self?.arrSearchCompanies = Array.init(self!.companies)
                    }
                    self?.companiesCollectionView.reloadData()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }
    
    
    @IBAction func beginSearch(_ sender: UIBarButtonItem) {
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        searchController.hidesNavigationBarDuringPresentation = false
        UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Cancel".localized()
        // searchController.searchBar.setValue("Cancel", forKey: "_cancelButtonText")
        searchController.searchBar.tintColor = UIColor.black
        searchController.searchBar.barTintColor = Constants.grayColor
        searchController.searchBar.placeholder = "Search".localized()
        self.present(searchController,animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        dismiss(animated: true, completion: nil)
        
        self.bIsSearch = true
        if self.bIsReSeller{
            let filterArray = self.arrSearchReseller.filter({ ($0.name ?? "").contains(searchBar.text ?? "") })
            if filterArray.count > 0{
                self.arrReSellerCompanies = filterArray
                self.companiesCollectionView.reloadData()
            }
        }else{
            let filterArray = self.arrSearchCompanies.filter({ ($0.company_name ?? "").contains(searchBar.text ?? "") })
            if filterArray.count > 0{
                self.companies = filterArray
                self.companiesCollectionView.reloadData()
            }
        }
       
        
    }
    
    func searchBar(_ searchBar: UISearchBar, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentString = (searchBar.text!.lowercased() as NSString).replacingCharacters(in: range, with: text)
        
        if currentString.count > 0 {
            self.bIsSearch = true
            
            if self.bIsReSeller{
                let filterArray = self.arrSearchReseller.filter({ (($0.name ?? "").lowercased()).contains(currentString)})
                
                if filterArray.count > 0{
                    self.arrReSellerCompanies = filterArray
                    self.companiesCollectionView.reloadData()
                }else{
                    self.arrReSellerCompanies.removeAll()
                    self.companiesCollectionView.reloadData()
                }
            }else{
                let filterArray = self.arrSearchCompanies.filter({ (($0.company_name ?? "").lowercased()).contains(currentString.lowercased())})
                
                if filterArray.count > 0{
                    self.companies = filterArray
                    self.companiesCollectionView.reloadData()
                }else{
                    self.companies.removeAll()
                    self.companiesCollectionView.reloadData()
                }
            }
        }
        else {
            if self.bIsReSeller{
                self.arrReSellerCompanies = self.arrSearchReseller
                self.companiesCollectionView.reloadData()
            }else{
                self.companies = self.arrSearchCompanies
                self.companiesCollectionView.reloadData()
            }
        }
       
        return true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.bIsSearch = false
        if self.bIsReSeller{
            self.arrReSellerCompanies = self.arrSearchReseller
            self.companiesCollectionView.reloadData()
        }else{
            self.companies = self.arrSearchCompanies
            self.companiesCollectionView.reloadData()
        }
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.bIsSearch = false
        if self.bIsReSeller{
            self.arrReSellerCompanies = self.arrSearchReseller
            self.companiesCollectionView.reloadData()
        }else{
            self.companies = self.arrSearchCompanies
            self.companiesCollectionView.reloadData()
        }
    }
}

extension CompaniesViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout, popupDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.bIsReSeller{
            return self.arrReSellerCompanies.count
        }
        
        return companies.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "companyCell", for: indexPath) as! CompaniesCollectionViewCell
        cell.mainView.layer.cornerRadius = 10.0
        cell.agentImageView.image = nil
        
        if self.bIsCarRent || self.bIsUsedCar{
            let companyDetails = self.companies[indexPath.row]
            cell.brandNameLabel.text = companies[indexPath.row].company_name
            cell.brandImageView.image = nil
            cell.agentImageView.heightAnchor.constraint(equalToConstant: 0.0).isActive = true
            
//            if let imgPath = companyDetails.company_img {
//                if let imgUrl = URL.init(string: imgPath){
//                    cell.brandImageView.af_setImage(withURL: imgUrl)
//                }
//            }
            
            cell.brandImageView.setImageOfApp(withUrl: companyDetails.company_img)
            
        }else{
            let companyDetails = self.arrReSellerCompanies[indexPath.row]
            cell.brandNameLabel.text = companyDetails.name
            cell.brandImageView.image = nil
            
//            if let imgPath = companyDetails.image {
//                if let imgUrl = URL.init(string: imgPath){
//                    cell.brandImageView.af_setImage(withURL: imgUrl)
//                }
//            }
//
//            if let imgPath = companyDetails.agency?.image{
//                if let imgUrl = URL.init(string: imgPath){
//                    cell.agentImageView.af_setImage(withURL: imgUrl)
//                }
//            }
            
            cell.brandImageView.setImageOfApp(withUrl: companyDetails.image)
            cell.agentImageView.setImageOfApp(withUrl: companyDetails.agency?.image)
            
        }
        
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewWidth = collectionView.bounds.width / 2
        if self.bIsReSeller{
            return CGSize(width: collectionViewWidth, height: 250)

        }else{
            return CGSize(width: collectionViewWidth, height: 200)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.bIsCarRent || self.bIsUsedCar{
            self.handlePopup(indexPath: indexPath)
            self.nBrandIndex = indexPath.row
        }else{
            self.performSegue(withIdentifier: "showCompanyDetails", sender: indexPath)
        }
        
    }
    
    //MARK:- HANDLE/SHOW POPUP
    private func handlePopup(indexPath: IndexPath){
        var arrAgencies = [String]()
        var arrImages   = [String]()

        if let arrDbAgencies = self.companies[indexPath.row].agencies{
            for agency in arrDbAgencies{
                arrAgencies.append(agency.agency_name ?? "")
                arrImages.append(agency.agency_img ?? "")
            }
        }
        
        if arrAgencies.count == 0{
            self.showDefaultAlert(title: "No Agency", message: "There are no agency available for selected brand")
            return
        }
       
        DispatchQueue.main.async {
            let vc = PopUpViewController()
            vc.customDelegate = self
            vc.strMainTitle = "Select agency : "
            vc.arrTitles    = arrAgencies
            vc.arrImages    = arrImages
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle   = .coverVertical
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func onBtnDismiss(selectedIndex: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.performSegue(withIdentifier: "showCompanyDetails", sender: IndexPath.init(row: selectedIndex, section: 0))
        }
    }
 
    private func showPopup(_ controller: UIViewController, sourceView: UIView) {
        let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
        presentationController.sourceView = sourceView
        presentationController.sourceRect = sourceView.bounds
        presentationController.permittedArrowDirections = [.down,.up]
        self.present(controller, animated: true)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCompanyDetails"{
            guard let indexPath = sender as? IndexPath else {return}
            let vc = segue.destination as! SellerCompanyViewController
            vc.bIsCarRent       = self.bIsCarRent
            vc.bIsUsedCar       = self.bIsUsedCar

            if self.bIsCarRent || self.bIsUsedCar{
                guard let agencyObj = self.companies[self.nBrandIndex].agencies?[indexPath.row] else {return}
                vc.carRentCompany   = self.companies[self.nBrandIndex]
                vc.carRentAgency    = agencyObj
                
                print("agencyObj : \(agencyObj.agency_name)")
                
            }else{
                vc.resellerCompany = self.arrReSellerCompanies[indexPath.row]
                vc.bIsReseller     = true
            }
        }
        
        if segue.identifier == "showFilter"{
            let vc = segue.destination as! FilterViewController
            vc.lastSelectedContinentID = self.selectedFilterID
            vc.delegate = self
        }
        
        if segue.identifier == "showSort"{
            let vc = segue.destination as! SortViewController
            vc.delegate = self
        }
    }
}



// MARK:- SortViewDelegate
extension CompaniesViewController: SortViewDelegate {
    
    func didSelect(_ sortType: SortType) {
        switch sortType {
        case .ascending:
            //companies.sort(by:{$0.company_name ?? "" < $1.company_name ?? ""})
            if self.bIsReSeller{
                self.arrReSellerCompanies = self.arrReSellerCompanies.sorted(by: {$0.name ?? "" < $1.name ?? ""})
            }else{
                self.companies = self.companies.sorted(by: {$0.company_name ?? "" < $1.company_name ?? ""})
            }
            companiesCollectionView.reloadData()
        case .descending:
            if self.bIsReSeller{
                self.arrReSellerCompanies = self.arrReSellerCompanies.sorted(by: {$0.name ?? "" > $1.name ?? ""})
            }else{
                self.companies = self.companies.sorted(by: {$0.company_name ?? "" > $1.company_name ?? ""})
            }
            companiesCollectionView.reloadData()
        }
    }
    
}

// MARK:- FilterViewDelegate
extension CompaniesViewController: FilterViewDelegate{
    func didSelect(continent: Continent, lastSelectedContinentID: Int) {
        
        print("lastSelectedConitnueID : \(lastSelectedContinentID)")
        selectedFilterID = lastSelectedContinentID
        if lastSelectedContinentID == -1{
            self.fetchCompaniesList()
        }else{
            if self.bIsReSeller{
                getCompaniesBy(continent.id)
            }else{
                manuallyFilterInCarRentCarUse(continetnID: continent.id)
            }
        }
    }
    
    //TODO: - Yash changes
    
    func manuallyFilterInCarRentCarUse(continetnID:Int){
        
        self.companies = arrayMainCompanies.filter { (a) -> Bool in
            return a.continent_id == continetnID
        }
        self.companiesCollectionView.reloadData()
    }
    
    /// Function that get companies based on continent id
    func getCompaniesBy(_ continentId: Int){
        self.showLoader()
        
        if self.bIsReSeller{
            Network.shared.makeHttpRequest(model: [ResellerCompany](), method: .get, APIName: "user/getCompaniesByContinentId/\(continentId)", parameters: nil) { (result) in
                
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(let companies):
                        self.arrReSellerCompanies = companies
                        self.companiesCollectionView.reloadData()
                    case .failure(let error):
                        self.showAlert(message: error.errorDescription ?? "", status: "")
                    }
                }
            }
        }else{
            Network.shared.makeHttpRequest(model: [RentCompany](), method: .get, APIName: "user/getCompaniesByContinentId/\(continentId)", parameters: nil) { (result) in
                
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(let companies):
                        self.companies = companies
                        self.companiesCollectionView.reloadData()
                    case .failure(let error):
                        self.showAlert(message: error.errorDescription ?? "", status: "")
                    }
                }
            }
        }
    }
    
    //TODO: - Yash changes
    
    func filterInCarRentAndCarUsed(){
    
        
    }
    
}
