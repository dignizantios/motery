//
//  SuccessViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/31/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class SuccessViewController: UIViewController {
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var lblOrderSubmitted: UILabel!
    @IBOutlet weak var lblOrderSuccess: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblOrderSubmitted.text = "Order submitted".localized()
        self.lblOrderSuccess.text = "Order submitted successfully. \nThanks for using Motery.".localized()
        continueButton.setTitle("Continue".localized(), for: .normal)
        continueButton.layer.cornerRadius = 9.0
        // Do any additional setup after loading the view.
        self.navigationItem.hidesBackButton = true
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    @IBAction func continueButton(_ sender: UIButton) {
        
        if let arrVC = self.navigationController?.viewControllers{
            for viewController in arrVC{
//                if let washCompaniesVC = viewController as? WashCompaniesViewController{
//                    self.navigationController?.popToViewController(washCompaniesVC, animated: true)
//                    break
//                }
                
                if let companyVC = viewController as? HomeViewController{
                    self.navigationController?.popToViewController(companyVC, animated: true)
                    break
                }
            }
        }
    }
}
