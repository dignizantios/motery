//
//  uploadOptionsViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/5/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

enum checkWhichServiceVC {
    case protectionCarWash
    case otherService
}

class UploadOptionsViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate , UITextViewDelegate {
    
    var index: Int!    = 0
    var optionImages   = [UIImage?]()
    var imagePicker    = UIImagePickerController()
    var arrAttachments = [Attachment]()
    var dataDict       = [String: Any]()
    var bIsCarRent     = false
    var bIsUsedCar     = false
    var bIsReseller    = false
    var bIsInsurance   = false
    
    //Seller
    var selectedId = [Int]()
    var arraySelectedServices = [ResellerService]()
    
    var arraySelectedServicesForCarWash = [Service]()
    var selectedPackgeForCarWash = Package()
    
    var carRentPrice = Double()
    
    var checkService = checkWhichServiceVC.otherService
    
    //["Civil id", "Car photo", "Car photo", "Car photo", "Car photo", "Car photo", "Car photo", "Car photo"]
    
    @IBOutlet weak var describtionLabel: UILabel!
    @IBOutlet weak var descriptionLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var optionsTableView: CustomTableView!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var extraNotesTextView: UITextView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    
    
    
//MARK:- VIEW DELEGATES

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        
        if !isProtectionWashCar {
            self.fetchUploadTypes()
            self.describtionLabel.isHidden = false
            self.descriptionLabelHeightConstraint.constant = 20
        }
        else {
            self.descriptionLabelHeightConstraint.constant = 0
            self.describtionLabel.isHidden = true
        }
        
        //TODO: Vishal
//        self.fetchUploadTypes()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Attachments".localized()
    }

    //MARK:- SETUP VIEW
    private func setupView(){
        extraNotesTextView.text = "Extra notes".localized()
        continueButton.setTitle("Continue".localized(), for: .normal)
        describtionLabel.text = "Please upload the following attachments".localized()
        
        extraNotesTextView.textColor = .gray
        extraNotesTextView.layer.cornerRadius = 9.0
        optionsTableView.tableFooterView = UIView()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        optionsTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: optionsTableView.frame.size.width, height: 1))
        continueButton.layer.cornerRadius = 10.0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.mainView.addGestureRecognizer(tapGesture)
        
        let ViewForDoneButtonOnKeyboard = UIToolbar()
        ViewForDoneButtonOnKeyboard.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDoneOnKeyboard = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(self.doneBtnFromKeyboardClicked))
        ViewForDoneButtonOnKeyboard.backgroundColor = .lightGray
        btnDoneOnKeyboard.tintColor = .black
        ViewForDoneButtonOnKeyboard.setItems([flexibleSpace,btnDoneOnKeyboard], animated: true)
        extraNotesTextView.inputAccessoryView = ViewForDoneButtonOnKeyboard
    }
    
//MARK:- FETCH UPLOAD DOCS TYPES
    
    private func fetchUploadTypes(){
        self.showLoader()
        var strURL = ""
        
        var bIsPostRequest = false
        var param : [String: [Any]] = [:]
        if self.bIsReseller{
            strURL = Constants.GET_RESELLER_ATTACHMENT
            bIsPostRequest = true
            param["service_id"] = self.selectedId
        }else if (self.bIsInsurance){
            strURL = Constants.GET_INSURANCE_ATTACHMENT + (self.dataDict["insurance_type"] as? String ?? "")
        }else{
            if self.bIsCarRent{
                strURL = Constants.GET_RENT_ATTACHMENTS
            }else if self.bIsUsedCar{
                strURL = Constants.GET_USED_ATTACHMENTS
            }else{
                strURL = Constants.GET_WASH_ATTACHMENTS
            }
        }
       
        self.optionImages.removeAll()
        
//        fetchRequestData
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: bIsPostRequest, bIsShowAlert: true, parameter:param) { [weak self] (responseData) in

            if let responseData = responseData{
                do{
                    self?.arrAttachments = try JSONDecoder().decode([Attachment].self, from: responseData)
                    for _ in self!.arrAttachments{
                        self?.optionImages.append(nil)
                    }
                    self?.optionsTableView.reloadData()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }

    
//MARK:- ACTIONS

    @IBAction func doneBtnFromKeyboardClicked (sender: Any) {
        self.view.endEditing(true)
    }
    
    @IBAction func dismissKeyboard(gesture: UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    //MARK:- IMAGE PICKER
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let selectedImage = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
//        if (index < optionImages.count){
//            for _ in index...optionImages.count{
//                self.optionImages.append(UIImage())
//            }
//        }
//        else {
//            for _ in optionImages.count...index{
//                self.optionImages.append(UIImage())
//            }
//        }
        self.optionImages[self.index] = selectedImage
        picker.dismiss(animated: true, completion: nil)
        optionsTableView.reloadData()
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: { () -> Void in
            
            self.optionImages[self.index] = image
        })
        optionsTableView.reloadData()
    }
    
    //MARK:- TEXTVIEW DELEGATES
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        let desiredOffset = CGPoint(x: 0, y: optionsTableView.bounds.height - 100)
        mainScrollView.setContentOffset(desiredOffset, animated: true)
        
        textView.text = ""
        textView.textColor = .black
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        let desiredOffset = CGPoint(x: 0, y: optionsTableView.bounds.height - 350)
        mainScrollView.setContentOffset(desiredOffset, animated: true)
        
        if (textView.text.trimmingCharacters(in: .whitespaces).isEmpty){
            
            extraNotesTextView.text = "Extra notes".localized()
            extraNotesTextView.textColor = .gray
        }
    }
    
    @IBAction func continueAction(_ sender: UIButton) {
        self.handleCountinueAction()
    }
    
    
    private func handleCountinueAction(){
        var arrImgs = [UIImage]()
        for img in self.optionImages{
            if let imgs = img{
                arrImgs.append(imgs)
            }
        }
        
//        isProtectionWashCar = false
        if !isProtectionWashCar {
            if self.arrAttachments.count == 0 || self.arrAttachments.count != arrImgs.count{
                self.showDefaultAlert(title: "Attachments".localized(), message: "Please upload all required documents".localized())
                return
            }
        }
        
//        if self.arrAttachments.count == 0 || self.arrAttachments.count != arrImgs.count{
//            self.showDefaultAlert(title: "Attachments".localized(), message: "Please upload all required documents".localized())
//            return
//        }
        
        let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "OrderDetailsViewController") as! OrderDetailsViewController
        self.dataDict["extra_notes"] = self.extraNotesTextView.text ?? ""
        detailsVC.selectedServiceId = self.selectedId
        detailsVC.dictData       = self.dataDict
        detailsVC.arraySelectedServices = arraySelectedServices
        detailsVC.arraySelectedServiceForCarWash = self.arraySelectedServicesForCarWash
        detailsVC.selectedPackgeForCarWash = selectedPackgeForCarWash
        detailsVC.carRentPrice = self.carRentPrice
        detailsVC.optionImages   = arrImgs
        detailsVC.arrAttachments = self.arrAttachments
        detailsVC.bIsCarRent     = self.bIsCarRent
        detailsVC.bIsUsedCar     = self.bIsUsedCar
        detailsVC.bIsReseller    = self.bIsReseller
        detailsVC.bIsInsurance   = self.bIsInsurance
        self.navigationController?.pushViewController(detailsVC, animated: true)
    }
    
}

extension UploadOptionsViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.arrAttachments.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell", for: indexPath) as! UploadOptionTableViewCell
        cell.index = indexPath.row
        cell.delegate = self
        cell.optionLabel.text = self.arrAttachments[indexPath.row].name ?? ""
        
        if (optionImages.count > indexPath.row){
            cell.optionImageView.image = optionImages[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 90
    }
}

extension UploadOptionsViewController: UploadPhotoDelegate {
    
    func uploadPhoto(index: Int) {
        
        self.index = index
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel".localized(), style: .cancel) { _ in
            
        }
        actionSheet.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera".localized(), style: .default)
        { _ in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery".localized(), style: .default)
        { _ in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(deleteActionButton)
        self.present(actionSheet, animated: true, completion: nil)
    }
}
