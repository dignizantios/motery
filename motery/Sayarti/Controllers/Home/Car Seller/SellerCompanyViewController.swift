//
//  sellerCompanyViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/3/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class SellerCompanyViewController: UIViewController , UIScrollViewDelegate {
    
    var isOpened = [serviceCell]()
    private var imageView = UIImageView()
    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var agentImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var mainTableView: CustomTableView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet weak var btnFavorite: UIButton!
    
    var companyImage = UIImage()
    var agentImage   = UIImage()
    var companyName  = ""
    var company      = Company()
    var bIsCarRent   = false
    var carRentAgency  : Agency?
    var carRentCompany : RentCompany?
    var carRentDesc    : CarRentDesc?
    var resellerDesc   : ResellerDesc?
    
    var bIsFavorite    = false
    var bIsUsedCar     = false
    var resellerCompany = ResellerCompany()
    var bIsReseller     = false

//MARK:- VIEW DELEGATE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("company : \(company)")
        
        self.setupView()
        self.fetchAgencyDetails()
        self.fetchResellerDetails()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        imageView.removeFromSuperview()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    private func setupView(){
        self.agentImageView.contentMode = .scaleAspectFit
        
        if(self.bIsCarRent || self.bIsUsedCar){
            
//            if let companyImg = URL.init(string: self.carRentCompany?.company_img ?? ""){
//                imageView.af_setImage(withURL: companyImg)
//            }
            imageView.setImageOfApp(withUrl: self.carRentCompany?.company_img)
            navigationItem.title  = self.carRentCompany?.company_name ?? ""
            descriptionLabel.text = ""
            agentImageView.image  = nil
        }else{
//            if let companyImg = URL.init(string: self.resellerCompany.image ?? ""){
//                imageView.af_setImage(withURL: companyImg)
//            }
//            if let companyImg = URL.init(string: self.resellerCompany.agency?.image ?? ""){
//                agentImageView.af_setImage(withURL: companyImg)
//            }
            
            imageView.setImageOfApp(withUrl: self.resellerCompany.image)
            agentImageView.setImageOfApp(withUrl: self.resellerCompany.agency?.image)
            
            navigationItem.title  = self.resellerCompany.name
            descriptionLabel.text = self.resellerCompany.description
        }
        
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        optionsView.layer.cornerRadius = 25.0
        var x = 0
        while x < 5 {
            let close = serviceCell()
            isOpened.append(close)
            x = x + 1
        }
        mainTableView.tableFooterView = UIView()
        mainTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: mainTableView.frame.size.width, height: 1))
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    
    private func setupUI() {
    
        guard let navigationBar = self.navigationController?.navigationBar else { return }
        
        navigationBar.addSubview(imageView)
        //imageView.layer.cornerRadius = Const.ImageSizeForLargeState / 2
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor, constant: -Const.ImageRightMargin),
            imageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Const.ImageBottomMarginForLargeState),
            imageView.heightAnchor.constraint(equalToConstant: Const.ImageSizeForLargeState),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor)
            ])
    }
    
    private func moveAndResizeImageForPortrait() {
        guard let height = navigationController?.navigationBar.frame.height else { return }
        
        let coeff: CGFloat = {
            let delta = height - Const.NavBarHeightSmallState
            let heightDifferenceBetweenStates = (Const.NavBarHeightLargeState - Const.NavBarHeightSmallState)
            return delta / heightDifferenceBetweenStates
        }()
        
        let factor = Const.ImageSizeForSmallState / Const.ImageSizeForLargeState
        
        let scale: CGFloat = {
            let sizeAddendumFactor = coeff * (1.0 - factor)
            return min(1.0, sizeAddendumFactor + factor)
        }()
        
        // Value of difference between icons for large and small states
        let sizeDiff = Const.ImageSizeForLargeState * (1.0 - factor) // 8.0
        
        let yTranslation: CGFloat = {
            /// This value = 14. It equals to difference of 12 and 6 (bottom margin for large and small states). Also it adds 8.0 (size difference when the image gets smaller size)
            let maxYTranslation = Const.ImageBottomMarginForLargeState - Const.ImageBottomMarginForSmallState + sizeDiff
            return max(0, min(maxYTranslation, (maxYTranslation - coeff * (Const.ImageBottomMarginForSmallState + sizeDiff))))
        }()
        
        let xTranslation = max(0, sizeDiff - coeff * sizeDiff)
        
        imageView.transform = CGAffineTransform.identity
            .scaledBy(x: scale, y: scale)
            .translatedBy(x: xTranslation, y: yTranslation)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        moveAndResizeImageForPortrait()
    }
    

//MARK:-  ======== FETCH RESELLER DETAILS ========

    private func fetchResellerDetails(){
        if !self.bIsReseller { return }
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_RESELLER_DETAILS + "\(self.resellerCompany.id ?? 0)" , bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData) in
            if let jsonData = responseData{
                do {
                    self?.resellerDesc = try JSONDecoder().decode(ResellerDesc.self, from: jsonData)
                    self?.handleResellerDesc()
                    print("Reseller \(self?.resellerDesc)")
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }
    
    private func handleResellerDesc(){
//        if let companyImg = URL.init(string: self.resellerDesc?.company?.image ?? ""){
//            imageView.af_setImage(withURL: companyImg)
//        }
//        if let companyImg = URL.init(string: self.resellerDesc?.agency?.image ?? ""){
//            agentImageView.af_setImage(withURL: companyImg)
//        }
        
        imageView.setImageOfApp(withUrl: self.resellerDesc?.company?.image)
        agentImageView.setImageOfApp(withUrl: self.resellerDesc?.agency?.image)
        
        navigationItem.title  = self.resellerDesc?.company?.name ?? ""
        descriptionLabel.text = self.resellerDesc?.company?.description ?? ""
        
        if let resellerObj = self.resellerDesc{
            if let bIsFavorite = resellerObj.agency?.favorite{
                self.bIsFavorite = bIsFavorite
                self.btnFavorite.setImage(bIsFavorite ? #imageLiteral(resourceName: "addToFavorite") : #imageLiteral(resourceName: "Favorite"), for: .normal)
            }else{
                self.bIsFavorite = false
                self.btnFavorite.setImage(#imageLiteral(resourceName: "Favorite") , for: .normal)
            }
        }
        let x = self.resellerDesc?.categories?.count ?? 0
        
        if x > 0{
            for _ in 0...x{
                let close = serviceCell()
                self.isOpened.append(close)
            }
        }
        self.mainTableView.reloadData()
    }

//MARK:-  ======== FETCH AGENCY DETAILS ========
    
    private func fetchAgencyDetails(){
        
        print("ID : \(self.carRentAgency?.agency_id)")
        
        if !self.bIsCarRent && !self.bIsUsedCar { return }
        
        self.showLoader()
     
        //TODO: - old strURL Yash changes
        let strURL = self.bIsCarRent ? Constants.GET_RENT_CARS + "\(self.carRentCompany?.company_id ?? 0)" + "/\(self.carRentAgency?.agency_id ?? 0)" : Constants.GET_USED_CARS + "\(self.carRentCompany?.company_id ?? 0)" + "/\(self.carRentAgency?.agency_id ?? 0)"
        
    //  let strURL = Constants.GET_RESELLER_DETAILS + "\(self.carRentCompany?.company_id ?? 0)"
        print("strURL  :\(strURL)")
        
        //TODO: - Yash changes
//        old url = Constants.GET_RESELLER_DETAILS + "\(self.resellerCompany.id ?? 0)"
//        old url replace with strURL
        
        
        self.showLoader()
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData) in
            
            if let jsonData = responseData{
                do {
                    
                    //TODO: - Yash changes New
                    self?.carRentDesc = try JSONDecoder().decode(CarRentDesc.self, from: jsonData)
                    self?.handleDescData()
                    //Old
//                    self?.resellerDesc = try JSONDecoder().decode(ResellerDesc.self, from: jsonData)
//                    self?.handleResellerDesc()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }
    
//    private func handleResellerDesc(){
//        if let companyImg = URL.init(string: self.resellerDesc?.company?.image ?? ""){
//            imageView.af_setImage(withURL: companyImg)
//        }
//        if let companyImg = URL.init(string: self.resellerDesc?.agency?.image ?? ""){
//            agentImageView.af_setImage(withURL: companyImg)
//        }
//
//        if let carRentDescObj = self.carRentDesc{
////            if let companyImg = URL.init(string: carRentDescObj.company?.image ?? ""){
////                imageView.af_setImage(withURL: companyImg)
////            }
//
//            imageView.setImageOfApp(withUrl: carRentDescObj.company?.image)
//
//        navigationItem.title  = self.resellerDesc?.company?.name ?? ""
//        descriptionLabel.text = self.resellerDesc?.company?.description ?? ""
//
//        if let resellerObj = self.resellerDesc{
//            if let bIsFavorite = resellerObj.agency?.favorite{
//                self.bIsFavorite = bIsFavorite
//                self.btnFavorite.setImage(bIsFavorite ? #imageLiteral(resourceName: "addToFavorite") : #imageLiteral(resourceName: "Favorite"), for: .normal)
//            }else{
//                self.bIsFavorite = false
//                self.btnFavorite.setImage(#imageLiteral(resourceName: "Favorite") , for: .normal)
//            }
//        }
//        let x = self.resellerDesc?.categories?.count ?? 0
//
//        if x > 0{
//            for _ in 0...x{
//                let close = serviceCell()
//                self.isOpened.append(close)
//            }
//        }
//        self.mainTableView.reloadData()
//    }

//MARK:-  ======== FETCH AGENCY DETAILS ========
    
//    private func fetchAgencyDetails(){
//        if !self.bIsCarRent && !self.bIsUsedCar { return }
//
//        self.showLoader()
//        let strURL = self.bIsCarRent ? Constants.GET_RENT_CARS + "\(self.carRentCompany?.company_id ?? 0)" + "/\(self.carRentAgency?.agency_id ?? 0)" : Constants.GET_USED_CARS + "\(self.carRentCompany?.company_id ?? 0)" + "/\(self.carRentAgency?.agency_id ?? 0)"
//
//        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData) in
//
//            if let jsonData = responseData{
//                do {
//                    self?.carRentDesc = try JSONDecoder().decode(CarRentDesc.self, from: jsonData)
//                    self?.handleDescData()
//                }catch{
//                    print("Error : \(error.localizedDescription)")
//                }
//            }
//            self?.hideLoader()
//        }
//    }
    
    private func handleDescData(){
        
        if let carRentDescObj = self.carRentDesc{
            if let companyImg = URL.init(string: carRentDescObj.company?.image ?? ""){
                imageView.af_setImage(withURL: companyImg)
            }
            navigationItem.title  = carRentDescObj.company?.name ?? ""
            
            if let bIsFavorite = carRentDescObj.agency?.favorite{
                self.bIsFavorite = bIsFavorite
                self.btnFavorite.setImage(bIsFavorite ? #imageLiteral(resourceName: "addToFavorite") : #imageLiteral(resourceName: "Favorite"), for: .normal)
            }else{
                self.bIsFavorite = false
                self.btnFavorite.setImage(#imageLiteral(resourceName: "Favorite") , for: .normal)
            }
            self.agentImageView.contentMode = .scaleAspectFit
//            if let imgURL = URL.init(string: carRentDescObj.agency?.image ?? ""){
//                self.agentImageView.af_setImage(withURL: imgURL)
//            }
            
            self.agentImageView.setImageOfApp(withUrl: carRentDescObj.agency?.image)
            descriptionLabel.text = carRentDescObj.agency?.description ?? ""
            self.mainTableView.reloadData()
        }
    }
    
    private func setCompanyFavorite(){
        self.showLoader()
        
        var companyID  = self.carRentDesc?.company?.id ?? 0
        var agencyID   = self.carRentDesc?.agency?.id ?? 0
        var bIsPostReq = true
        
        var strURL = self.bIsCarRent ? Constants.POST_RENT_FAVORITES : Constants.POST_USED_FAVORITES

        if self.bIsReseller{
            companyID  = self.resellerDesc?.company?.id ?? 0
            agencyID   = self.resellerDesc?.agency?.id  ?? 0
            strURL     = Constants.GET_RESELLER_FAVORITES + "\(companyID)/\(agencyID)"
            bIsPostReq = false
        }
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: bIsPostReq, bIsShowAlert: true, parameter: ["company_id":"\(companyID)","agency_id":"\(agencyID)"]) { [weak self] (responseData) in
            
            if let jsonData = responseData {
                print("JSON : ",jsonData.debugDescription)
                self?.bIsFavorite = !(self?.bIsFavorite ?? false)
                self?.btnFavorite.setImage(self?.bIsFavorite ?? false ? #imageLiteral(resourceName: "addToFavorite") : #imageLiteral(resourceName: "Favorite"), for: .normal)
            }
            
            self?.hideLoader()
        }
    }
    
    private func handleCallAction(){
        self.showLoader()
        var comapanyID = Int()
        var agencyID   = Int()
        
//        let strURL = self.bIsCarRent ? Constants.POST_RENT_CALL : Constants.POST_USED_CALL
        
        //TODO:- Change JD
        
        var strURL = String()
        
        if bIsCarRent {
            comapanyID = self.carRentDesc?.company?.id ?? 0
            agencyID   = self.carRentDesc?.agency?.id ?? 0
            strURL = Constants.POST_RENT_CALL
        } else if bIsUsedCar {
            comapanyID = self.carRentDesc?.company?.id ?? 0
            agencyID   = self.carRentDesc?.agency?.id ?? 0
            strURL = Constants.POST_USED_CALL
        } else if bIsReseller {
            comapanyID = self.resellerDesc?.company?.id ?? 0
            agencyID   = self.resellerDesc?.agency?.id ?? 0
            strURL = Constants.POST_REQUEST_CALL
        } else {
            return
        }
        
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: true, bIsShowAlert: true, parameter: ["company_id":"\(comapanyID)","agency_id":"\(agencyID)"]) { [weak self] (responseData) in
            
            if let jsonData = responseData {
                print("JSON : ",jsonData.debugDescription)
                do {
                  if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? [String : Any]
                    {
                        self?.showDefaultAlert(title: "", message: json["message"] as! String)
                    }                   
                }
                catch {
                    
                }
            }
            
            self?.hideLoader()
        }
    }
    
    //Vishal
    func showAlertAction() {
        
        let dialogMessage = UIAlertController(title: "Confirm".localized(), message: "Are you sure you want to contact with this dealer?".localized(), preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: { (action) -> Void in
            self.checkLoginStatus { (isLoggedIn) in
                if isLoggedIn {
                    self.handleCallAction()
                }
            }
            print("Ok")
        })
        
        let cancel = UIAlertAction(title: "Cancel".localized(), style: .cancel) { (action) -> Void in }
        
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
//MARK:- ======== ACTION METHODS ==========
    
    @IBAction func callAction(_ sender: UIButton) {
        
        showAlertAction()
        
        //Yash changies
//        checkLoginStatus { (isLoggedIn) in
//            if isLoggedIn {
//                self.handleCallAction()
//            }
//        }
        
        //old
//        if let url = URL(string: "tel://12345678") {
//            UIApplication.shared.open(url, options: [:], completionHandler: nil)
//        }
    }
    
    @IBAction func locationAction(_ sender: UIButton) {
        if self.bIsReseller{
            if let url = URL(string: self.resellerDesc?.agency?.location ?? "") {
                UIApplication.shared.open(url)
            }
        }else{
            var longitude : Double = 47.991876
            var latitude : Double = 29.380192

            if self.bIsCarRent{
                longitude = Double(self.carRentDesc?.agency?.longitude ?? "0.0") ?? 0.0
                latitude  = Double(self.carRentDesc?.agency?.latitude ?? "0.0") ?? 0.0
            }

            if let url = URL(string: "http://maps.google.com/maps?saddr=\(latitude),\(longitude)") {

                UIApplication.shared.open(url)
            }
            else if let url2 = URL(string: "http://maps.apple.com/maps?saddr=\((latitude)),\((longitude))") {
                UIApplication.shared.open(url2)
            }
        }
    }
    
    @IBAction func addToFavoriteAction(_ sender: UIButton) {
        self.checkLoginStatus(complition: { (isLoggedIn) in
            if isLoggedIn {
                self.setCompanyFavorite()
            }
        })
    }
    
    @IBAction func shareAction(_ sender: UIButton) {
        
        let vc = UIActivityViewController(activityItems: ["Mercedes"], applicationActivities: [])
        self.present(vc, animated: true)
    }
}

extension SellerCompanyViewController: UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(self.bIsCarRent || self.bIsUsedCar){
            return 1//self.carRentDesc?.cars?.count ?? 0
        }
        
        return self.resellerDesc?.categories?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if (isOpened[section].open){
            return 2
        }
        else {
            return 1
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "firstCell", for: indexPath) as! OptionTableViewCell
            if(self.bIsCarRent || self.bIsUsedCar){
                cell.optionLabel.text = "Cars"
            }else{
                cell.optionLabel.text = self.resellerDesc?.categories![indexPath.section].category_name ?? ""
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell", for: indexPath) as! CategoriesTableViewCell
            cell.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: UIScreen.main.bounds.width)
            cell.bIsCarRent = self.bIsCarRent
            cell.bIsUsedCar = self.bIsUsedCar
            
            if self.bIsCarRent || self.bIsUsedCar{
                var categoryObj      = Category()
                categoryObj.rentCars = carRentDesc?.cars ?? [RentCar]()
                print("Category rentcars : \(categoryObj.rentCars)")
                print("Category rentcars count : \(categoryObj.rentCars.count)")
                cell.category        = categoryObj
                print("Category : \(categoryObj)")
            }else{
                var categoryObj      = Category()
                print("Data: ", self.resellerDesc?.categories![indexPath.section].cars ?? [ResellerCar]())
                categoryObj.resellerCars = self.resellerDesc?.categories![indexPath.section].cars ?? [ResellerCar]()
                cell.category = categoryObj
                cell.mainCollectionView.reloadData()

            }
            cell.delegate = self
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 0){
            return 50.0
        }
        else {
            return 250.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Index: ", indexPath.section)
        if (indexPath.row == 0) {
            if (!isOpened[indexPath.section].open){
                isOpened[indexPath.section].open = true
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
               // let bottomOffset = CGPoint(x: 0, y: mainScrollView.contentSize.height - mainScrollView.bounds.size.height + 250)
               // mainScrollView.setContentOffset(bottomOffset, animated: true)
            }
            else {
                isOpened[indexPath.section].open = false
                let sections = IndexSet.init(integer: indexPath.section)
                tableView.reloadSections(sections, with: .none)
            }
        }
    }
}

extension SellerCompanyViewController : CategoriesTableViewCellDelegate {
  
    func selectClass(car: ResellerCar) {
        performSegue(withIdentifier: "showClass", sender: car)
    }
    
    func selectRentClass(rentCar: RentCar) {
        performSegue(withIdentifier: "showClass", sender: rentCar)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showClass"{
            if let rentCar = sender as? RentCar{
                let vc = segue.destination as! CarDetailsViewController
                vc.rentCar = rentCar
                vc.agencyName = self.carRentAgency?.agency_name ?? ""
                vc.bIsCarRent = self.bIsCarRent
                vc.bIsUsedCar = self.bIsUsedCar
                
            }else{
                guard let car = sender as? ResellerCar else{return}
                let vc = segue.destination as! CarDetailsViewController
                vc.resellerCar = car
                vc.agencyName  = self.resellerCompany.agency?.name ?? ""
                vc.resellerAgencyId  = "\(self.resellerCompany.agency?.id ?? 0)"
                vc.bIsReseller = true
            }
        }
    }
}
