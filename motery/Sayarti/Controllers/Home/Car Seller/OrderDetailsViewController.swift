//
//  orderDetailsViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/6/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class OrderDetailsViewController: UIViewController, popupDelegate {
    
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var agentLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var servicesLabel: UILabel!
    @IBOutlet weak var servicesTableView: CustomTableView!
    @IBOutlet weak var tblServiceHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contactInfoLabel: UILabel!
    @IBOutlet weak var contactInfoTableView: CustomTableView!
    @IBOutlet weak var tblContactInfoHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var totalAmountTableView: CustomTableView!
    @IBOutlet weak var tblTotalAmountHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblPayment: UILabel!
    @IBOutlet weak var vwPayment: UIView!
    @IBOutlet weak var lblPaymentValue: UILabel!
    @IBOutlet weak var btnPaymentSelected: UIButton!
    @IBOutlet weak var lblOnlinePayment: UILabel!
    @IBOutlet weak var btnOnlinePayment: UIButton!
    @IBOutlet weak var stackVwPayment: UIStackView!
    
    @IBOutlet weak var continueButton: UIButton!
    
    //TODO: Vishal
    @IBOutlet weak var vwApplyCoupn: UIView!
    @IBOutlet weak var lblApplyCoupn: UILabel!
    @IBOutlet weak var txtEnterCoupnCode: UITextField!
    @IBOutlet weak var btnApplyCoupn: UIButton!
    @IBOutlet weak var vwCoupnValue: UIView!
    @IBOutlet weak var lblDiscountTitle: UILabel!
    @IBOutlet weak var lblDiscountValue: UILabel!
    @IBOutlet weak var lblFinalAmount: UILabel!
    @IBOutlet weak var lblFinalAmountValue: UILabel!
    
    let services = ["Test drive","Book car"]
    let servicesValue = ["Free","200 KD"]
    let contactInfo = ["Name","Phone number","Email"]
    var contactInfoValue = ["","",""]
    let total = ["Total"]
    let totalValue = ["200 KD"]
    var dictData = [String: Any]()
    var arrServiceTitle = [String]()
    var arrServicePrice = [String]()
    var totalPrice = 0.0
    var optionImages = [UIImage]()
    var arrAttachments = [Attachment]()
    var bIsCarRent = false
    var bIsUsedCar = false
    var bIsReseller = false
    var bIsInsurance = false
    var paymentObj : OrderPayment?
    var bIsOrderDetails = false
    var strAgencyName = ""
    var strCarName    = ""
    var strImageName  = ""
    var userDetails   = User()
    var paymentType = Bool()
    
    var selectedServiceId = [Int]()
     var arraySelectedServices = [ResellerService]()
    
    var arraySelectedServiceForCarWash = [Service]()
    var selectedPackgeForCarWash = Package()
    var carRentPrice = Double()
    var isCashPayment = false
    var checkOrderTypeString = ""
    var strCoupnCode = ""
    var strDiscount = ""
    var strDiscountAmount = 0.0
    
    
//MARK:- VIEW DELEGATES
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("dictData : \(dictData)")
        self.carImageView.image = nil
        continueButton.layer.cornerRadius = 9.0
        self.setupView()
        self.addObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Order details".localized()
        if !isCashPaymnetSelected {
            self.stackVwPayment.isHidden = false
            [btnPaymentSelected, btnOnlinePayment].forEach { (btn) in
                btn?.isUserInteractionEnabled = false
            }
            if self.paymentType {
                self.btnPaymentSelected.isSelected = true
            }
            else {
                self.btnOnlinePayment.isSelected = true
            }
        }
        else {
            self.isCashPayment = false
            self.btnOnlinePayment.isSelected = true
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        isCashPaymnetSelected = true
        [servicesTableView, contactInfoTableView, totalAmountTableView].forEach { (tbl) in
            tbl?.removeObserver(self, forKeyPath: "contentSize")
        }
    }
    
    private func setupView(){
        continueButton.setTitle("Continue".localized(), for: .normal)
        contactInfoLabel.text = "Contact info".localized()
        servicesLabel.text = "Services".localized()
        totalAmountLabel.text = "Total amount".localized()
        lblPayment.text = "Payment".localized()
        lblPaymentValue.text = "Cash Payment".localized()
        lblOnlinePayment.text = "Online Pay".localized()
        
        if self.bIsOrderDetails{
            self.handleOrderDetails()
        }else{
            self.contactInfoValue.removeAll()
            if let name = Constants.getValueFromUserDefault(key: Constants.KEY_USER_NAME) as? String{
                 self.contactInfoValue.append(name)
            }else{
                 self.contactInfoValue.append("")
            }
            if let mobile = Constants.getValueFromUserDefault(key: Constants.KEY_USER_MOBILE) as? String{
                self.contactInfoValue.append(mobile)
            }else{
                self.contactInfoValue.append("")
            }
           
            if let email = Constants.getValueFromUserDefault(key: Constants.KEY_USER_EMAIL) as? String{
                self.contactInfoValue.append(email)
            }else{
                self.contactInfoValue.append("")
            }
            
            self.handleOrderFromCategory()
        }
        
        continueButton.layer.cornerRadius    = 9.0
        servicesTableView.tableFooterView    = UIView(frame: CGRect(x: 0, y: 0, width: servicesTableView.frame.size.width, height: 1))
        contactInfoTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: contactInfoTableView.frame.size.width, height: 1))
        totalAmountTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: totalAmountTableView.frame.size.width, height: 1))
        
        //TODO:- Vishal
        if checkOrderTypeString == "" {
            [vwApplyCoupn, vwCoupnValue].forEach { (vw) in
                vw?.isHidden = true
            }
        }
        
        if isProtectionWashCar {
            if self.strCoupnCode == "" {
                self.vwApplyCoupn.isHidden = false
            }
        }
        else if self.bIsInsurance {
            if self.strCoupnCode == "" {
                self.vwApplyCoupn.isHidden = false
            }
        }
    }
    
    //TODO: Vishal Setup
    func addObserver() {
        [servicesTableView, contactInfoTableView, totalAmountTableView].forEach { (tbl) in
            tbl?.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        }
    }
    
    private func handleOrderFromCategory(){
        
        if self.bIsReseller || self.bIsInsurance{
            let strPrice    = self.dictData["service_price"] as? String ?? ""
        //    self.totalPrice = Double(strPrice.replacingOccurrences(of: " KD", with: "")) ?? 0.0
            
            for i  in 0..<self.arraySelectedServices.count{
                self.arrServiceTitle.append(self.arraySelectedServices[i].name ?? "")
                self.arrServicePrice.append("\(self.arraySelectedServices[i].price ?? 0.0)")
                self.totalPrice += self.arraySelectedServices[i].price ?? 0.0
            }
//            self.arrServiceTitle.append(self.dictData["service_name"] as? String ?? "")
//            self.arrServicePrice.append(strPrice)
        }
        
        if self.bIsInsurance{
            let strPrice     = self.dictData["total_price"] as? Double ?? 0.0
            self.totalPrice += strPrice
        }
        
        if self.bIsCarRent || self.bIsUsedCar || self.bIsReseller || self.bIsInsurance{
            if let agencyName = self.dictData["agency_name"] as? String{
                print(agencyName)
                self.agentLabel.text = agencyName
            }
            if let carName = self.dictData["car_name"] as? String{
                print(carName)
                self.classLabel.text = carName
            }
            
            
            if self.bIsCarRent || self.bIsUsedCar{
                self.totalPrice = self.carRentPrice
            }
            
//            if let carImage = self.dictData["car_image"] as? String{
//                print(carImage)
//                if let carURL = URL.init(string: carImage){
//                    self.carImageView.af_setImage(withURL: carURL)
//                }
//            }
            
            self.carImageView.setImageForCar(withUrl: self.dictData["car_image"] as? String)
            
            if !self.bIsReseller && !self.bIsInsurance{
                self.servicesLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
                self.servicesTableView.heightAnchor.constraint(equalToConstant: 0).isActive = true
            }
            
            return
        }
        
        if let serviceData = self.dictData["service_info"] as? Service{
            let strPrice    = String.init(format: "%.2f KD", (serviceData.price ?? 0.0))
            self.totalPrice = serviceData.price ?? 0.0
            self.arrServiceTitle.append(serviceData.name ?? "")
            self.arrServicePrice.append(strPrice)
            
            self.carImageView.setImageForCar(withUrl: self.dictData["car_image"] as? String)
            
            if let agencyName = self.dictData["car_name"] as? String{
                print(agencyName)
                self.agentLabel.text = agencyName
            }
            if let carName = self.dictData["car_name"] as? String{
                print(carName)
                self.classLabel.text = carName
            }
            
        }else{
            
            self.carImageView.setImageForCar(withUrl: self.dictData["car_image"] as? String)
            
            if let agencyName = self.dictData["car_name"] as? String{
                print(agencyName)
                self.agentLabel.text = agencyName
            }
            if let carName = self.dictData["car_name"] as? String{
                print(carName)
                self.classLabel.text = carName
            }
            
            for i  in 0..<self.arraySelectedServiceForCarWash.count{
                self.arrServiceTitle.append(self.arraySelectedServiceForCarWash[i].name ?? "")
                self.arrServicePrice.append("\(self.arraySelectedServiceForCarWash[i].price ?? 0.0)")
                self.totalPrice += self.arraySelectedServiceForCarWash[i].price ?? 0.0
            }
            
            if self.selectedPackgeForCarWash.name ?? "" != "" {
                self.arrServiceTitle.append(self.selectedPackgeForCarWash.name ?? "")
                self.arrServicePrice.append("\(self.selectedPackgeForCarWash.price ?? 0.0)")
                self.totalPrice += self.selectedPackgeForCarWash.price ?? 0.0
            }
        }
        
        //TODO: - Yash Comment
   //     self.setupView()
    }
    
    private func handleOrderDetails(){
        self.continueButton.isHidden = true
    
        self.agentLabel.text = self.strAgencyName
        self.classLabel.text = self.strCarName
        
        if self.arrServiceTitle.count == 0{
            self.servicesLabel.heightAnchor.constraint(equalToConstant: 0).isActive = true
            self.servicesTableView.heightAnchor.constraint(equalToConstant: 0).isActive = true
        }
        
        self.contactInfoValue.removeAll()
        self.contactInfoValue.append(self.userDetails.fullname ?? "")
        self.contactInfoValue.append(self.userDetails.mobile)
        self.contactInfoValue.append(self.userDetails.email)
        self.contactInfoTableView.reloadData()
        
//        if let carURL = URL.init(string: self.strImageName){
//            self.carImageView.af_setImage(withURL: carURL)
//        }
        self.carImageView.setImageForCar(withUrl: self.strImageName)
        
        if self.checkOrderTypeString == "Protection & Car Wash".localized() || self.checkOrderTypeString == "Car Insurance".localized() {
            
            if self.strCoupnCode != "" {
                self.vwApplyCoupn.isHidden = true
                self.vwCoupnValue.isHidden = false
                
                self.lblDiscountValue.text = self.strDiscount
                let finalPrize = self.totalPrice-self.strDiscountAmount
                self.lblFinalAmountValue.text = String.init(format: "%.2f KD",finalPrize)
            }
            else {
                self.vwApplyCoupn.isHidden = true
                self.vwCoupnValue.isHidden = true
            }
        }
    }
    
    
//MARK:- CONTINUE ACTION
    
    @IBAction func onBtnContinue(_ sender: Any) {
        self.handleContinueAction()
    }
    
    private func handleContinueAction(){
        self.showLoader()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.001) {
            self.convertImageData()
            
            //TODO: Vishal
            self.addCarWashOrder()
//            self.checkPaymentOption()
        }
    }
    
    //TODO: Vishal
    @IBAction func btnPaymentSelectedAction(_ sender: Any) {
        
        if self.btnPaymentSelected.isSelected == true {
            self.isCashPayment = false
            self.btnPaymentSelected.isSelected = false
            self.btnOnlinePayment.isSelected = true
        }
        else {
            self.isCashPayment = true
            self.btnOnlinePayment.isSelected = false
            self.btnPaymentSelected.isSelected = true
        }
    }
    
    @IBAction func btnOnlinePaymentAction(_ sender: Any) {
        
        if self.btnOnlinePayment.isSelected == true {
            self.isCashPayment = true
            self.btnPaymentSelected.isSelected = true
            self.btnOnlinePayment.isSelected = false
        }
        else {
            self.isCashPayment = false
            self.btnPaymentSelected.isSelected = false
            self.btnOnlinePayment.isSelected = true
        }
    }
    
    func checkPaymentOption() {
        let alert = UIAlertController(title: "Payment option".localized(), message: "How to Pay?".localized(), preferredStyle: .alert)
        
        let codAction = UIAlertAction(title: "Cash Payment".localized(), style: .default) { (action) in
            self.isCashPayment = true
            self.addCarWashOrder()
        }
        alert.addAction(codAction)
        
        let onlinePay = UIAlertAction(title: "Online Pay".localized(), style: .default) { action in
            self.isCashPayment = false
            self.addCarWashOrder()
        }
        alert.addAction(onlinePay)
        self.present(alert, animated: true, completion: nil)
    }
    
    //TODO: Vishal
    func moveToSuccess() {
        self.hideLoader()
        self.moveToSuccessScreen()
//        if let arrVC = self.navigationController?.viewControllers {
//            self.hideLoader()
//            for viewController in arrVC {
//
//                if let companyVC = viewController as? HomeViewController {
//                    self.navigationController?.popToViewController(companyVC, animated: true)
//                    break
//                }
//            }
//        }
    }
    
    @IBAction func btnApplyCoupnAction(_ sender: Any) {
        self.txtEnterCoupnCode.resignFirstResponder()
        if (self.txtEnterCoupnCode.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            showAlert(message: "Please enter coupn code", status: "Error".localized())
        }
        else {
            if self.bIsInsurance {
                getCarInsuranceApplyCoupnDataAPI { coupnData in
                    self.setCoupnData(coupnData: coupnData)
                }
            }
            else if isProtectionWashCar {
                self.getCarWashApplyCoupnDataAPI { coupnData in
                    self.setCoupnData(coupnData: coupnData)
                }
            }
        }
    }
    
    func setCoupnData(coupnData: NSDictionary) {
        self.vwCoupnValue.isHidden = false
        if coupnData["type"] as! String == "%" {
            let discount = (self.totalPrice*(coupnData["discount"] as! Double))/100
            self.lblDiscountValue.text = String.init(format: "%.2f KD",discount)
            let prize = self.totalPrice-discount
            self.lblFinalAmountValue.text = String.init(format: "%.2f KD",prize)
            //                    print("Total : \(self.totalPrice)")
        }
        else if coupnData["type"] as! String == "KD" {
            self.lblDiscountValue.text = String.init(format: "%.2f KD",coupnData["discount"] as! Double)
            let prize = self.totalPrice-(coupnData["discount"] as! Double)
            self.lblFinalAmountValue.text = String.init(format: "%.2f KD",prize)
            //                    print("Total : \(self.totalPrice)")
        }
    }
    
//MARK:- CONTINUE ACTION
    private func addCarWashOrder(){
        var requestData = self.dictData
        var strURL      = ""
       
        //REMOVE EXTRA PARAMETERS
        requestData.removeValue(forKey: "car_name")
        requestData.removeValue(forKey: "car_image")
        requestData.removeValue(forKey: "agency_name")
        requestData.removeValue(forKey: "service_info")
        requestData.removeValue(forKey: "package_info")
        requestData.removeValue(forKey: "total_price")
        
        
        
        
        //GET REQUEST URL
        if(self.bIsCarRent){ strURL = Constants.POST_RENT_ORDER } //car rent
        else if(self.bIsUsedCar){ strURL = Constants.POST_USED_ORDER } //used car
        else if(self.bIsReseller){ strURL = Constants.POST_RESELLER_ORDER
            requestData["service_id"] = self.selectedServiceId
        }//reseller
        else if(self.bIsInsurance){ strURL = Constants.POST_INSURANCE_ORDER
            requestData["coupon_code"] = self.txtEnterCoupnCode.text ?? ""
        }//insurance
        else { strURL = Constants.POST_WASH_ADD_ORDER
            requestData["service_id"] = self.selectedServiceId
            requestData["coupon_code"] = self.txtEnterCoupnCode.text ?? ""
        } //car wash
        
        //TODO: Vishal
        if self.isCashPayment {
            requestData["payment_type"] = "cash"
        }
        else {
            requestData["payment_type"] = "payment_getway"
        }
        
//        print("Param: ", requestData)
        //API REQUEST
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: true, bIsShowAlert: true, parameter: requestData) { [weak self] (responseData) in
            
            if let responseData = responseData{
                do {
                    let dictData = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any]
                    
                    //TODO: Vishal
                    if self?.isCashPayment == true {
                        self?.moveToSuccess()
                    } else {
                        if let dictData = dictData{
                            self?.handleOrderSuccess(dictData: dictData)
                        }
                    }
                    
//                    if let dictData = dictData{
//                        self?.handleOrderSuccess(dictData: dictData)
//                    }
                    //self?.moveToSuccessScreen()
                    
                } catch {
                    self?.hideLoader()
                    print(error.localizedDescription)
                }
            }else{
                self?.hideLoader()
            }
        }
    }
    
// <<<< ORDER SUCCESS NEXT TO PAYMENT >>>>
    
    private func handleOrderSuccess(dictData: [String: Any]){
    
        var orderId = 0
        var strURL  = Constants.GET_PAYMENT_ORDER
        
        if self.bIsUsedCar{
            orderId = (dictData["used_car_order_id"] as? Int) ?? 0
            strURL = "\(strURL)\(orderId)/5"
        }else if self.bIsCarRent{
            orderId = (dictData["rent_id"] as? Int) ?? 0
            strURL = "\(strURL)\(orderId)/4"
        }else if self.bIsReseller{
            orderId = (dictData["order_id"] as? Int) ?? 0
            strURL = "\(strURL)\(orderId)/1"
        }else if self.bIsInsurance{
            orderId = (dictData["insurance_id"] as? Int) ?? 0
            strURL = "\(strURL)\(orderId)/3"
        }else{
            orderId = (dictData["car_wash_order_id"] as? Int) ?? 0
            strURL = "\(strURL)\(orderId)/2"
        }
        
        
        //API REQUEST
        APIManager.shared.fetchRequestData(strURL: strURL, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData) in
            
            if let responseData = responseData{
                do {
                    self?.paymentObj = try JSONDecoder().decode(OrderPayment.self, from: responseData)
                    self?.selectPaymentMethod()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }
    
    private func selectPaymentMethod(){
        if self.totalPrice <= 0.0{
            self.hideLoader()
            self.moveToSuccessScreen()
            return
        }
        
        if self.paymentObj?.payment_methods?.count == 0{
            let alert = UIAlertController(title: "Error".localized(), message: "Something went wrong. Please try again!".localized(), preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { action in
                 if let arrVC = self.navigationController?.viewControllers{
                     for viewController in arrVC{
    
                         if let companyVC = viewController as? HomeViewController{
                             self.navigationController?.popToViewController(companyVC, animated: true)
                             break
                         }
                     }
                 }
            }))
            self.present(alert, animated: true, completion: nil)
            return
         }
        
        DispatchQueue.main.async {
            let vc = PopUpViewController()
            var arrTitle = [String]()
            vc.customDelegate = self
            
            if let arrPaymentMethod = self.paymentObj?.payment_methods{
                for title in arrPaymentMethod{
                    arrTitle.append(title.PaymentMethodName ?? "")
                }
            }
            vc.arrTitles = arrTitle
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle   = .coverVertical
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func onBtnDismiss(selectedIndex: Int) {
        if self.paymentObj?.payment_methods?.count ?? 0 > selectedIndex {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                if let paymentObj = self.paymentObj?.payment_methods![selectedIndex]{
                    self.moveToPaymentScreen(url: paymentObj.PaymentMethodUrl ?? "")
                }
            }
        }
    }
    
    private func moveToPaymentScreen(url: String){
        self.title = ""
        let successVC = self.storyboard?.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        successVC.strURL = url
        self.navigationController?.pushViewController(successVC, animated: true)
    }
    
    private func moveToSuccessScreen(){
        let successVC = self.storyboard?.instantiateViewController(withIdentifier: "SuccessViewController") as! SuccessViewController
        self.navigationController?.pushViewController(successVC, animated: true)
    }
    
//MARK:- CONVERT IMAGES TO BASE64
    
    private func convertImageData(){
        var arrImgData = [[String:Any]]()
        
        for i in 0..<self.arrAttachments.count{
            
            if let id = self.arrAttachments[i].id{
                var data = [String: Any]()
                data["id"] = id
                let imgDataObj = self.optionImages[i].resized(withPercentage: 0.3)?
                                .jpegData(compressionQuality:0.0)?.base64EncodedString()
                data["image"] = imgDataObj
                arrImgData.append(data)
            }
        }
        
        if(arrImgData.count > 0){
            self.dictData["attachments"] = arrImgData
        }
    }
    
}

extension OrderDetailsViewController: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        switch tableView {
            
            case servicesTableView:
                return self.arrServiceTitle.count
            
            case contactInfoTableView:
                return contactInfo.count
            
            case totalAmountTableView:
                return total.count
            
            default:
                return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch tableView {
            
        case servicesTableView:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! CarResellerOrderDetailsTableViewCell
            
            cell.titleLabel.text = self.arrServiceTitle[indexPath.row].localized()
            cell.valueLabel.text = String.init(format: "%.2f KD", (Double(self.arrServicePrice[indexPath.row]) ?? 0.0))
            return cell
            
        case contactInfoTableView:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "contactInfoCell", for: indexPath) as! CarResellerOrderDetailsTableViewCell
            cell.titleLabel.text = contactInfo[indexPath.row].localized()
            cell.valueLabel.text = contactInfoValue[indexPath.row]
            return cell
            
        case totalAmountTableView:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "totalCell", for: indexPath) as! CarResellerOrderDetailsTableViewCell
            cell.titleLabel.text = total[indexPath.row].localized()
            cell.valueLabel.text = String.init(format: "%.2f KD",self.totalPrice)
            return cell
            
        default:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "serviceCell", for: indexPath) as! CarResellerOrderDetailsTableViewCell
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

//MARK:- Overide Method
extension OrderDetailsViewController {
    //Vishal Setup
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
           
            self.view.layoutIfNeeded()
            self.view.layoutSubviews()
            
            self.tblServiceHeightConstraint.constant = self.servicesTableView.contentSize.height
            self.tblTotalAmountHeightConstraint.constant = self.totalAmountTableView.contentSize.height
            self.tblContactInfoHeightConstraint.constant = self.contactInfoTableView.contentSize.height
            
            }
        }
}

//MARK: API Setup
extension OrderDetailsViewController {
    
    func getCarWashApplyCoupnDataAPI(completionHandlor:@escaping(NSDictionary)->Void) {
        self.showLoader()
        let param = ["coupon_code" : self.txtEnterCoupnCode.text ?? ""]
        
        APIManager.shared.fetchRequestData(strURL: Constants.POST_CAR_WASH, bIsPostRequest: true, bIsShowAlert: true, parameter: param) { [weak self](response) in
            guard let dataResponse = response else {
                print("Error")
                self?.hideLoader()
                return
            }
            do  {
                let jsonData = try JSONSerialization.jsonObject(with: dataResponse, options: [])
                print("Data: \(jsonData)")
                self?.hideLoader()
                completionHandlor(jsonData as! NSDictionary)
            }
            catch {
                print("Error: \(error.localizedDescription)")
            }
            self?.hideLoader()
        }
    }
  
    func getCarInsuranceApplyCoupnDataAPI(completionHandlor:@escaping(NSDictionary)->Void) {
        self.showLoader()
        let param = ["coupon_code" : self.txtEnterCoupnCode.text ?? ""]
        
        APIManager.shared.fetchRequestData(strURL: Constants.POST_CAR_INSURANCE, bIsPostRequest: true, bIsShowAlert: true, parameter: param) { [weak self](response) in
            guard let dataResponse = response else {
                print("Error")
                self?.hideLoader()
                return
            }
            do  {
                let jsonData = try JSONSerialization.jsonObject(with: dataResponse, options: [])
                print("Data: \(jsonData)")
                self?.hideLoader()
                completionHandlor(jsonData as! NSDictionary)
            }
            catch {
                print("Error: \(error.localizedDescription)")
            }
            self?.hideLoader()
        }
    }
}
