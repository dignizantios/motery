//
//  FavoritesViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 2/28/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Localize_Swift
import AlamofireImage

class FavoritesViewController: UIViewController {
    
    let images = ["class1","wash1","item1","item2","item3"]
    let titles = ["Car reseller","Protection & Car Wash","Car insurance"]
    var favAllCategory  = Favorites()
    var arrAllFavorites = [String]()
    var refreshController = UIRefreshControl()
    
    @IBOutlet weak var lblNoFav: UILabel!
    @IBOutlet weak var favoriteCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.lblNoFav.text = "No favorites".localized()
        
        refreshController.addTarget(self, action: #selector(fetchFavoritesList), for: .valueChanged)
        favoriteCollectionView.addSubview(refreshController)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = "Favorites".localized()
        
        if #available(iOS 13.0, *) {
             Constants.APP_DELEGATE.window?.overrideUserInterfaceStyle = .light
             let app = UINavigationBarAppearance()
             app.backgroundColor = .white
             self.navigationController?.navigationBar.scrollEdgeAppearance = app
        }
        
        if Localize.currentLanguage() == "ar"{
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
        }else{
            navigationController?.view.semanticContentAttribute = .forceLeftToRight
        }
        
        self.checkLoginStatus { (isLoggedIn) in
            if isLoggedIn && self.arrAllFavorites.count == 0{
                self.fetchFavoritesList()
            }
        }
    }
    
    @objc func showLoginVC(){
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginVC.modalPresentationStyle = .fullScreen
        loginVC.bIsForGuestUser = true
        //let navVC = UINavigationController.init(rootViewController: )

        self.tabBarController?.navigationController?.pushViewController(loginVC, animated: true)
    }
    
//MARK:- ============= FETCH FAVORITES LIST ==============
    
    @objc private func fetchFavoritesList(){
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_FAVORITES_LIST, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData) in
            
            if let jsonData = responseData{
                do {
                    self?.refreshController.endRefreshing()
                    self?.arrAllFavorites = []
                    
                    self?.favAllCategory = try JSONDecoder().decode(Favorites.self, from: jsonData)
                    if (self?.favAllCategory.carReseller?.count ?? 0) > 0{
                        self?.arrAllFavorites.append("Car Reseller".localized())
                    }
                    if (self?.favAllCategory.carWash?.count ?? 0) > 0{
                        self?.arrAllFavorites.append("Protection & Car Wash".localized())
                    }
                    if (self?.favAllCategory.insurance?.count ?? 0) > 0{
                        self?.arrAllFavorites.append("Car Insurance".localized())
                    }
                    if (self?.favAllCategory.carRent?.count ?? 0) > 0{
                        self?.arrAllFavorites.append("Car Rent".localized())
                    }
                    if (self?.favAllCategory.usedCar?.count ?? 0) > 0{
                        self?.arrAllFavorites.append("Used Car".localized())
                    }
                    if self?.arrAllFavorites.count ?? 0 > 0{
                        self?.lblNoFav.isHidden = true
                    }else{
                        self?.lblNoFav.isHidden = false
                    }
                    self?.favoriteCollectionView.reloadData()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            
            self?.hideLoader()
        }
    }
    
    
    
}

extension FavoritesViewController: UICollectionViewDelegateFlowLayout , UICollectionViewDataSource , UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let strSectionName = self.arrAllFavorites[section]
        
        if strSectionName == "Car Reseller".localized(){
            return self.favAllCategory.carReseller?.count ?? 0
        }else if strSectionName == "Protection & Car Wash".localized(){
            return self.favAllCategory.carWash?.count ?? 0
        }else if strSectionName == "Car Insurance".localized(){
            return self.favAllCategory.insurance?.count ?? 0
        }else if strSectionName == "Car Rent".localized(){
            return self.favAllCategory.carRent?.count ?? 0
        }else if strSectionName == "Used Car".localized(){
            return self.favAllCategory.usedCar?.count ?? 0
        }
        return 0
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.arrAllFavorites.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "favoriteCell", for: indexPath) as! WashCompanyCollectionViewCell
        cell.mainView.layer.cornerRadius = 9.0
        cell.companyImageView.image = UIImage(named: images[indexPath.row])

        let strSectionName = self.arrAllFavorites[indexPath.section]
        var strImgURL      = ""
        
        if strSectionName == "Car Reseller".localized(){
            let carReseller = self.favAllCategory.carReseller?[indexPath.row].company
            cell.companyNameLabel.text = carReseller?.name ?? ""
            strImgURL = carReseller?.image ?? ""

        }else if strSectionName == "Protection & Car Wash".localized(){
            let carWash = self.favAllCategory.carWash?[indexPath.row].store
            cell.companyNameLabel.text = carWash?.name ?? ""
            strImgURL = carWash?.image ?? ""

        }else if strSectionName == "Car Insurance".localized(){
            let carInsurance = self.favAllCategory.insurance?[indexPath.row].company
            cell.companyNameLabel.text = carInsurance?.name ?? ""
            strImgURL = carInsurance?.image ?? ""

        }else if strSectionName == "Car Rent".localized(){
            let carRent = self.favAllCategory.carRent?[indexPath.row].company
            cell.companyNameLabel.text = carRent?.name ?? ""
            strImgURL = carRent?.image ?? ""

        }else if strSectionName == "Used Car".localized(){
            let carUsed = self.favAllCategory.usedCar?[indexPath.row].company
            cell.companyNameLabel.text = carUsed?.name ?? ""
            strImgURL = carUsed?.image ?? ""
        }
        
        cell.companyImageView.image = nil
        
//        if let carURL = URL.init(string: strImgURL){
//            cell.companyImageView.af_setImage(withURL: carURL)
//        }
        
        cell.companyImageView.setImageOfApp(withUrl: strImgURL)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.bounds.width / 2
        return CGSize(width: width, height: 200)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionHeader", for: indexPath) as? FavoriateCollectionReusableView{
            sectionHeader.sectionTitleLabel.text = self.arrAllFavorites[indexPath.section].localized()
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let strSectionName = self.arrAllFavorites[indexPath.section]
        
        if strSectionName == "Car Reseller".localized(){
            self.performSegue(withIdentifier: "carReseller", sender: indexPath)
        }else if strSectionName == "Protection & Car Wash".localized(){
            self.performSegue(withIdentifier: "carWash", sender: indexPath)
        }else if strSectionName == "Car Insurance".localized(){
            self.performSegue(withIdentifier: "carInsurance", sender: indexPath)
        }else if strSectionName == "Car Rent".localized(){
            self.performSegue(withIdentifier: "carReseller", sender: indexPath)
        }else if strSectionName == "Used Car".localized(){
             self.performSegue(withIdentifier: "carReseller", sender: indexPath)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let nIndex = sender as? IndexPath else {return}
        let strSectionName = self.arrAllFavorites[nIndex.section]
        
        switch segue.identifier {
            case "carReseller":
                let vc = segue.destination as! SellerCompanyViewController
                
                if strSectionName == "Car Reseller".localized(){
                    vc.bIsReseller     = true
                    var resllerCompany = (self.favAllCategory.carReseller?[nIndex.row].company) ?? ResellerCompany()
                    
                    print("resellerCpmany L: \(resllerCompany)")
                    
                    var agencyObj = ResellerAgency()
                    agencyObj.id = self.favAllCategory.carReseller?[nIndex.row].agency?.id
                    agencyObj.name = self.favAllCategory.carReseller?[nIndex.row].agency?.name ?? ""
                    resllerCompany.agency = agencyObj
                    vc.resellerCompany = resllerCompany
                }else if strSectionName == "Car Rent".localized(){
                    let rentObj = (self.favAllCategory.carRent?[nIndex.row])
                    var rentCompObj = RentCompany()
                    rentCompObj.company_id   = rentObj?.company?.id
                    rentCompObj.company_img  = rentObj?.company?.image
                    rentCompObj.company_name = rentObj?.company?.name
                    vc.carRentCompany  = rentCompObj
                    var rentAgency     = Agency()
                    rentAgency.agency_id  = rentObj?.agency?.id
                    rentAgency.agency_img = rentObj?.agency?.image
                    rentAgency.agency_name = rentObj?.agency?.name
                    vc.carRentAgency   = rentAgency
                    vc.bIsCarRent      = true
                }else if strSectionName == "Used Car".localized(){
                    let rentObj = (self.favAllCategory.usedCar?[nIndex.row])
                    var rentCompObj = RentCompany()
                    rentCompObj.company_id   = rentObj?.company?.id
                    rentCompObj.company_img  = rentObj?.company?.image
                    rentCompObj.company_name = rentObj?.company?.name
                    vc.carRentCompany  = rentCompObj
                    var rentAgency     = Agency()
                    rentAgency.agency_id  = rentObj?.agency?.id
                    rentAgency.agency_img = rentObj?.agency?.image
                    rentAgency.agency_name = rentObj?.agency?.name
                    vc.carRentAgency   = rentAgency
                    vc.bIsUsedCar      = true
                }
            case "carWash":
                if let destinationVC = segue.destination as? WashCompanyDetailsViewController{
                    let selectedStore     = self.favAllCategory.carWash?[nIndex.row].store ?? Store()
                    destinationVC.storeId = selectedStore.id ?? 0
                    destinationVC.strImgLogo = selectedStore.image ?? ""
                    destinationVC.title   = selectedStore.name ?? ""
               }
            
            case "carInsurance":
                let insuranceVC = segue.destination as! InsuranceCompanyDetailsViewController
                insuranceVC.insuranceCompany = self.favAllCategory.insurance?[nIndex.row].company ?? InsuranceCompany()
          
        default:
            break
        }
    }
}
