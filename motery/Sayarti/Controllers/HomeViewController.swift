//
//  HomeViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 2/28/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Localize_Swift

class HomeViewController: UIViewController , UIGestureRecognizerDelegate {
    
    //MARK:- VARIABLES
    var offers = [Offer]()
    var ads = [Ad]()
    var animate: Bool = true
    var categories = [HomeCategory]()
    
    @IBOutlet weak var homeTableView: UITableView!
    @IBOutlet weak var welcomeLabel: UILabel!
    var selectedIndex = 0
    
    /// Use this variable to indecate weather all the  api's done loading
    var numberOfApiResponses: Int = 0 {
        didSet{
//            if numberOfApiResponses == 3 {
            if numberOfApiResponses == 1 {  //Vishal
                self.hideLoader()
                animateHome()
            }
        }
    }
    

    //MARK:- VIEW DELEGATES

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showLoader()
//        getOffers()   //VISHAL
//        getAds()      //VISHAL
        getCategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
        if #available(iOS 13.0, *) {
           Constants.APP_DELEGATE.window?.overrideUserInterfaceStyle = .light
           let app = UINavigationBarAppearance()
           app.backgroundColor = .white
           self.navigationController?.navigationBar.scrollEdgeAppearance = app
        }
        
       // welcomeLabel.text = "Welcome to Motery".localized()

        if (animate){
            animate = false
            self.welcomeLabel.transform = .init(translationX: self.view.frame.width, y: 0)
            animateHome()
        }
        self.navigationController?.navigationBar.isHidden = true
        
        if Localize.currentLanguage() == "ar"{
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
        }else{
            navigationController?.view.semanticContentAttribute = .forceLeftToRight
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
   
    fileprivate func getOffers(){
        Network.shared.makeHttpRequest(model: [Offer](), method: .get, APIName: "user/getOffers", parameters: nil) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let offers):
                    self.offers = offers
                    self.numberOfApiResponses += 1
                case .failure(let error):
                    self.showAlert(message: error.errorDescription ?? "", status: "")
                }
            }
        }
    }
    
    fileprivate func getAds(){
        Network.shared.makeHttpRequest(model: [Ad](), method: .get, APIName: "user/getAds", parameters: nil) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let ads):
                    self.ads = ads
                    self.numberOfApiResponses += 1
                case .failure(let error):
                    self.showAlert(message: error.errorDescription ?? "", status: "")
                }
            }
        }
        
    }
    
    fileprivate func getCategories(){
        Network.shared.makeHttpRequest(model: [HomeCategory](), method: .get, APIName: "user/getHomePageCategories", parameters: nil) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let categories):
                    self.categories = categories
                    self.numberOfApiResponses += 1
                case .failure(let error):
                    self.showAlert(message: error.errorDescription ?? "", status: "")
                }
            }
        }
        
    }
    
    
    func animateHome(){
        
        homeTableView.reloadData()
        
        let cells = homeTableView.visibleCells
        let tableViewHeight = homeTableView.bounds.size.height
        
        for cell in cells {
            cell.transform = CGAffineTransform(translationX: 0, y: tableViewHeight)
        }
        
        var delayCounter = 0
        
        for cell in cells {
            
            UIView.animate(withDuration: 1.3, delay: Double(delayCounter) * 0.05, usingSpringWithDamping: 0.8 , initialSpringVelocity: 0 ,options: .curveEaseInOut, animations: {
                
                cell.transform = CGAffineTransform.identity
            }, completion: nil)
            
            delayCounter += 1
        }
        
        UIView.animate(withDuration: 0.6, delay: Double(delayCounter) * 0.05, usingSpringWithDamping: 0.8 , initialSpringVelocity: 0 ,options: .curveEaseInOut, animations: {
            
            self.welcomeLabel.transform = .identity
        }, completion: nil)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
}

extension HomeViewController: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 0){
//            let value: CGFloat =  (( view.frame.width - 10 ) * 0.7) + 25
            let value: CGFloat =  ((view.frame.width) * 0.6)
//            return value
            return 0    //VISHAL
        }
            
        else if indexPath.row == 1 {
            
            print("Table height : \(((view.bounds.width) * 0.6))")
//            return ((view.frame.width) * 0.5) + 56
            return 0    //VISHAL
        }
            
        else {
            let value: CGFloat =  (( view.frame.width - 10 ) * 0.6) + 8
            return value
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (indexPath.row == 0 ){
            let cell = tableView.dequeueReusableCell(withIdentifier: "firstCell", for: indexPath) as! HomeFirstTableViewCell
            cell.ads = ads
            cell.mainCollectionView.reloadData()
            cell.pageController.numberOfPages = ads.count
            cell.delegate = self
            return cell
        }
            
        else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "secondCell", for: indexPath) as! BestSellerTableViewCell
            cell.offers = offers
            cell.bestSellerCollectionView.reloadData()
            cell.delegate = self
            return cell
        }
            
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "thirdCell", for: indexPath) as! HomeSecondTableViewCell
            cell.category = categories[indexPath.row - 2]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        
        switch indexPath.row{
            
            case 0:
                break
            case 2:
                self.performSegue(withIdentifier: "showBuy", sender: self)
            case 3:
                self.performSegue(withIdentifier: "showWash", sender: self)
            case 4:
                self.performSegue(withIdentifier: "showInsurance", sender: self)
            case 5:
                self.performSegue(withIdentifier: "showBuy", sender: self)
            case 6:
                self.performSegue(withIdentifier: "showBuy", sender: self)
            default:
                break
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "goToDetails"{
            if let senderIndex = sender as? Int{

                let vc = segue.destination as! SellerCompanyViewController
                if self.offers[senderIndex].module_type == 1{ //reseller
                    vc.bIsReseller = true
                    
                    var company = ResellerCompany()
                    var agencyObj = ResellerAgency()
                    agencyObj.id = self.offers[senderIndex].agency_id ?? 0
                    agencyObj.name = self.offers[senderIndex].name ?? ""
                    company.id = self.offers[senderIndex].company_id
                    company.agency = agencyObj
                    vc.resellerCompany = company
                }else if self.offers[senderIndex].module_type == 4{ //car rent
                    vc.bIsCarRent = true
                    var company   = RentCompany()
                    var agencyObj = Agency()
                    agencyObj.agency_id = self.offers[senderIndex].agency_id ?? 0
                    company.company_id  = self.offers[senderIndex].company_id ?? 0
                    vc.carRentCompany   = company
                    vc.carRentAgency    = agencyObj
                    
                }else if self.offers[senderIndex].module_type == 5{ //used car
                    vc.bIsUsedCar = true
                    var company   = RentCompany()
                    var agencyObj = Agency()
                    agencyObj.agency_id = self.offers[senderIndex].agency_id ?? 0
                    company.company_id  = self.offers[senderIndex].company_id ?? 0
                    vc.carRentCompany   = company
                    vc.carRentAgency    = agencyObj
                }
                
            }
        }
        
        if segue.identifier == "showOfferWash"{
            if let senderIndex = sender as? Int{
                
                let vc = segue.destination as! WashCompanyDetailsViewController
                if self.offers[senderIndex].module_type == 2{ //car wash
                    vc.storeId = self.offers[senderIndex].store_id ?? 0
                }
            }
        }
        
        if segue.identifier == "showOfferInsurance"{
            if let senderIndex = sender as? Int{
                
                let vc = segue.destination as! InsuranceCompanyDetailsViewController
                if self.offers[senderIndex].module_type == 3{ //reseller
                    var insuranceC = InsuranceCompany()
                    insuranceC.ins_comp_id = self.offers[senderIndex].company_id ?? 0
                    vc.insuranceCompany = insuranceC
                }
            }
        }
        
        if segue.identifier == "showBuy" && self.selectedIndex == 5{
            isProtectionWashCar = false
            let vc = segue.destination as! CompaniesViewController
            vc.strTitle = self.categories[self.selectedIndex - 2].name
            vc.bIsCarRent = true
        }
        
        if segue.identifier == "showBuy" && self.selectedIndex == 6{
            isProtectionWashCar = false
            let vc = segue.destination as! CompaniesViewController
            vc.strTitle = self.categories[self.selectedIndex - 2].name
            vc.bIsUsedCar = true
        }
        
        if segue.identifier == "showBuy" && self.selectedIndex == 2{
            isProtectionWashCar = false
            let vc = segue.destination as! CompaniesViewController
            vc.strTitle = self.categories[self.selectedIndex - 2].name
            vc.bIsReSeller = true
        }
        
        if segue.identifier == "showWash" && self.selectedIndex == 3{
            //TODO: Vishal
            isProtectionWashCar = true
            let vc = segue.destination as! WashCompaniesViewController
            vc.strTitle = self.categories[self.selectedIndex - 2].name
        }
        
        if segue.identifier == "showInsurance" && self.selectedIndex == 4{
            isProtectionWashCar = false
            let vc = segue.destination as! InsuranceCompaniesViewController
            vc.strTitle = self.categories[self.selectedIndex - 2].name
        }
        
    }
}

extension HomeViewController: BestSellerTableViewCellDelegate {
    
    func goToDetails(index: Int) {
        //if segue.identifier == "showOfferWash"{
        
        if self.offers[index].module_type == 1 || self.offers[index].module_type == 4 || self.offers[index].module_type == 5 { //reseller
            performSegue(withIdentifier: "goToDetails", sender: index)

        }else if self.offers[index].module_type == 2{ //car wash
            performSegue(withIdentifier: "showOfferWash", sender: index)
        }else if self.offers[index].module_type == 3{ //insurance
            performSegue(withIdentifier: "showOfferInsurance", sender: index)
        }
    }
    
    
}


extension HomeViewController: adsDelegate {
    
    func didSelectAd(index: Int) {
        print("selected index : \(index)")
    }
}
