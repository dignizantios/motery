//
//  CalculatorViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 4/1/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//


import UIKit
import Localize_Swift

class CalculatorViewController: UIViewController , UITextFieldDelegate {
    
    let years = ["1","2","3","4","5"]
    let titles = ["Profit","Installments","Total"]
    var prices = ["0 ","0 ","0 "]
    var selectedYear = 1
    let rateOfInterest = [0.033,0.064,0.096,0.1285,0.1625]
    var strPrice = String()
    
    @IBOutlet weak var yearsCollectionView: UICollectionView!
    @IBOutlet weak var yearsLabel: UILabel!
    @IBOutlet weak var calculateButton: UIButton!
    @IBOutlet weak var paymentTableView: CustomTableView!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var downPaymentTextField: UITextField!
    @IBOutlet weak var installmentPeriodLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calculateButton.layer.cornerRadius = 9.0
        
        let ViewForDoneButtonOnKeyboard = UIToolbar()
        ViewForDoneButtonOnKeyboard.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let btnDoneOnKeyboard = UIBarButtonItem(title: "Done".localized(), style: .done, target: self, action: #selector(self.doneBtnFromKeyboardClicked))
        btnDoneOnKeyboard.tintColor = .black
        ViewForDoneButtonOnKeyboard.backgroundColor = .lightGray
        ViewForDoneButtonOnKeyboard.setItems([flexibleSpace,btnDoneOnKeyboard], animated: true)
        priceTextField.inputAccessoryView = ViewForDoneButtonOnKeyboard
        downPaymentTextField.inputAccessoryView = ViewForDoneButtonOnKeyboard
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        yearsLabel.text = "1" + " Year".localized()
        title = "Calculator".localized()
        priceTextField.placeholder = "Car price".localized()
        downPaymentTextField.placeholder = "Down payment".localized()
        installmentPeriodLabel.text = "Installment period".localized()
        calculateButton.setTitle("Calculate".localized(), for: .normal)
        priceTextField.text = strPrice
        if Localize.currentLanguage() == "ar"{
            priceTextField.textAlignment = .right
            downPaymentTextField.textAlignment = .right
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
        }else{
            navigationController?.view.semanticContentAttribute = .forceLeftToRight
        }
    }
    
    @IBAction func doneBtnFromKeyboardClicked (sender: Any) {
        
        self.view.endEditing(true)
    }
    
    @IBAction func continueAction(_ sender: UIButton) {
        
        self.calculateAmount()
    }
    
    @IBAction func restoreAction(_ sender: UIBarButtonItem) {
        
        priceTextField.text = ""
        downPaymentTextField.text = ""
        paymentTableView.isHidden = true
        yearsLabel.text = "1" + " Year".localized()
        yearsCollectionView.reloadData()
    }
    
    private func calculateAmount(){
        if priceTextField.text == ""{
            self.showAlert(message: "Please enter your car price".localized(), status: "Car price".localized())
            return
        }
        
        //TODO: - Yash Changes
        
        print("language : \(Localize.currentLanguage())")

        var strDownPayment = ""
        if Localize.currentLanguage() == "ar"{
            strDownPayment = numberArabicToEnglish(stringValue: downPaymentTextField.text ?? "0")
        }else{
            strDownPayment = numberArabicToEnglish(stringValue: downPaymentTextField.text ?? "0")
        }
        let downPayment = Double(strDownPayment) ?? 0.0
        
        print("downPayment :\(downPayment)")
        
        //Price change
        var strPrice = ""
        
        if Localize.currentLanguage() == "ar"{
            strPrice = numberArabicToEnglish(stringValue: priceTextField.text ?? "0")
        }else{
            strPrice = numberArabicToEnglish(stringValue: priceTextField.text ?? "0")
        }
        
        print("strPrice:\(strPrice)")
        
        prices.removeAll()
        if var amount = Double(strPrice){
            
            amount -= downPayment
            let profit = self.rateOfInterest[selectedYear - 1] * amount
            let subTotal = (profit + amount)/Double(selectedYear * 12)
            let total = profit + amount
            
            //TODO: - Yash Changes
            
            let strProfit = String.init(format: "%.2f ", profit)
            let strSubTotal = String.init(format: "%.2f ", subTotal)
            let strTotal = String.init(format: "%.2f ", total)
            
            if Localize.currentLanguage() == "ar"{
               self.prices.append(numberEnglishToArabic(stringValue: strProfit))
               self.prices.append(numberEnglishToArabic(stringValue: strSubTotal))
               self.prices.append(numberEnglishToArabic(stringValue: strTotal))
            }else{
               self.prices.append(numberArabicToEnglish(stringValue: strProfit))
               self.prices.append(numberArabicToEnglish(stringValue: strSubTotal))
               self.prices.append(numberArabicToEnglish(stringValue: strTotal))
            }
           
            paymentTableView.reloadData()
            paymentTableView.isHidden = false
        }
        
    }
    
    
    
}

extension CalculatorViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return years.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "yearCell", for: indexPath) as! YearCollectionViewCell
        cell.yearLabel.text = years[indexPath.row]
        cell.yearLabel.layer.cornerRadius = 18
        
        if (indexPath.row == 0 ){
            cell.yearLabel.textColor = .white
            cell.yearLabel.backgroundColor = Constants.appColor
        }
        else {
            cell.yearLabel.textColor = .black
            cell.yearLabel.backgroundColor = Constants.lightGray
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if (years.count < 8){
            
            let totalCellWidth = 45 * years.count
            let totalSpacingWidth = 0 * (years.count - 1)
            
            let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            
            return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        }
        else {
            return UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 45, height: 45)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let firstIndexPath = IndexPath(row: 0, section: 0)
        let cell2 = collectionView.cellForItem(at: firstIndexPath) as! YearCollectionViewCell
        cell2.yearLabel.textColor = .black
        cell2.yearLabel.backgroundColor = Constants.lightGray
        
        let cell = collectionView.cellForItem(at: indexPath) as! YearCollectionViewCell
        cell.yearLabel.backgroundColor = Constants.appColor
        cell.yearLabel.textColor = .white
        self.selectedYear = Int(years[indexPath.row]) ?? 1
        
        if (Int(years[indexPath.row]) == 1){
            yearsLabel.text = years[indexPath.row] + " Year".localized()
        }
        else {
            yearsLabel.text = years[indexPath.row] + " Years".localized()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! YearCollectionViewCell
        cell.yearLabel.backgroundColor = Constants.lightGray
        cell.yearLabel.textColor = .black
    }
}

extension CalculatorViewController: UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CalculatorCell", for: indexPath) as! CalculatorTableViewCell
        cell.priceLabel.text = prices[indexPath.row] + "KD".localized()
        cell.titleLabel.text = titles[indexPath.row].localized()
        cell.mainView.layer.borderColor = UIColor.white.cgColor
        cell.mainView.layer.borderWidth = 1
        
        if (Localize.currentLanguage() == "ar"){
            cell.priceLabel.textAlignment = .left
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 40
    }
}
