//
//  LoginOrRegisterViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/6/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class LoginOrRegisterViewController: UIViewController {
    
    @IBOutlet weak var loginBTN: UIButton!
    @IBOutlet weak var registerBTN: UIButton!
    @IBOutlet weak var SkipButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginBTN.setTitle("Login".localized(), for: .normal)
        registerBTN.setTitle("Register".localized(), for: .normal)
        self.SkipButton.setTitle("Skip".localized(), for: .normal)
        loginBTN.layer.cornerRadius = 5.0
        registerBTN.layer.cornerRadius = 5.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    @IBAction func btnSkipAction(_ sender: Any) {
        Constants.APP_DELEGATE.setTabbarAsRoot()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.navigationController?.navigationBar.isHidden = true
    }
}
