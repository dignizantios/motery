//
//  selectLanguageViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/6/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Localize_Swift

class SelectLanguageViewController: UIViewController {

    @IBOutlet weak var arabicBTN: UIButton!
    @IBOutlet weak var englishBTN: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        arabicBTN.layer.cornerRadius = 5.0
        englishBTN.layer.cornerRadius = 5.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func arabicAction(_ sender: UIButton) {
        
        UserDefaults.standard.set("ar", forKey: Constants.KEY_LAUNGUAGE)
        UserDefaults.standard.synchronize()
        Localize.setCurrentLanguage("ar")
        Constants.APP_DELEGATE.setupUTL(semanticContentAttribute: .forceRightToLeft, goToHome: false)
        Constants.APP_DELEGATE.handleUserInterfaceForLanguage()
        performSegue(withIdentifier: "goToLoginSelection", sender: self)
    }
    
    @IBAction func englishAction(_ sender: UIButton) {
        UserDefaults.standard.set("en", forKey: Constants.KEY_LAUNGUAGE)
        UserDefaults.standard.synchronize()
        Localize.setCurrentLanguage("en")
        Constants.APP_DELEGATE.setupUTL(semanticContentAttribute: .forceLeftToRight, goToHome: false)
        Constants.APP_DELEGATE.handleUserInterfaceForLanguage()
        performSegue(withIdentifier: "goToLoginSelection", sender: self)
        
    }
}
