//
//  resetPasswordViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/7/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController , UITextFieldDelegate {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var sendVerifyButton: UIButton!
    @IBOutlet weak var lblEmailDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sendVerifyButton.layer.cornerRadius = 9.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapGesture)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        emailTextField.placeholder = "Email".localized()
                
        //        self.title = "Reset your password".localized()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = Constants.appColor
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]

        emailTextField.placeholder = "Enter your email".localized()
        sendVerifyButton.setTitle("Send".localized(), for: .normal)
        lblEmailDescription.text = "Enter your email address below and we'll send you an email with instruction on how to change your password".localized()
        self.title = "Forgot password".localized()
        //        if (Localize.currentLanguage() == "ar"){
        //
        //            emailTextField.textAlignment = .right
        //        }
        //        else {
        //            emailTextField.textAlignment = .left
        //        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func dismissKeyboard(gesture: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func sendVerifyButton(_ sender: UIButton) {
        
        if ((emailTextField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || !(emailTextField.text?.isValidEmail())!)
        {
            showAlert(message: "Please enter valid Email".localized(), status: "Error".localized())
        }
        else {
            resetPassword()
           /* Loading.shared.show()
            let data : [String: String] = ["email":emailTextField.text!]
            Services.forgetPassword(data: data) { (response, error) in

                DispatchQueue.main.async {
                    Loading.shared.hide()
                }

                if let err = error{
                    self.showAlert(message: err, status: "Error")
                    return
                }

                else if (response.status == "success"){
                    self.showAlert(message: "email sent successfully".localized(), status: "goToLogin")
                }
                else {
                    self.showAlert(message: response.message, status: "Error")
                }
            }*/
        }
    }
    
    func resetPassword(){
        if ((emailTextField.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || !(emailTextField.text?.isValidEmail())!) {
            showAlert(message: "Please enter valid Email".localized(), status: "Error".localized())
        }
        else {
            
            self.showLoader()
            
            let parameters = ["email":emailTextField.text!]
            
            Network.shared.makeHttpRequest(model: ForgotPasswordResponse(), method: .post, APIName: "user/forgotPassword", parameters: parameters) { (result) in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(let response):
                        
                        print("response \(response)")
                        if #available(iOS 13.0, *) {
                            Constants.APP_DELEGATE.window?.overrideUserInterfaceStyle = .light
                        }
                        self.showAlert(message: response.message, status: "back")
                        
                    case .failure(let error):
                        self.showAlert(message: error.errorDescription ?? "", status: "")
                    }
                }
            }
        }
    }
}
