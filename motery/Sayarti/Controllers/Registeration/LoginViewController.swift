//
//  LoginViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/6/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Localize_Swift
import FirebaseAuth

class LoginViewController: UIViewController {
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var loginBTN: UIButton!
    @IBOutlet weak var registerBTN: UIButton!
    @IBOutlet weak var mainViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var forgetPasswordBTN: UIButton!
    

    var bIsForGuestUser = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mainViewHeight.constant = super.view.frame.height
        loginBTN.layer.cornerRadius = 5.0
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.mainView.addGestureRecognizer(tapGesture)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        if Constants.bIsUserLoggedIn(){
            self.navigationController?.popViewController(animated: true)
        }
        
        //        self.title = "Login".localized()
        emailTF.placeholder = "Email".localized()
        passwordTF.placeholder = "Password".localized()
        loginBTN.setTitle("Login".localized(), for: .normal)
        forgetPasswordBTN.setTitle("Forgot your password?".localized(), for: .normal)
        registerBTN.setTitle("Do not have an account? Register".localized(), for: .normal)
        UserDefaults.standard.set(true, forKey: "hideDismissButton")
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        if (Localize.currentLanguage() == "ar"){

            emailTF.textAlignment = .right
            passwordTF.textAlignment = .right
        }
        else {
            emailTF.textAlignment = .left
            passwordTF.textAlignment = .left
        }

        if Localize.currentLanguage() == "ar"{
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
        }else{
            navigationController?.view.semanticContentAttribute = .forceLeftToRight
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func dismissKeyboard(gesture: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    @IBAction func onBtnClose(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        Constants.APP_DELEGATE.setTabbarAsRoot()
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        
//        PhoneAuthProvider.provider().verifyPhoneNumber("+919898795879", uiDelegate: nil) { (verificationID, error) in
//
//            if let error = error {
//                print(error.localizedDescription)
//                self.showAlert(message: "Error".localized(), status: error.localizedDescription)
//                return
//            }
//
//            print("PHONE ID :", verificationID ?? "")
//
//        }
        
        if ((emailTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || !(emailTF.text?.isValidEmail())!)
        {
            showAlert(message: "Please enter valid Email".localized(), status: "Error".localized())
        }

        else if (!(emailTF.text?.isValidEmail() ?? false)){
            showAlert(message: "Please enter valid Email".localized(), status: "Error".localized())
        }

        else if ((passwordTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!)
        {
            showAlert(message: "Please enter valid password it must be more than 6 letters".localized(), status: "Error".localized())
        }

        else if ((passwordTF.text?.count)! < 6){

            showAlert(message: "Please enter valid password it must be more than 6 letters".localized(), status: "Error".localized())
        }

        else {

            self.showLoader()
            let strPlayerId = Constants.APP_DELEGATE.strPlayerId
            let parameters = ["email":emailTF.text!,
                              "password":passwordTF.text!,
                              "device_type" : 1,
                              "player_id" : strPlayerId] as [String : Any]

            Network.shared.makeHttpRequest(model: LoginResponse(), method: .post, APIName: "user/login", parameters: parameters) { (result) in
                DispatchQueue.main.async {
                    self.hideLoader()
                    switch result {
                    case .success(let response):
                        Constants.setValueInUserDefault(value: response.access_token, key: Constants.KEY_USER_TOKEN)
                        Constants.setValueInUserDefault(value: true, key: Constants.KEY_USER_LOGIN)
                        Constants.setValueInUserDefault(value: response.user.email , key: Constants.KEY_USER_EMAIL)
                        Constants.setValueInUserDefault(value: response.user.mobile , key: Constants.KEY_USER_MOBILE)
                        Constants.setValueInUserDefault(value: response.user.fullname ?? "", key: Constants.KEY_USER_NAME)
                        Constants.setValueInUserDefault(value: response.user.profile_image ?? "", key: Constants.KEY_USER_IMAGE)
                        Constants.setValueInUserDefault(value: response.user.id , key: Constants.KEY_USER_ID)
                        Constants.setValueInUserDefault(value: strPlayerId , key: Constants.KEY_PLAYER_ID)
                       
                        if #available(iOS 13.0, *) {
                         Constants.APP_DELEGATE.window?.overrideUserInterfaceStyle = .light
                        }
                        
                        Constants.APP_DELEGATE.setTabbarAsRoot()
                        
//                        if self.bIsForGuestUser{
//                         for favVC in self.navigationController!.viewControllers{
//                             if let fav = favVC as? TabBarController{
//                                 self.navigationItem.setHidesBackButton(true, animated: true)
//                                 fav.navigationItem.setHidesBackButton(true, animated: true)
//                                 fav.navigationController?.setNavigationBarHidden(true, animated: true)
//                                 self.navigationController?.popToViewController(fav, animated: true)
//                                 return
//                             }
//                         }
//                           return
//                        }
//
//                        let tabVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
//                        self.navigationController?.pushViewController(tabVC, animated: true)
                        //self.performSegue(withIdentifier: "goToHome", sender: self)
                    case .failure(let error):
                        self.showAlert(message: error.errorDescription ?? "", status: "")
                    }
                }
            }
        }//
    }
    
    @IBAction func dismissAction(_ sender: Any) {
       
//        if (UserDefaults.standard.bool(forKey: "firstTime")) {
//            self.dismiss(animated: true, completion: nil)
//        }else {
//            performSegue(withIdentifier: "goToHome", sender: self)
//        }
        
        Constants.APP_DELEGATE.setTabbarAsRoot()
    }
    
}


// MARK:- UITextFieldDelegate Methods
extension LoginViewController: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField == emailTF){
            scrollview.setContentOffset(CGPoint.init(x: 0, y: 125), animated: true)
        }else {
            scrollview.setContentOffset(CGPoint.init(x: 0, y: 130), animated: true)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollview.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
