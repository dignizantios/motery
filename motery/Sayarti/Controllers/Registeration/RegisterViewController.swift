//
//  RegisterViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/6/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Localize_Swift
import Firebase

class RegisterViewController: UIViewController , UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    @IBOutlet weak var scrollview: UIScrollView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var registerBTN: UIButton!
    @IBOutlet weak var dismissButton: UIButton!
    @IBOutlet weak var confirmPasswordTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var lblCreateAccnt: UILabel!
    
    @IBOutlet weak var txtCCode: UITextField!
    var arrCountries = [Countries]()
    var countryPickerView = UIPickerView()
    var bIsForGuestUser = true

    var dictParams : [String:Any] = [:]
    
//MARK:- ============== VIEW DELEGATES ==============

    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerBTN.layer.cornerRadius = 5.0
        
        self.lblCreateAccnt.text = "Create new account".localized()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.mainView.addGestureRecognizer(tapGesture)
        
        self.countryPickerView.delegate = self
        self.txtCCode.inputView = self.countryPickerView
        self.fetchCountryList()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Constants.bIsUserLoggedIn(){
            self.navigationController?.popViewController(animated: true)
        }

        self.title = "Register".localized()
        nameTF.placeholder = "Name".localized()
        emailTF.placeholder = "Email".localized()
        phoneNumberTF.placeholder = "Phone number".localized()
        passwordTF.placeholder = "Password".localized()
        confirmPasswordTF.placeholder = "Confirm Password".localized()
        registerBTN.setTitle("Register".localized(), for: .normal)

        if (Localize.currentLanguage() == "ar"){
            nameTF.textAlignment = .right
            emailTF.textAlignment = .right
            phoneNumberTF.textAlignment = .right
            passwordTF.textAlignment = .right
            confirmPasswordTF.textAlignment = .right
        }
        else {
            nameTF.textAlignment = .left
            emailTF.textAlignment = .left
            phoneNumberTF.textAlignment = .left
            passwordTF.textAlignment = .left
            confirmPasswordTF.textAlignment = .left
        }
        
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.prefersLargeTitles = false
       
        if (UserDefaults.standard.bool(forKey: "hideDismissButton")){
            dismissButton.isHidden = true
        }
        
        
    }
    
    //MARK:- ============== FETCH COUNTRY LIST ==============

    private func fetchCountryList(){
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_COUNTRIES, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self](responseData) in
            
            if let jsonData = responseData{
                do{
                    self?.arrCountries = try JSONDecoder().decode([Countries].self, from: jsonData)
                    self?.handleCountryPicker()
                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }

    private func handleCountryPicker(){
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = .black
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.onBtnDone))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.onBtnCancel))

        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        self.txtCCode.inputAccessoryView = toolBar

        
        if self.arrCountries.count > 0 {
            self.txtCCode.text = "+ \(self.arrCountries[0].country_code ?? 0)"
        }
        
    }
    
    @objc func onBtnDone(){
        self.txtCCode.text = "+ \(self.arrCountries[self.countryPickerView.selectedRow(inComponent: 0) ].country_code ?? 0)"
        self.view.endEditing(true)
    }
    
    @objc func onBtnCancel(){
        self.view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.arrCountries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "+ \(self.arrCountries[row].country_code ?? 0)"
    }
    
    //MARK:- ============== TEXTFIELD DELEGATE ==============
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func dismissKeyboard(gesture: UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        //        if (textField == nameTF){
        //
        //            scrollview.setContentOffset(CGPoint.init(x: 0, y: 140), animated: true)
        //        }
        //        else if ( textField == emailTF ) {
        //
        //            scrollview.setContentOffset(CGPoint.init(x: 0, y: 170), animated: true)
        //        }
        //
        //        else if (textField == phoneNumberTF){
        //
        //            scrollview.setContentOffset(CGPoint.init(x: 0, y: 195), animated: true)
        //        }
        //
        //        else if (textField == passwordTF){
        //
        //            scrollview.setContentOffset(CGPoint.init(x: 0, y: 220), animated: true)
        //        }
        //
        //        else {
        //
        //            scrollview.setContentOffset(CGPoint.init(x: 0, y: 245), animated: true)
        //        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        //        let bottomOffset = CGPoint(x: 0, y: scrollview.contentSize.height - scrollview.bounds.size.height)
        //        scrollview.setContentOffset(bottomOffset, animated: true)
    }
    
    @IBAction func registerAction(_ sender: UIButton) {
        
        if ((nameTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!)
        {
            showAlert(message: "Please enter your name".localized(), status: "Error".localized())
        }

        else if ((emailTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! || !(emailTF.text?.isValidEmail())!)
        {
            showAlert(message: "Please enter valid Email".localized(), status: "Error".localized())
        }

        else if ((phoneNumberTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)! )
        {
            showAlert(message: "Mobile number must be required and at least 8 digits".localized(), status: "Error".localized())
        }
        else if (phoneNumberTF.text!.count < 8) {
            showAlert(message: "Mobile number must be required and at least 8 digits".localized(), status: "Error".localized())
        }
        else if ((passwordTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!)
        {
            showAlert(message: "Please enter valid password it must be more than 6 letters".localized(), status: "Error".localized())
        }

        else if ((passwordTF.text?.count)! < 6){

            showAlert(message: "Please enter valid password it must be more than 6 letters".localized(), status: "Error".localized())
        }

        else if ((confirmPasswordTF.text?.trimmingCharacters(in: .whitespaces).isEmpty)!)
        {

            showAlert(message: "Please enter confirm password".localized(), status: "Error".localized())
        }

        else if (passwordTF.text != confirmPasswordTF.text)
        {

            showAlert(message: "Password and confirmed password aren’t the same".localized(), status: "Error".localized())
        }

        else
        {
            
            let strPlayerId = Constants.APP_DELEGATE.strPlayerId
            let countryObj = self.arrCountries[self.countryPickerView.selectedRow(inComponent: 0) ]
            
            let parameters = ["fullname": nameTF.text!,"email":emailTF.text!,"mobile": phoneNumberTF.text!, "password":passwordTF.text! ,"confirm_password": confirmPasswordTF.text!, "device_type" : 1,"player_id" : strPlayerId, "country_id" :countryObj.id ?? 0 ] as [String : Any]
            
            self.dictParams = parameters
            handleRegisterUser()
            
            //TODO: - Yash changes
            //self.performSegue(withIdentifier: "showOTP", sender: nil)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let otpVC = segue.destination as? VerifyPhoneViewController{
            let strPlayerId = Constants.APP_DELEGATE.strPlayerId
            let countryObj = self.arrCountries[self.countryPickerView.selectedRow(inComponent: 0) ]
            let parameters = ["fullname": nameTF.text!,"email":emailTF.text!,"mobile": phoneNumberTF.text!, "password":passwordTF.text! ,"confirm_password": confirmPasswordTF.text!, "device_type" : 1,"player_id" : strPlayerId, "country_id" :countryObj.id ?? 0 ] as [String : Any]
            
            
            
            
            let strPhoneNumber = "+\(countryObj.country_code ?? 0)\(phoneNumberTF.text!)"
            
            //idToken/
            
            otpVC.dictParameters = parameters
            otpVC.countryObj = countryObj
            otpVC.strMobileNumber = strPhoneNumber
            otpVC.bIsForGuestUser = self.bIsForGuestUser
        }
    }
   
    
    
    @IBAction func dismissAction(_ sender: UIButton) {
        
        performSegue(withIdentifier: "goToHome", sender: self)
    }
    
    
    func handleRegisterUser(){
            let strPlayerId = Constants.APP_DELEGATE.strPlayerId

            Network.shared.makeHttpRequest(model: LoginResponse(), method: .post, APIName: "user/register", parameters: self.dictParams) { (result) in
                   DispatchQueue.main.async {
                       self.hideLoader()
                       switch result {
                       case .success(let response):
                           Constants.setValueInUserDefault(value: response.access_token, key: Constants.KEY_USER_TOKEN)
                           Constants.setValueInUserDefault(value: true, key: Constants.KEY_USER_LOGIN)
                           Constants.setValueInUserDefault(value: response.user.email , key: Constants.KEY_USER_EMAIL)
                           Constants.setValueInUserDefault(value: response.user.mobile , key: Constants.KEY_USER_MOBILE)
                           Constants.setValueInUserDefault(value: response.user.fullname ?? "", key: Constants.KEY_USER_NAME)
                           Constants.setValueInUserDefault(value: response.user.profile_image ?? "", key: Constants.KEY_USER_IMAGE)
                           Constants.setValueInUserDefault(value: response.user.id , key: Constants.KEY_USER_ID)
                           Constants.setValueInUserDefault(value: strPlayerId , key: Constants.KEY_PLAYER_ID)
                           
                           
                           Constants.APP_DELEGATE.setTabbarAsRoot()
    //                       if self.bIsForGuestUser{
    //                        for favVC in self.navigationController!.viewControllers{
    //                            if let fav = favVC as? TabBarController{
    //                                self.navigationItem.setHidesBackButton(true, animated: true)
    //                                fav.navigationItem.setHidesBackButton(true, animated: true)
    //                                fav.navigationController?.setNavigationBarHidden(true, animated: true)
    //                                fav.navigationController?.navigationBar.setNeedsLayout()
    //                                self.navigationController?.popToViewController(fav, animated: true)
    //                                return
    //                            }
    //                        }
    //                          return
    //                       }
    //
    //                       let tabVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
    //                       self.navigationController?.navigationBar.isHidden = true
    //
    //                       self.navigationController?.pushViewController(tabVC, animated: true)
                           //self.performSegue(withIdentifier: "goToHome", sender: self)
                       case .failure(let error):
                           self.showAlert(message: error.errorDescription ?? "", status: "")
                       }
                   }
               }

           }
}
