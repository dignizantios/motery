//
//  verifyPhoneViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/7/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Localize_Swift
import Firebase
import KWVerificationCodeView


class VerifyPhoneViewController: UIViewController {
    
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet weak var lblEnterOTP: UILabel!
    
    @IBOutlet weak var lblDidNot: UILabel!
    @IBOutlet weak var verificationCodeView: KWVerificationCodeView!
    var dictParameters = [String: Any]()
    var strVerificationID = ""
    var countryObj = Countries()
    var strMobileNumber = ""
    var bIsForGuestUser = false

    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        verifyButton.layer.cornerRadius = 5.0
        self.handleOTP()
        self.title = "Verify your number".localized()
        self.lblEnterOTP.text = "Enter your OTP code here".localized()
        self.lblDidNot.text = "Did not receive OTP?".localized()
        self.verifyButton.setTitle("Verify Now".localized(), for: .normal)
        self.resendButton.setTitle("Resend now".localized(), for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.tintColor = Constants.appColor
    self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black]
       //self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    
    @IBAction func resendAction(_ sender: UIButton) {
        self.handleOTP()
    }
    
    @IBAction func verifyAction(_ sender: UIButton) {
           
        if self.verificationCodeView.hasValidCode() {
            self.handleFirebaseSignUp()
        }else{
            self.showAlert(message: "OTP".localized(), status: "Please enter valid OTP")
        }
    }
    
    func handleOTP(){
        self.view.endEditing(true)
        self.showLoader()

        PhoneAuthProvider.provider().verifyPhoneNumber(self.strMobileNumber, uiDelegate: nil) { [weak self](verificationID, error) in
       
               if let error = error {
                   print(error.localizedDescription)
                   self?.hideLoader()
                   self?.showAlert(message:error.localizedDescription, status:  "Error".localized())
                   return
               }
           
            self?.strVerificationID = verificationID ?? ""
            print("PHONE ID :", verificationID ?? "")
            self?.hideLoader()
        }
     }

    
    func handleFirebaseSignUp(){
        self.view.endEditing(true)
        self.showLoader()
        
        let strOTP = self.verificationCodeView.getVerificationCode()
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: self.strVerificationID,
        verificationCode: strOTP)
        
        Auth.auth().signIn(with: credential) { [weak self] (authResult, error) in
          if let error = error {
            print(error.localizedDescription)
             self?.hideLoader()
            self?.showAlert(message: error.localizedDescription, status: "Error".localized())
            return
          }
         // self?.hideLoader()
            print("Success Register", authResult?.user.uid ?? "")
            authResult?.user.getIDTokenForcingRefresh(true, completion: { (idToken, error) in //getIDToken(completion: { (idToken, error) in=
            if let error = error {
               print(error.localizedDescription)
               self?.hideLoader()
               self?.showAlert(message: error.localizedDescription, status: "Error".localized())
               return
            }
                
                self?.dictParameters["idToken"] = idToken ?? ""
                self?.handleRegisterUser()
            })
        }
    }
    
    
    func handleRegisterUser(){
        let strPlayerId = Constants.APP_DELEGATE.strPlayerId

        Network.shared.makeHttpRequest(model: LoginResponse(), method: .post, APIName: "user/register", parameters: self.dictParameters) { (result) in
               DispatchQueue.main.async {
                   self.hideLoader()
                   switch result {
                   case .success(let response):
                       Constants.setValueInUserDefault(value: response.access_token, key: Constants.KEY_USER_TOKEN)
                       Constants.setValueInUserDefault(value: true, key: Constants.KEY_USER_LOGIN)
                       Constants.setValueInUserDefault(value: response.user.email , key: Constants.KEY_USER_EMAIL)
                       Constants.setValueInUserDefault(value: response.user.mobile , key: Constants.KEY_USER_MOBILE)
                       Constants.setValueInUserDefault(value: response.user.fullname ?? "", key: Constants.KEY_USER_NAME)
                       Constants.setValueInUserDefault(value: response.user.profile_image ?? "", key: Constants.KEY_USER_IMAGE)
                       Constants.setValueInUserDefault(value: response.user.id , key: Constants.KEY_USER_ID)
                       Constants.setValueInUserDefault(value: strPlayerId , key: Constants.KEY_PLAYER_ID)
                       
                       
                       Constants.APP_DELEGATE.setTabbarAsRoot()
//                       if self.bIsForGuestUser{
//                        for favVC in self.navigationController!.viewControllers{
//                            if let fav = favVC as? TabBarController{
//                                self.navigationItem.setHidesBackButton(true, animated: true)
//                                fav.navigationItem.setHidesBackButton(true, animated: true)
//                                fav.navigationController?.setNavigationBarHidden(true, animated: true)
//                                fav.navigationController?.navigationBar.setNeedsLayout()
//                                self.navigationController?.popToViewController(fav, animated: true)
//                                return
//                            }
//                        }
//                          return
//                       }
//
//                       let tabVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
//                       self.navigationController?.navigationBar.isHidden = true
//
//                       self.navigationController?.pushViewController(tabVC, animated: true)
                       //self.performSegue(withIdentifier: "goToHome", sender: self)
                   case .failure(let error):
                       self.showAlert(message: error.errorDescription ?? "", status: "")
                   }
               }
           }

       }
    
}

extension VerifyPhoneViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }
}
