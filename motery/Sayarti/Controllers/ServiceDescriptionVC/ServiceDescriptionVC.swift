//
//  ServiceDescriptionVC.swift
//  Sayarti
//
//  Created by Vishal on 07/03/20.
//  Copyright © 2020 Mina Malak. All rights reserved.
//

import UIKit

class ServiceDescriptionVC: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrize: UILabel!
    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var imgCarPic: UIImageView!
    @IBOutlet weak var imgWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblCarDescription: UILabel!
    
    var serviceData = ResellerService()
    
    
    //MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpUi()
        self.setUpData()
//        vwMain.clipsToBounds = true
        
        vwMain.layer.cornerRadius = 35
        vwMain.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner respectively
    }
    
    func setUpUi() {
        
        vwMain.layer.shadowColor = UIColor.gray.cgColor
        vwMain.layer.shadowOpacity = 0.5
        vwMain.layer.shadowOffset = .zero
        vwMain.layer.shadowRadius = 6
    }
    
    func setUpData() {
        
        if self.serviceData.image == nil {
            imgWidthConstraint.constant = 0
            imgHeightConstraint.constant = 0
        }
        else {
            imgWidthConstraint.constant = 60
            imgHeightConstraint.constant = 60
        }
        
        self.lblTitle.text = serviceData.name
        if let price = serviceData.price {
            if price <= 0 {
                lblPrize.isHidden = true
            }
            else {
                lblPrize.isHidden = false
                lblPrize.text = String(format: "%.2f KD", price)
            }
        }
        self.imgCarPic.setImageForCar(withUrl: serviceData.image)
        self.lblCarDescription.text = self.serviceData.description ?? ""
    }
    
    
    @IBAction func btnDismissAction(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
}
