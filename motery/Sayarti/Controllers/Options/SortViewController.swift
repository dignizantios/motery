//
//  sortViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/2/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Localize_Swift



enum SortType: String {
    case ascending = "Alphabetically (A-Z)"
    case descending = "Alphabetically (Z-A)"
}

protocol SortViewDelegate{
    func didSelect(_ sortType: SortType)
}

class SortViewController: UIViewController {
    
    let sortTypes: [SortType] = [.ascending, .descending]
    var selectedSortType: SortType = .ascending
    var delegate: SortViewDelegate?
    
    @IBOutlet weak var sortTableView: UITableView!
    @IBOutlet weak var applyButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sortTableView.tableFooterView = UIView()
        applyButton.layer.cornerRadius = 9.0
        self.applyButton.setTitle("Apply".localized(), for: .normal)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.title = "Sort".localized()
    }
    
    @IBAction func applyAction(_ sender: UIButton) {
         delegate?.didSelect(selectedSortType)
        self.navigationController?.popViewController(animated: true)
    }
}

extension SortViewController: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return sortTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "sortCell", for: indexPath) as! OptionTableViewCell
        cell.mainView.layer.cornerRadius = 10.0
        cell.optionLabel.text = (sortTypes[indexPath.row].rawValue).localized()
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
        cell.selectedBackgroundView = UIView()
        cell.selectedBackgroundView?.backgroundColor = .clear
        cell.accessoryType = .checkmark
        selectedSortType = sortTypes[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
        cell.accessoryType = .none
    }
}
