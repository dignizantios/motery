//
//  filterViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/2/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

protocol FilterViewDelegate {
    func didSelect(continent: Continent,lastSelectedContinentID:Int)
}


class FilterViewController: UIViewController {
    
    
    var continents = [Continent]()
    var selectedContinent = Continent()
    var delegate: FilterViewDelegate?
    
    
    @IBOutlet weak var filterTableView: UITableView!
    @IBOutlet weak var applyButton: UIButton!
    
    var lastSelectedContinentID = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("filter selection Id: \(lastSelectedContinentID)")
        
        getContinents()
        applyButton.layer.cornerRadius = 9.0
        filterTableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showLoader()
        self.title = "Filter".localized()
    }
    
    @IBAction func applyAction(_ sender: UIButton) {
        delegate?.didSelect(continent: selectedContinent,lastSelectedContinentID:self.lastSelectedContinentID)
        self.navigationController?.popViewController(animated: true)
    }
    
    fileprivate func getContinents(){
        Network.shared.makeHttpRequest(model: [Continent](), method: .get, APIName: "user/getContinents", parameters: nil) { (result) in
            DispatchQueue.main.async {
                self.hideLoader()
                switch result {
                case .success(let continents):
                    
                    let addContinent = Continent()
                    addContinent.name = "All"
                    addContinent.selectedIndex = 0
                    addContinent.id = -1
                    
                    self.continents.append(addContinent)

                    for i in 0..<continents.count{
                        self.continents.append(continents[i])
                    }
                                        
                    for i in 0..<self.continents.count{
                        
                        if self.continents[i].id == self.lastSelectedContinentID{
                            self.continents[i].selectedIndex = 1
                        }else{
                            self.continents[i].selectedIndex = 0
                        }
                    }
                    
                    self.filterTableView.reloadData()
                case .failure(let error):
                    self.showAlert(message: error.errorDescription ?? "", status: "")
                }
            }
        }
    }
}

extension FilterViewController: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return continents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "filterCell", for: indexPath) as! OptionTableViewCell
        cell.mainView.layer.cornerRadius = 10.0
        cell.optionLabel.text = continents[indexPath.row].name
        
        if continents[indexPath.row].selectedIndex == 1{
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
//        cell.accessoryType = .checkmark
//        continents[indexPath.row].selectedIndex = 1
//        selectedContinent = continents[indexPath.row]
//        self.lastSelectedContinentID = continents[indexPath.row].id
        
        
        for i in 0..<continents.count{
            let dict = continents[i]
            continents[i].selectedIndex = 0
            continents[i] = dict
        }
        
        continents[indexPath.row].selectedIndex = 1
        
        selectedContinent = continents[indexPath.row]
        self.lastSelectedContinentID = continents[indexPath.row].id
        
        tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
//        let cell = tableView.cellForRow(at: indexPath) as! OptionTableViewCell
//        cell.accessoryType = .none
//        continents[indexPath.row].selectedIndex = 0
//        cell.backgroundColor = .clear
        
    }
}
