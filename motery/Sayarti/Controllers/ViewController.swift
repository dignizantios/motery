//
//  ViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 2/28/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

//extension UIPanGestureRecognizer {
//
//    public struct PanGestureDirection: OptionSet {
//        public let rawValue: UInt8
//
//        public init(rawValue: UInt8) {
//            self.rawValue = rawValue
//        }
//
//        static let Up = PanGestureDirection(rawValue: 1 << 0)
//        static let Down = PanGestureDirection(rawValue: 1 << 1)
//        static let Left = PanGestureDirection(rawValue: 1 << 2)
//        static let Right = PanGestureDirection(rawValue: 1 << 3)
//    }
//
//    private func getDirectionBy(velocity: CGFloat, greater: PanGestureDirection, lower: PanGestureDirection) -> PanGestureDirection {
//        if velocity == 0 {
//            return []
//        }
//        return velocity > 0 ? greater : lower
//    }
//
//    public func direction(in view: UIView) -> PanGestureDirection {
//        let velocity = self.velocity(in: view)
//        let yDirection = getDirectionBy(velocity: velocity.y, greater: PanGestureDirection.Down, lower: PanGestureDirection.Up)
//        let xDirection = getDirectionBy(velocity: velocity.x, greater: PanGestureDirection.Right, lower: PanGestureDirection.Left)
//        return xDirection.union(yDirection)
//    }
//}
