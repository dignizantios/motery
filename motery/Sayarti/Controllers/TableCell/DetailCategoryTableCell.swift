//
//  DetailCategoryTableCell.swift
//  Sayarti
//
//  Created by Vishal on 17/04/20.
//  Copyright © 2020 Mina Malak. All rights reserved.
//

import UIKit

class DetailCategoryTableCell: UITableViewCell {

    @IBOutlet weak var vwMain: UIView!
    @IBOutlet weak var lblHeader: UILabel!
    @IBOutlet weak var btnSelection: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var vwDescription: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        vwMain.layer.masksToBounds = true
        vwMain.layer.cornerRadius = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
