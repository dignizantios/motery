//
//  showOrdersViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/17/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class ShowOrdersViewController: UIViewController {
    
    var userOrder = UserOrder()
    var arrCategories = [String]()

    @IBOutlet weak var tblMyOrder: UITableView!
    @IBOutlet weak var lblNoOrder: UILabel!
    
    
//MARK:- ############### VIEW DELEGATES ###############
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchOrders()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.lblNoOrder.text = "No orders".localized()
        self.title = "Orders".localized()
        // Do any additional setup after loading the view.
    }
    
    private func fetchOrders(){
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_ORDER_LIST, bIsPostRequest: false, bIsShowAlert: false, parameter: [:]) { [weak self] (responseData) in
            
            if let jsonData = responseData{
                do{
                    self?.userOrder = try JSONDecoder().decode(UserOrder.self, from: jsonData)
                    if self?.userOrder.carReseller?.count ?? 0 > 0{
                        self?.arrCategories.append("Car Reseller".localized())
                    }
                    if self?.userOrder.carWash?.count ?? 0 > 0{
                        self?.arrCategories.append("Protection & Car Wash".localized())
                    }
                    if self?.userOrder.carInsurance?.count ?? 0 > 0{
                        self?.arrCategories.append("Car Insurance".localized())
                    }
                    if self?.userOrder.carRent?.count ?? 0 > 0{
                        self?.arrCategories.append("Car Rent".localized())
                    }
                    if self?.userOrder.usedCar?.count ?? 0 > 0{
                        self?.arrCategories.append("Used Car".localized())
                    }
                    
                    if self?.arrCategories.count ?? 0 > 0{
                        self?.lblNoOrder.isHidden = true
                    }else{
                        self?.lblNoOrder.isHidden = false
                    }
                    
                    self?.tblMyOrder.reloadData()

                }catch{
                    print("Error : \(error.localizedDescription)")
                }
            }
            self?.hideLoader()
        }
    }
    
    
    
    
}

extension ShowOrdersViewController: UITableViewDataSource , UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrCategories.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let strSectionName = self.arrCategories[section]
        
        if strSectionName == "Car Reseller".localized(){
            return self.userOrder.carReseller?.count ?? 0
        }else if strSectionName == "Protection & Car Wash".localized(){
            return self.userOrder.carWash?.count ?? 0
        }else if strSectionName == "Car Insurance".localized(){
            return self.userOrder.carInsurance?.count ?? 0
        }else if strSectionName == "Car Rent".localized(){
            return self.userOrder.carRent?.count ?? 0
        }else if strSectionName == "Used Car".localized(){
            return self.userOrder.usedCar?.count ?? 0
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let viewHeader = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width , height: 50))
        let lblTitle = UILabel.init(frame: CGRect.init(x: 20, y: 5, width: self.view.frame.size.width - 40 , height: 40))
        lblTitle.textColor = .black
        lblTitle.text = arrCategories[section].localized()
        lblTitle.font = UIFont.boldSystemFont(ofSize: 23.0)
        lblTitle.backgroundColor = .clear
        viewHeader.backgroundColor = UIColor.init(red: 247.0/255.0, green: 247.0/255.0, blue: 250.0/255.0, alpha: 1.0)
        viewHeader.addSubview(lblTitle)
        return viewHeader
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell", for: indexPath) as! OrderTableViewCell
        if (indexPath.row > 1) {
            cell.servicesLabel.text = "Car Reseller".localized()
        }
        cell.mainView.layer.cornerRadius = 10.0
        
        let strSectionName = self.arrCategories[indexPath.section]
        
        if strSectionName == "Car Reseller".localized(){
            let carReseller = self.userOrder.carReseller?[indexPath.row]
            cell.orderIdLabel.text = "Order".localized() + "#\(carReseller?.id ?? 0)"
            //TODO: - Yash changes
            cell.servicesLabel.text = carReseller?.service_name ?? ""
            cell.companyLabel.text = carReseller?.agency?.name ?? ""
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let strDate = carReseller?.date {
                let datevalue = dateFormatter.date(from: strDate)
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let strValue = dateFormatter.string(from: datevalue ?? Date())
                cell.dateLabel.text = strValue
            }
 
        }else if strSectionName == "Protection & Car Wash".localized(){
            let carWash = self.userOrder.carWash?[indexPath.row]
            cell.orderIdLabel.text = "Order".localized() + "#\(carWash?.id ?? 0)"
            //TODO: - Yash changes
            cell.servicesLabel.text = carWash?.service ?? ""
            cell.companyLabel.text = carWash?.car_wash_store?.name ?? ""
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let strDate = carWash?.date {
                let datevalue = dateFormatter.date(from: strDate)
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let strValue = dateFormatter.string(from: datevalue ?? Date())
                cell.dateLabel.text = strValue
            }
            
        }else if strSectionName == "Car Insurance".localized(){
            let carInsurance = self.userOrder.carInsurance?[indexPath.row]
            cell.orderIdLabel.text = "Order".localized() + "#\(carInsurance?.insurance_id ?? 0)"
            //TODO: - Yash changes
            cell.servicesLabel.text = carInsurance?.service_name ?? ""
            cell.companyLabel.text = carInsurance?.company?.name ?? ""
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let strDate = carInsurance?.date {
                let datevalue = dateFormatter.date(from: strDate)
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let strValue = dateFormatter.string(from: datevalue ?? Date())
                cell.dateLabel.text = strValue
            }
            
        }else if strSectionName == "Car Rent".localized(){
            let carRent = self.userOrder.carRent?[indexPath.row]
            cell.orderIdLabel.text = "Order".localized() + "#\(carRent?.id ?? 0)"
            cell.servicesLabel.text = carRent?.car?.name ?? ""
            cell.companyLabel.text = carRent?.company?.name ?? ""
          //  cell.dateLabel.text = carRent?.date
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let strDate = carRent?.date {
                let datevalue = dateFormatter.date(from: strDate)
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let strValue = dateFormatter.string(from: datevalue ?? Date())
                cell.dateLabel.text = strValue
            }
            
        }else if strSectionName == "Used Car".localized(){
            let carUsed = self.userOrder.usedCar?[indexPath.row]
            cell.orderIdLabel.text = "Order".localized() + "#\(carUsed?.id ?? 0)"
            cell.servicesLabel.text = carUsed?.car?.name ?? ""
            cell.companyLabel.text = carUsed?.car?.agency ?? ""
           // cell.dateLabel.text = carUsed?.date
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let strDate = carUsed?.date {
                let datevalue = dateFormatter.date(from: strDate)
                dateFormatter.dateFormat = "dd-MM-yyyy"
                let strValue = dateFormatter.string(from: datevalue ?? Date())
                cell.dateLabel.text = strValue
            }
         }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isCashPaymnetSelected = false
        performSegue(withIdentifier: "userOrderDetails", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "userOrderDetails"{
            guard let indexPath = sender as? IndexPath else { return }
            let strSectionName = self.arrCategories[indexPath.section]
        
            if let detailVC = segue.destination as? OrderDetailsViewController{
                    detailVC.bIsOrderDetails = true
                
                if strSectionName == "Car Reseller".localized(){
                    let carReseller = self.userOrder.carReseller?[indexPath.row]
                    //TODO: - Yash changes
                    
                    detailVC.arrServiceTitle = carReseller?.service?.map({$0.name ?? ""}) ?? []
                    detailVC.arrServicePrice = carReseller?.service?.map({String.init($0.price ?? 0.0)}) ?? []
                    detailVC.totalPrice    = carReseller?.total_amount ?? 0.0
                    detailVC.strAgencyName = carReseller?.agency?.name ?? ""
                    detailVC.strCarName    = carReseller?.car?.name ?? ""
                    detailVC.strImageName  = carReseller?.car?.image ?? ""
                    detailVC.userDetails   = carReseller?.user ?? User()
                    detailVC.paymentType = carReseller?.payment_type ?? "" == "cash" ? true : false

                }else if strSectionName == "Protection & Car Wash".localized(){
                    let carWash = self.userOrder.carWash?[indexPath.row]
                    //TODO: - Yash changes
//                    detailVC.arrServiceTitle.append(carWash?.car_wash_service?.name ?? "")
//                    detailVC.arrServicePrice.append(String.init(format: "%.2f KD", (carWash?.car_wash_service?.price ?? 0.0)))
                    
                    detailVC.arrServiceTitle = carWash?.car_wash_service?.map({$0.name ?? ""}) ?? []
                    detailVC.arrServicePrice = carWash?.car_wash_service?.map({String.init($0.price ?? 0.0)}) ?? []
                    detailVC.totalPrice    = carWash?.total_amount ?? 0.0
                    detailVC.strAgencyName = carWash?.car_wash_store?.name ?? ""
                    detailVC.strCarName    = carWash?.car_type?.type_name ?? ""
                    detailVC.strImageName  = carWash?.car_type?.type_img ?? ""
                    detailVC.userDetails   = carWash?.user ?? User()
                    detailVC.paymentType = carWash?.payment_type ?? "" == "cash" ? true : false
                    detailVC.checkOrderTypeString = "Protection & Car Wash".localized()
                    detailVC.strCoupnCode = carWash?.coupon_code ?? ""
                    detailVC.strDiscount = carWash?.discount ?? ""
                    detailVC.strDiscountAmount = carWash?.discount_amount ?? 0.0
                }else if strSectionName == "Car Insurance".localized(){
                    let carInsurance = self.userOrder.carInsurance?[indexPath.row]
               
                    detailVC.arrServiceTitle.append(carInsurance?.insurance_package?.pack_title ?? "")
                    detailVC.arrServicePrice.append(String.init(format: "%.2f KD", (carInsurance?.insurance_package?.price ?? 0.0)))
                //TODO: - Yash changes
//                    detailVC.arrServiceTitle.append(carInsurance?.insurance_company_additional_service?.serv_name ?? "")
//                    detailVC.arrServicePrice.append(String.init(format: "%.2f KD", (carInsurance?.insurance_company_additional_service?.serv_charge ?? 0.0)))
                    
                    detailVC.arrServiceTitle = carInsurance?.insurance_company_additional_service?.map({$0.serv_name ?? ""}) ?? []
                    detailVC.arrServicePrice = carInsurance?.insurance_company_additional_service?.map({String.init($0.serv_charge ?? 0.0)}) ?? []

                    
                    detailVC.totalPrice    = carInsurance?.total_amount ?? 0.0
                    detailVC.strAgencyName = carInsurance?.company?.name ?? ""
                    detailVC.strCarName    = carInsurance?.car_type?.type_name ?? ""
                    detailVC.strImageName  = carInsurance?.car_type?.type_img ?? ""
                    detailVC.userDetails   = carInsurance?.user ?? User()
                    detailVC.strCoupnCode = carInsurance?.coupon_code ?? ""
                    detailVC.strDiscount = carInsurance?.discount ?? ""
                    detailVC.strDiscountAmount = carInsurance?.discount_amount ?? 0.0
                    detailVC.paymentType = carInsurance?.payment_type ?? "" == "cash" ? true : false
                    detailVC.checkOrderTypeString = "Car Insurance".localized()
                }else if strSectionName == "Car Rent".localized(){
                    let usedCar = self.userOrder.carRent?[indexPath.row]
                    detailVC.totalPrice    = usedCar?.total_amount ?? 0.0
                    detailVC.strAgencyName = usedCar?.agency?.name ?? ""
                    detailVC.strCarName    = usedCar?.car?.name ?? ""
                    detailVC.strImageName  = usedCar?.car?.car_img ?? ""
                    detailVC.userDetails   = usedCar?.user ?? User()
                    detailVC.paymentType = usedCar?.payment_type ?? "" == "cash" ? true : false
                }else if strSectionName == "Used Car".localized(){
                    let usedCar = self.userOrder.usedCar?[indexPath.row]
                    detailVC.totalPrice    = usedCar?.total_amount ?? 0.0
                    detailVC.strAgencyName = usedCar?.car?.agency ?? ""
                    detailVC.strCarName    = usedCar?.car?.name ?? ""
                    detailVC.strImageName  = usedCar?.car?.car_img ?? ""
                    detailVC.paymentType = usedCar?.payment_type ?? "" == "cash" ? true : false
                    detailVC.userDetails   = usedCar?.user ?? User()
                }
                
            }
        }
    }
    
}
