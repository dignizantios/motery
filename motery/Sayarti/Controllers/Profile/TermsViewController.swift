//
//  termsViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/17/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import WebKit

class TermsViewController: UIViewController {
    
    var termsCondition = TermsNConditions()
    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var txtView: UITextView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtView.isEditable = false
        title = "Terms & conditions".localized()
        self.fetchWebData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    private func fetchWebData(){
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_T_AND_C , bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData) in
            
            if let jsonData = responseData{
                do{
                    self?.termsCondition = try JSONDecoder().decode(TermsNConditions.self, from: jsonData)
                    self?.handleWebView()
                }catch{
                    print("Error: \(error)")
                }
            }
            self?.hideLoader()
        }
    }
    
    
    private func handleWebView(){
       self.txtView.text = self.termsCondition.terms_and_conditions ?? "" //self.webView.loadHTMLString(self.termsCondition.terms_and_conditions ?? "", baseURL: nil)
    }
    
}
