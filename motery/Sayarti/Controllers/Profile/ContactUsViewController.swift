//
//  contactUsViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/14/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsViewController: UIViewController , MFMailComposeViewControllerDelegate{
    
    var emailObj = ContactEmail()
    var callObj  = ContactNumber()
    
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var callLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callView.layer.cornerRadius = 9.0
        emailView.layer.cornerRadius = 9.0
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(call(gesture:)))
        tapGesture.numberOfTapsRequired = 1
        self.callView.addGestureRecognizer(tapGesture)
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(email(gesture:)))
        tapGesture2.numberOfTapsRequired = 1
        self.emailView.addGestureRecognizer(tapGesture2)
        
        // Do any additional setup after loading the view.
        
        self.fetchEmailData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = "Contact us".localized()
        callLabel.text = "Call".localized()
        emailLabel.text = "Email".localized()
    }
    
    
    
    private func fetchEmailData(){
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_EMAIL , bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData) in
            
            if let jsonData = responseData{
                do{
                    self?.emailObj = try JSONDecoder().decode(ContactEmail.self, from: jsonData)
                }catch{
                    print("Error: \(error)")
                }
            }
            self?.fetchCallData()
        }
    }
    
    private func fetchCallData(){
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_CONTACT_NUMBER , bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData) in
            
            if let jsonData = responseData{
                do{
                    self?.callObj = try JSONDecoder().decode(ContactNumber.self, from: jsonData)
                }catch{
                    print("Error: \(error)")
                }
            }
            self?.hideLoader()
        }
    }
    
    

    func configureMailController() -> MFMailComposeViewController {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients([self.emailObj.email ?? ""])
        
        return composeVC
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func call(gesture: UITapGestureRecognizer)
    {
        if let url = URL(string: "tel://\(self.callObj.contact ?? "")") {
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func email(gesture: UITapGestureRecognizer)
    {
        
        let email = self.emailObj.email ?? ""
        if let url = URL(string: "mailto:\(email)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
