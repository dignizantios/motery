//
//  languageViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/14/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Localize_Swift

class LanguageViewController: UIViewController {
    
    let languages = ["English","العربية"]
    let images = ["english","arabic"]
    
    @IBOutlet weak var languageTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        languageTableView.tableFooterView = UIView()
        languageTableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: languageTableView.frame.size.width, height: 1))
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Language".localized()
    }
}

extension LanguageViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return languages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "languageCell", for: indexPath) as! LanguageTableViewCell
        cell.languageImageView.image = UIImage(named: images[indexPath.row])
        cell.languageLabel.text = languages[indexPath.row]
        
        if (UserDefaults.standard.string(forKey: Constants.KEY_LAUNGUAGE) == "ar" && languages[indexPath.row] == "العربية"){
            
            cell.accessoryType = .checkmark
        }
        else if (UserDefaults.standard.string(forKey: Constants.KEY_LAUNGUAGE) == "en" && languages[indexPath.row] == "English"){
            cell.accessoryType = .checkmark
        }
        else{
            cell.accessoryType = .none
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if (indexPath.row == 1) {
            
            Localize.setCurrentLanguage("ar")
            UserDefaults.standard.set("ar", forKey: Constants.KEY_LAUNGUAGE)
            //            Constants.headers["Language"] = "ar"
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            UserDefaults.standard.synchronize()
            languageTableView.reloadData()
            appDelegate.setupUTL(semanticContentAttribute: .forceRightToLeft, goToHome: true)
            
        }
        else {
            Localize.setCurrentLanguage("en")
            UserDefaults.standard.set("en", forKey: Constants.KEY_LAUNGUAGE)
            //            Constants.headers["Language"] = "en"
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
            UserDefaults.standard.synchronize()
            languageTableView.reloadData()
            appDelegate.setupUTL(semanticContentAttribute: .forceLeftToRight, goToHome: true)
        }
        
        
    }
}
