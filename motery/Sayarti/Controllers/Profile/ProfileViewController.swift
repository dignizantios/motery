//
//  ProfileViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 2/28/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Localize_Swift

class ProfileViewController: UIViewController , UIScrollViewDelegate {
    
    
    
    
    static var dismiss: Bool = true
    let blackView = UIView()
    var editProfileView: EditProfileView?
    var imagePicker = UIImagePickerController()
    private let imageView = UIImageView(image: UIImage(named: "Mina"))
//    let titles = ["Details","Orders","Terms & conditions","Language","Contact us","Logout"]
    let titles = ["Details","Orders","Language","Contact us","Sign out","Terms & conditions"]   //Vishal
//    let descriptions = ["Edit your profile info","View your previous orders","Read terms and conditions","Choose your language","Get in touch"]
    let descriptions = ["Edit your profile info","View your previous orders","Choose your language","Get in touch"]     //Vishal
    let segues = ["showOrders","showLanguage","showContactUs","showTerms"]
    let titlesGuest = ["Language","Contact us","Login","Terms & conditions"]
    let descriptionsGuest = ["Choose your language","Get in touch","Login",""]
    let seguesGuest = ["showLanguage","showContactUs","showTerms"]
    
    var userInfo = User()
    
    
//MARK:- VIEW DELEGATES
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Constants.bIsUserLoggedIn(){
            self.fetchProfileDetails()
        }
        //        else{
        //           self.showLoginVC()
        //       }
    }
    
    private func showLoginVC(){
       let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginVC.modalPresentationStyle = .fullScreen
        loginVC.bIsForGuestUser = true
        self.tabBarController?.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        title = "Profile".localized()
         if #available(iOS 13.0, *) {
            Constants.APP_DELEGATE.window?.overrideUserInterfaceStyle = .light
            let app = UINavigationBarAppearance()
            app.backgroundColor = .white
            self.navigationController?.navigationBar.scrollEdgeAppearance = app
         }
        self.navigationController?.navigationBar.setNeedsLayout()
        self.tabBarController?.navigationController?.navigationBar.setNeedsLayout()
        
        if (ProfileViewController.dismiss){
            blackView.removeFromSuperview()
        }else{
            self.tabBarController?.tabBar.isHidden = true
        }
        ProfileViewController.dismiss = true
        
        if Localize.currentLanguage() == "ar"{
            navigationController?.view.semanticContentAttribute = .forceRightToLeft
        }else{
            navigationController?.view.semanticContentAttribute = .forceLeftToRight
        }
                
        if Constants.bIsUserLoggedIn() && userInfo.id == 0 {
            fetchProfileDetails()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        imageView.removeFromSuperview()
    }
    
    @IBAction func dismissViewGesture(gesture: UITapGestureRecognizer){
        editProfileView?.removeFromSuperview()
        blackView.removeFromSuperview()
        self.tabBarController?.tabBar.isHidden = false
    }
    
    private func setupUI() {
        
        // Initial setup for image for Large NavBar state since the the screen always has Large NavBar once it gets opened
        guard let navigationBar = self.navigationController?.navigationBar, Constants.bIsUserLoggedIn() else { return }
        
//        navigationBar.addSubview(imageView)
//        imageView.layer.cornerRadius = Const.ImageSizeForLargeState / 2
//        imageView.clipsToBounds = true
//        imageView.translatesAutoresizingMaskIntoConstraints = false
//        NSLayoutConstraint.activate([
//            imageView.trailingAnchor.constraint(equalTo: navigationBar.trailingAnchor, constant: -Const.ImageRightMargin),
//            imageView.bottomAnchor.constraint(equalTo: navigationBar.bottomAnchor, constant: -Const.ImageBottomMarginForLargeState),
//            imageView.heightAnchor.constraint(equalToConstant: Const.ImageSizeForLargeState),
//            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor)
//            ])
    }
    
    private func fetchProfileDetails(){
        if Constants.bIsUserLoggedIn() == false{
            return
        }
        
        self.showLoader()
        
        APIManager.shared.fetchRequestData(strURL: Constants.GET_USER_PROFILE, bIsPostRequest: false, bIsShowAlert: true, parameter: [:]) { [weak self] (responseData) in
            
            if let jsonData = responseData{
                do{
                    self?.userInfo = try JSONDecoder().decode(User.self, from: jsonData)
                    
                }catch{
                    print(error.localizedDescription)
                }
            }
            self?.hideLoader()
        }
    }
    
    private func moveAndResizeImageForPortrait() {
        guard let height = navigationController?.navigationBar.frame.height else { return }
        
        let coeff: CGFloat = {
            let delta = height - Const.NavBarHeightSmallState
            let heightDifferenceBetweenStates = (Const.NavBarHeightLargeState - Const.NavBarHeightSmallState)
            return delta / heightDifferenceBetweenStates
        }()
        
        let factor = Const.ImageSizeForSmallState / Const.ImageSizeForLargeState
        
        let scale: CGFloat = {
            let sizeAddendumFactor = coeff * (1.0 - factor)
            return min(1.0, sizeAddendumFactor + factor)
        }()
        
        // Value of difference between icons for large and small states
        let sizeDiff = Const.ImageSizeForLargeState * (1.0 - factor) // 8.0
        
        let yTranslation: CGFloat = {
            /// This value = 14. It equals to difference of 12 and 6 (bottom margin for large and small states). Also it adds 8.0 (size difference when the image gets smaller size)
            let maxYTranslation = Const.ImageBottomMarginForLargeState - Const.ImageBottomMarginForSmallState + sizeDiff
            return max(0, min(maxYTranslation, (maxYTranslation - coeff * (Const.ImageBottomMarginForSmallState + sizeDiff))))
        }()
        
        let xTranslation = max(0, sizeDiff - coeff * sizeDiff)
        
        imageView.transform = CGAffineTransform.identity
            .scaledBy(x: scale, y: scale)
            .translatedBy(x: xTranslation, y: yTranslation)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        moveAndResizeImageForPortrait()
        
    }
    
    
//MARK:- ############## PROFILE VALIDATION ##############
    
    private func handleProfileValidation(){
        if editProfileView?.userNameTextField.text == "" {
            self.showDefaultAlert(title: "Full name".localized(), message: "Please enter your full name")
            return
        }
        
        if editProfileView?.userMobileTextField.text == "" {
            self.showDefaultAlert(title: "Phone number".localized(), message: "Please enter your phone number")
            return
        }
        
        if editProfileView?.userMobileTextField.text?.count ?? 0 < 8 {
            self.showDefaultAlert(title: "Phone number".localized(), message: "Mobile number must be required and at least 8 digits".localized())
            return
        }
        
        if editProfileView?.userEmailTextField.text == "" {
            self.showDefaultAlert(title: "Email".localized(), message: "Please enter your email")
            return
        }
        
        if editProfileView?.userEmailTextField.text?.isValidEmail() == false {
            self.showDefaultAlert(title: "Email".localized(), message: "Please enter valid email")
            return
        }
        self.hanldeUserProfileUpdate()
    }
    
    private func hanldeUserProfileUpdate(){
        self.showLoader()
        let imgDataObj = editProfileView?.userProfilePictureImageView.image!.resized(withPercentage: 0.3)?
            .jpegData(compressionQuality:0.0)?.base64EncodedString()
        
        let dictData = ["mobile" : self.editProfileView?.userMobileTextField.text ?? "",
                        "fullname" : self.editProfileView?.userNameTextField.text ?? "",
                        "email" : self.editProfileView?.userEmailTextField.text ?? "",
                        "image" : imgDataObj ?? ""]
        
        APIManager.shared.fetchRequestData(strURL: Constants.PUT_UPDATE_USER_PROFILE, bIsPostRequest: false, bIsShowAlert: true, parameter: dictData) { [weak self] (responseData) in
            
            if let _ = responseData{
                self?.userInfo.mobile   = self?.editProfileView?.userMobileTextField.text ?? ""
                self?.userInfo.fullname = self?.editProfileView?.userNameTextField.text ?? ""
                self?.userInfo.email    = self?.editProfileView?.userEmailTextField.text ?? ""
                self?.editProfileView?.removeFromSuperview()
                self?.dismissView()
                
                Constants.setValueInUserDefault(value: self?.userInfo.email , key: Constants.KEY_USER_EMAIL)
                Constants.setValueInUserDefault(value: self?.userInfo.mobile , key: Constants.KEY_USER_MOBILE)
                Constants.setValueInUserDefault(value: self?.userInfo.fullname ?? "", key: Constants.KEY_USER_NAME)
            }
            self?.hideLoader()
        }
    }
    
//MARK:- ############## SHOW PROFILE DETAILS ##############

    func showEditDetailsView(){
        
        editProfileView = Bundle.main.loadNibNamed("editProfileView", owner: self, options: nil)?.first as? EditProfileView
        
        blackView.backgroundColor = UIColor.black
        blackView.alpha = 0
        blackView.isUserInteractionEnabled = true
        editProfileView?.userNameTextField.text = self.userInfo.fullname ?? ""
        editProfileView?.userEmailTextField.text = self.userInfo.email
        editProfileView?.userMobileTextField.text = self.userInfo.mobile
        
        editProfileView?.updateButton.layer.cornerRadius = 9.0
        editProfileView?.mainView.layer.cornerRadius = 10.0
        editProfileView?.userProfilePictureImageView.layer.cornerRadius = (editProfileView?.userProfilePictureImageView.frame.height)!/2
        editProfileView?.userNameTextField.delegate = self
        editProfileView?.userEmailTextField.delegate = self
        editProfileView?.userMobileTextField.delegate = self
        editProfileView?.delegate = self
        editProfileView?.dismissKeyboard()
        editProfileView?.updateButton.setTitle("Update".localized(), for: .normal)
        editProfileView?.detailsLabel.text = "Details".localized()
        if let mainView = view.superview?.superview?.superview?.superview?.superview {
            
            if (mainView.frame.height < 600){
                editProfileView?.viewHeight.constant = 600
                
            }
            else {
                editProfileView?.viewHeight.constant = mainView.frame.height
            }
            view.layoutIfNeeded()
            mainView.addSubview(blackView)
            mainView.addSubview(editProfileView!)
            
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(dismissViewGesture(gesture:)))
            tapGesture.numberOfTapsRequired = 1
            editProfileView?.addGestureRecognizer(tapGesture)
            
            //            let window = UIApplication.shared.keyWindow
            blackView.frame = CGRect(x: 0, y: self.view.frame.height, width: view.frame.width, height: mainView.frame.height)
            editProfileView?.frame = CGRect(x: 0, y: self.view.frame.height, width: view.frame.width, height: mainView.frame.height)
            
            self.blackView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: mainView.frame.height)
            self.blackView.alpha = 0.8
            
            UIView.animate(withDuration: 0.4, animations: {
                self.tabBarController?.tabBar.isHidden = true
                self.editProfileView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: mainView.frame.height)
            }, completion: { finished in
            })
        }
    }
}

extension ProfileViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Constants.bIsUserLoggedIn() ? 6 : 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ShamilOptionTableViewCell
        cell.mainView.layer.cornerRadius = 10.0
        if Constants.bIsUserLoggedIn(){
            cell.optionNameLabel.text = titles[indexPath.row].localized()
        }else{
            cell.optionNameLabel.text = titlesGuest[indexPath.row].localized()
        }
                
        if Localize.currentLanguage() == "ar"{
            cell.imgGoIcon.image = #imageLiteral(resourceName: "goIcon1")

        }else{
            cell.imgGoIcon.image = #imageLiteral(resourceName: "goIcon")
        }
        
        
        if (indexPath.row < descriptions.count){
            if Constants.bIsUserLoggedIn(){
                cell.optionDescriptionLabel.text = descriptions[indexPath.row].localized()
            }else{
                cell.optionDescriptionLabel.text = descriptionsGuest[indexPath.row].localized()
            }
        }
        else {
            cell.optionNameLabel.textAlignment = .center
            cell.optionNameLabel.textColor = .red
        }
        
        //Vishal
        if Constants.bIsUserLoggedIn() {
            if self.titles.count-1 == indexPath.row {
                cell.imgGoIcon.isHidden = true
                cell.optionNameLabel.textAlignment = .center
                cell.optionNameLabel.textColor = .lightGray
                cell.optionNameLabel.font = cell.optionNameLabel.font.withSize(14)
                cell.mainView.backgroundColor = UIColor.clear
                
                if let title = cell.optionNameLabel.text{
                   cell.optionNameLabel.attributedText = title.getUnderLineAttributedText()
                }
            }
        }
        else {
            if self.titlesGuest.count-1 == indexPath.row {
                cell.imgGoIcon.isHidden = true
                cell.optionNameLabel.textAlignment = .center
                cell.optionNameLabel.textColor = .lightGray
                cell.optionNameLabel.font = cell.optionNameLabel.font.withSize(14)
                cell.mainView.backgroundColor = UIColor.clear
                
                if let title = cell.optionNameLabel.text{
                   cell.optionNameLabel.attributedText = title.getUnderLineAttributedText()
                }
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 5 || indexPath.row == 4){
            return Constants.bIsUserLoggedIn() ? 60.0 : 0.0
        }
        else {
            return 100.0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        isProtectionWashCar = false
        print("Clicked")
        if Constants.bIsUserLoggedIn() == false{
            if indexPath.row == titlesGuest.count - 2{
                Constants.APP_DELEGATE.setLogInRoot()
                return
            }
            else if indexPath.row == titlesGuest.count - 1 {
                self.performSegue(withIdentifier: self.seguesGuest[indexPath.row-1], sender: self)
            }
            else {
                self.performSegue(withIdentifier: self.seguesGuest[indexPath.row], sender: self)
            }
            
            return
        }
        
        if indexPath.row == 0 || indexPath.row == 1 {
            self.checkLoginStatus { (isLoggedIn) in
                if isLoggedIn {
                    if indexPath.row == 0 {
                        self.showEditDetailsView()
                    }else{
                        self.performSegue(withIdentifier: self.segues[0], sender: self)
                    }
                }
            }
        }else if indexPath.row == 4 {
            if Constants.bIsUserLoggedIn() == false{
                return
            }
            userLogout()
        } else if indexPath.row == 5 {  //Vishal
            self.performSegue(withIdentifier: self.segues[indexPath.row-2], sender: self)
        }
        else {
            self.performSegue(withIdentifier: self.segues[indexPath.row-1], sender: self)
        }
    }
    
    private func userLogout(){
            
        showLoader()        
        APIManager.shared.fetchRequestData(strURL: Constants.POST_USER_LOGOUT , bIsPostRequest: true, bIsShowAlert: true, parameter: ["player_id": Constants.getValueFromUserDefault(key: Constants.KEY_PLAYER_ID) ?? ""]) { [weak self] (responseData) in
            if let _ = responseData{
                self?.clearUserData()
            }
            self?.hideLoader()
        }
        
    }
    
    private func clearUserData(){
       Constants.setValueInUserDefault(value: "", key: Constants.KEY_USER_TOKEN)
       Constants.setValueInUserDefault(value: false, key: Constants.KEY_USER_LOGIN)
       Constants.setValueInUserDefault(value: "" , key: Constants.KEY_USER_EMAIL)
       Constants.setValueInUserDefault(value: "" , key: Constants.KEY_USER_MOBILE)
       Constants.setValueInUserDefault(value: "", key: Constants.KEY_USER_NAME)
       Constants.setValueInUserDefault(value: "", key: Constants.KEY_USER_IMAGE)
       Constants.setValueInUserDefault(value: 0 , key: Constants.KEY_USER_ID)
       Constants.setValueInUserDefault(value: "" , key: Constants.KEY_PLAYER_ID)
       Constants.APP_DELEGATE.configureViewAfterLogout()
    }
    
}

extension ProfileViewController: EditProfilePictureDelegate  {

    func updateProfile() {
        self.handleProfileValidation()
    }
    
    func addProfilePicture() {
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            
        }
        actionSheet.addAction(cancelActionButton)
        
        let saveActionButton = UIAlertAction(title: "Camera", style: .default)
        { _ in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                ProfileViewController.dismiss = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(saveActionButton)
        
        let deleteActionButton = UIAlertAction(title: "Gallery", style: .default)
        { _ in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .photoLibrary
                self.imagePicker.allowsEditing = false
                ProfileViewController.dismiss = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }
        actionSheet.addAction(deleteActionButton)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    
    func dismissView() {
        
        blackView.removeFromSuperview()
        self.tabBarController?.tabBar.isHidden = false
    }
}

extension ProfileViewController : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
}

extension ProfileViewController:  UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //        guard let selectedImage = info[.originalImage] as? UIImage else {
        //            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        //        }
        
        picker.dismiss(animated: true, completion: nil)
        //        optionsTableView.reloadData()
    }
    
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!){
        self.dismiss(animated: true, completion: { () -> Void in
            
            //            self.optionImages[self.index] = image
        })
        //        optionsTableView.reloadData()
    }
}


extension String {
    func getUnderLineAttributedText() -> NSAttributedString {
        return NSMutableAttributedString(string: self, attributes: [.underlineStyle: NSUnderlineStyle.single.rawValue])
    }
}
