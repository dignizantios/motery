//
//  ViewPhotosViewController.swift
//  Sayarti
//
//  Created by Mina Malak on 3/24/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Alamofire

class ViewPhotosViewController: UIViewController {
    
    var firstTime: Bool = true
    var photos = [UIImage]()
    var index: Int = 0
    var images = [String]()
    var bIsUrls = false
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func dismissViewAction(_ sender: UIButton) {
        
        dismiss(animated: true, completion: nil)
    }
}

extension ViewPhotosViewController: UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.bIsUrls{
            return images.count
        }
        return photos.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! ViewPhotoCollectionViewCell
        if self.bIsUrls{
//            if let strURL = URL.init(string: self.images[indexPath.row]){
//               cell.photoImageView.af_setImage(withURL: strURL)
//            }
            
            cell.photoImageView.setImageForCar(withUrl: self.images[indexPath.row])
            
        }else{
            cell.image = photos[indexPath.row]
        }
        
        if (firstTime){
            firstTime = false
            photosCollectionView.scrollToItem(at: IndexPath(item: index, section: 0), at: UICollectionView.ScrollPosition.centeredHorizontally, animated: false)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
       // let cel = cell as! ViewPhotoCollectionViewCell
       // cel.image = photos[indexPath.row]
       // print(indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //        let collectionViewWidth = collectionView.bounds.width
        return CGSize(width: collectionView.bounds.width, height: collectionView.bounds.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        return 0
    }
    
    //    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    //
    //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photoCell", for: indexPath) as! ViewPhotoCollectionViewCell
    //        cell.mainScrollView.contentSize = super.view.frame.size
    //    }
}
