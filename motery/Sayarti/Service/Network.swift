//
//  Network.swift
//  Elia
//
//  Created by Mina Mounir on 7/20/19.
//  Copyright © 2019 Mina. All rights reserved.
//

import UIKit


import UIKit

enum HTTPMethodObj: String{
    case get = "GET"
    case post = "POST"
    case delete = "DELETE"
}

enum ServiceError: Error {
    
    case noInternetConnection
    case missedData
    case custom(String)
    case other
}

extension ServiceError: LocalizedError {
    
    var errorDescription: String? {
        switch self {
        case .noInternetConnection:
            return "No Internet connection".localized()
        case .missedData:
            return "Please, complete your data".localized()
        case .other:
            return "Something went wrong. Please try again!".localized()
        case .custom(let message):
            return message
        }
    }
}

class Network{
    
    static let shared = Network()
    
    func makeHttpRequest<T: Decodable>(model: T,method: HTTPMethodObj, APIName: String, parameters: [String: Any]?, completion: @escaping (Result<T, ServiceError>)->()) {
        
        var request : URLRequest!
        
        print("URL: ", APIName)
        if let parameters = parameters {
            
            switch method{
            case .post, .delete:
                
                request = URLRequest(url: URL(string: Constants.BASE_URL + APIName)!)
                
                //                var dataString = ""
                //                for (key, value) in parameters{
                //                    dataString.append("\(key)=\(value)&")
                //                }
                
                
                if (APIName.contains("bookCarResellerOrder") || APIName.contains("requestCall")){
                    
                    var  jsonData = NSData()
                    
                    // var dataString2 :String = ""
                    do {
                        jsonData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) as NSData
                        // you can now cast it with the right type
                    } catch {
                        print(error.localizedDescription)
                    }
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.addValue(Constants.headers["access-token"]!, forHTTPHeaderField: "Authorization")
                    request.httpBody = jsonData as Data
                }
                    
                else {
                    let postString = getPostString(params: parameters)
                    request.httpBody = postString.data(using: .utf8)
                }
                
                
            case .get:
                var components = URLComponents(string: Constants.BASE_URL + APIName)
                for item in parameters {
                    
                    components?.queryItems = [
                        URLQueryItem(name: item.key, value: "\(item.value)")
                    ]
                    request = URLRequest(url: components!.url!)
                }
            }
        }
        else {
            
            // replace spaces
            let apiName = APIName.replacingOccurrences(of: " ", with: "%20")
            if let url = URL(string: Constants.BASE_URL + apiName){
                request = URLRequest(url: url)
            }
            else {
                let safeUrl = APIName.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? ""
                if let url = URL(string: Constants.BASE_URL + safeUrl) {
                    request = URLRequest(url: url)
                }
            }
        }
        
        request.httpMethod = method.rawValue
        request.addValue(Constants.getCurrentLanguage(), forHTTPHeaderField: "Accept-Language")
        
        print("CURRENT LANGUAGE :",Constants.getCurrentLanguage() )
        
        //        request.addValue(Constants.headers["Content-Type"]!, forHTTPHeaderField: "Content-Type")
        //        request.addValue(Constants.headers["App-Version"]!, forHTTPHeaderField: "App-Version")
        
        //        if (APIName.contains("getpolls?page")){
        //            request.addValue(Constants.headers["User-Country"]!, forHTTPHeaderField: "User-Country")
        //        }
        //
        //        if (APIName.contains("users/login") || APIName.contains("users/logout") || APIName.contains("users/register")){
        //
        //            request.addValue(UserDefaults.standard.string(forKey: "PlayerId") ?? "", forHTTPHeaderField: "OneSignal-Player-ID")
        //        }
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            //            let str = String(bytes: data!, encoding: .utf8)
            //            print(str)
            
            if let response = response as? HTTPURLResponse{
                if ( response.statusCode == 200 || response.statusCode == 201 ) {
                    if let data = data {
                        do {
                            let model = try JSONDecoder().decode(T.self, from: data)
                            print("Data: ", model)
                            completion(.success(model))
                        }
                        catch {
                            print(error)
                            completion(.failure(ServiceError.custom(error.localizedDescription)))
                        }
                    }
                }
                else{
                    if let data = data{
                        do {
                            if let jsonResponce = try  JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any]{
                                
                                if let errorMessage = jsonResponce["message"] as? String{
                                    completion(.failure(ServiceError.custom(errorMessage)))
                                }else if let errorMessage = jsonResponce["error"] as? String{
                                    completion(.failure(ServiceError.custom(errorMessage)))
                                }else {
                                    completion(.failure(ServiceError.custom("error please try again later")))
                                }
                            }
                        }
                        catch {
                            completion(.failure(ServiceError.custom(error.localizedDescription)))
                        }
                    }
                }
            }
            else {
                completion(.failure(ServiceError.noInternetConnection))
            }
            }.resume()
    }
    
    func getPostString(params:[String:Any]) -> String
    {
        var data = [String]()
        for(key, value) in params
        {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
}
