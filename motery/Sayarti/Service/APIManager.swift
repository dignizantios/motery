//
//  APIManager.swift
//  Sayarti
//
//  Created by Vasim Ajmeri on 19/09/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import Foundation
import UIKit



import Alamofire

class APIManager: NSObject {
    
    static let shared = APIManager()
    let DEFAULT_ERROR_MESSAGE = "Something went wrong. Please try again!".localized()
    let DEFAULT_ERROR_TITLE   = "Error".localized()


//MARK:- ############## FETCH POST URL REQUESTS DATA ###############
    
    func fetchRequestData(strURL: String, bIsPostRequest: Bool, bIsShowAlert: Bool, parameter : [String: Any], completionHandler : @escaping (_ responseResult: Data?) -> Void){
        
        //BASIC HEADERS
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept-Language": Constants.getCurrentLanguage(),
            "Authorization": Constants.getUserToken()
        ]
//        print("Header: \(headers)")
        //REQUEST URL
        
        print("URL : \(URL.init(string: strURL))")
        
        guard let fetchURL = URL.init(string: strURL) else {return}
        
        //REQUEST TYPE
        var requestType    = bIsPostRequest ? HTTPMethod.post : HTTPMethod.get
        
        if strURL == Constants.PUT_UPDATE_USER_PROFILE { requestType = HTTPMethod.put }
        
        let encoding       = JSONEncoding.default
        
        //parameters
        var parameters : [String : Any]? = nil
        
        if(bIsPostRequest || strURL == Constants.PUT_UPDATE_USER_PROFILE){
            parameters = parameter
        }
        
//        print("paramter : \(parameters)")
        
        //REQUEST
        Alamofire.request(fetchURL,method:requestType ,parameters:parameters,encoding:encoding,headers: headers)
         .responseString { response in

            switch response.result {
                case .success:
                    print("Successful")
                    do{
                        if let responseData = response.data{
                            //Json decode
                            print("fetch URL :\(fetchURL)")
                            print("response Data : \(response.value)")
                            
                            let decoded = try JSONSerialization.jsonObject(with: responseData, options: [])
                            if let decoded = decoded as? [String: Any] {
                                if let strError = decoded["error"] as? String{
                                    if(bIsShowAlert){
                                        self.showAlert(title: self.DEFAULT_ERROR_TITLE, message: strError)
                                    }
                                    completionHandler(nil)
                                    return
                                }
                            }

                            //Success data response
                            completionHandler(responseData)

                        }else{
                            if(bIsShowAlert){
                                self.showAlert(title: self.DEFAULT_ERROR_TITLE, message: self.DEFAULT_ERROR_MESSAGE)
                            }
                            completionHandler(nil)
                        }
                    }catch{
                        if(bIsShowAlert){
                            self.showAlert(title: self.DEFAULT_ERROR_TITLE, message: self.DEFAULT_ERROR_MESSAGE)
                        }
                        completionHandler(nil)
                    }


                case let .failure(error):
                    self.showAlert(title: self.DEFAULT_ERROR_TITLE, message: error.localizedDescription)
                    print("Error : ", error.localizedDescription )
                    completionHandler(nil)
            }
        }
    }//end
    
    //MARK:- SHOW ALERT
    private func showAlert(title: String, message: String){
        if let currentVC = UIApplication.getTopViewController() {
            currentVC.showDefaultAlert(title: title, message: message)
        }
    }
    
}
