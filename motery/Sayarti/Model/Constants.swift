//
//  Constants.swift
//  Sayarti
//
//  Created by Mina Malak on 3/4/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class Constants {
    
    static var headers = ["Content-Type": "application/json",
                          "Accept-Language": getCurrentLanguage(),
                          "access-token": getUserToken()]
    
    static var appColor = UIColor(red: 61.0/255.0, green: 86.0/255.0, blue: 245/255.0, alpha: 1.0)
    static var grayColor = UIColor(red: 247.0/255.0, green: 247.0/255.0, blue: 249.0/255.0, alpha: 1.0)
    static var lightGray = UIColor(red: 198.0/255.0, green: 198.0/255.0, blue: 198.0/255.0, alpha: 1.0)
    
    //APP_DELEGATE
    static var APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate
    
    //LOCAL KEYS
    static var KEY_USER_TOKEN  = "keyUserAuthToken"
    static var KEY_USER_LOGIN  = "keyUserIsLoggedIn"
    static var KEY_USER_ID     = "keyUserLoginId"
    static var KEY_USER_IMAGE  = "keyUserProfileImage"
    static var KEY_USER_NAME   = "keyUserFullName"
    static var KEY_USER_EMAIL  = "keyUserEmailId"
    static var KEY_USER_MOBILE = "keyUserMobileNumber"
    static var KEY_LAUNGUAGE   = "keyCurrentLanguage"
    static var KEY_PLAYER_ID   = "keyPushPlayerId"
    static var KEY_APP_LAUNCH  = "keyFirstAppLaunch"

    
    //ONE SIGNAL
    static var ONE_SIGNAL_KEY = "034b52ef-e6de-4f13-864c-c170da76c3cd"
    
    //BASE URL
//    static var BASE_URL   = "http://173.231.196.229/~tvavisa/motery/api/"
//    static var BASE_URL = "http://motery.dignizantapps.com/api/" //Test
    static var BASE_URL   = "https://moteryapp.com/motery/api/" //Live

    static var GET_CAR_TYPES           = BASE_URL + "user/getCarTypes"
    static var GET_USER_PROFILE        = BASE_URL + "user/getProfile"
    static var PUT_UPDATE_USER_PROFILE = BASE_URL + "user/updateProfile"
    
    //ALL URLS
    static var GET_ALL_WASH_STORE      = BASE_URL + "user/getCarWashStores"
    static var GET_WASH_STORE_DETAILS  = BASE_URL + "user/getStoreDetails/"
    static var GET_WASH_STORE_SERVICES = BASE_URL + "user/getCashWashServices/"
    static var GET_WASH_PACKAGES       = BASE_URL + "user/getCarWashPackages/"
    static var GET_WASH_FAVORITES      = BASE_URL + "user/washMarkFavorite"
    static var GET_WASH_ATTACHMENTS    = BASE_URL + "user/getCarWashAttachments"
    static var POST_WASH_ADD_ORDER     = BASE_URL + "user/addCarWashOrder"
    
    //CAR RENT
    static var GET_ALL_RENT_COMPANIES  = BASE_URL + "user/getRentComp"
    static var GET_RENT_CARS           = BASE_URL + "user/getRentCars/"
    static var GET_RENT_CAR_DETAIL     = BASE_URL + "user/getCarDetails/"
    static var POST_RENT_FAVORITES     = BASE_URL + "user/rentMarkFavorite"
    static var POST_RENT_CALL          = BASE_URL + "user/rentRequestCall"
    static var GET_RENT_ATTACHMENTS    = BASE_URL + "user/getCarRentAttachments"
    static var POST_RENT_ORDER         = BASE_URL + "user/addRentOrder"

    //USED CARS
    static var GET_USED_CAR_COMPANIES  = BASE_URL + "user/getUsedCarCompanies"
    static var GET_USED_CARS           = BASE_URL + "user/getUsedCars/"
    static var POST_USED_FAVORITES     = BASE_URL + "user/UsedCarMarkFavorite"
    static var POST_USED_CALL          = BASE_URL + "user/UsedCarRequestCall"
    static var GET_USED_CAR_DETAIL     = BASE_URL + "user/getUsedCarDetails/"
    static var GET_USED_ATTACHMENTS    = BASE_URL + "user/getUsedCarAttachments"
    static var POST_USED_ORDER         = BASE_URL + "user/addUsedCarOrder"
    //MARK:- Change Jaydeep
    static var POST_REQUEST_CALL       = BASE_URL + "user/requestCall"
    static var POST_WASH_REQUEST_CALL  = BASE_URL + "user/washRequestCall"
    
    //CAR RESELLER
    static var GET_RESELLER_COMPANIES  = BASE_URL + "user/getCarResellerCompanies"
    static var GET_RESELLER_DETAILS    = BASE_URL + "user/getCompanyDetailsById/"
    static var GET_RESELLER_CAR        = BASE_URL + "user/getCarById/"
    static var GET_RESELLER_CAR_IMAGES = BASE_URL + "user/getCarImagesById/"
    static var GET_RESELLER_ATTACHMENT = BASE_URL + "user/getAttachmentsByServiceId"
    static var POST_RESELLER_ORDER     = BASE_URL + "user/bookCarResellerOrder"
    static var GET_RESELLER_FAVORITES  = BASE_URL + "user/markResellerCompanyFavorite/"

    
    //INSURANCE
    static var GET_INSURANCE_COMPANIES     = BASE_URL + "user/getInsuranceCompanies"
    static var GET_INSURANCE_TYPES         = BASE_URL + "user/getInsuranceTypes"
    static var GET_INSURANCE_PACKAGES      = BASE_URL + "user/getInsCompPackages/"
    static var GET_INSURANCE_ADD_SERVICES  = BASE_URL + "user/getAddServices/"
    static var GET_INSURANCE_DETAILS       = BASE_URL + "user/getInsComp/"
    static var GET_INSURANCE_FAVORITES     = BASE_URL + "user/insuranceCompanyMarkFavorite"
    static var GET_INSURANCE_CALL          = BASE_URL + "user/insuranceRequestCall"
    static var GET_INSURANCE_ATTACHMENT    = BASE_URL + "user/getInsuranceAttachments/"
    static var POST_INSURANCE_ORDER        = BASE_URL + "user/addInsOrder"
    
    //FAVORITES LIST
    static var GET_FAVORITES_LIST      = BASE_URL + "user/getFavorites"

    //ORDER LIST
    static var GET_ORDER_LIST          = BASE_URL + "user/getMyOrders"
    static var GET_ORDER_DETAILS       = BASE_URL + "user/getOrderDetails"

    
    //PAYMENT
    static var GET_PAYMENT_ORDER       = BASE_URL + "user/payOrder/"
    
    //TERMS AND CONDITION
    static var GET_T_AND_C             = BASE_URL + "user/getTermsAndConditions"
    static var GET_CONTACT_NUMBER      = BASE_URL + "user/getContactNumber"
    static var GET_EMAIL               = BASE_URL + "user/getEmail"
    
    static var GET_COUNTRIES           = BASE_URL + "user/getCountries"


    //LOGOUT
    static var POST_USER_LOGOUT        = BASE_URL + "user/logout"

    //Get Detail
    static var GET_INSTANCE_PACKAGE_DETAIL = BASE_URL + "user/getInsurancePackageDetails/"
    
    //Apply Coupn
    static var POST_CAR_WASH           = BASE_URL + "user/applyCouponcode"
    static var POST_CAR_INSURANCE      = BASE_URL + "user/applyInsCouponcode"
    
    static var CONST_TOKEN  = ""
    
    
    //MARK:- GET CURRENT USER TOKEN
    static func getUserToken() -> String{
        if let strToken = self.getValueFromUserDefault(key: self.KEY_USER_TOKEN) as? String{
            return "Bearer \(strToken)"
        }
        return ""
    }
    
    static func getCurrentLanguage() -> String{
        if let strLanguage = self.getValueFromUserDefault(key: self.KEY_LAUNGUAGE) as? String{
            return strLanguage
        }
        return "en"
    }
    
    static func bIsUserLoggedIn() -> Bool{
        if let bIsLogin = self.getValueFromUserDefault(key: self.KEY_USER_LOGIN) as? Bool{
            return bIsLogin
        }
        return false
    }
    
    static func bIsAppLaunched() -> Bool{
        if let bIsLogin = self.getValueFromUserDefault(key: self.KEY_APP_LAUNCH) as? Bool{
            return bIsLogin
        }
        return false
    }
    
    static func setValueInUserDefault(value: Any?, key: String){
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    static func getValueFromUserDefault(key: String) -> Any?{
       return UserDefaults.standard.value(forKey: key)
    }
    
    
}
