//
//  Category.swift
//  Sayarti
//
//  Created by Omar mohammed on 3/21/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit
import Foundation

struct User: Codable {
    var mobile: String = ""
    var email: String = ""
    var fullname: String?
    var profile_image: String?
    var id: Int = 0
}

struct LoginResponse: Codable {
    var message = ""
    var access_token = ""
    var token_type = ""
    var user = User()
}

struct ForgotPasswordResponse: Codable {
    var message = ""
}

struct Offer: Codable {
    
    var name : String?
    var store_id : Int? 
    var company_id : Int? 
    var agency_id : Int?
    var image : String?
    var percentage : Int?
    var start_date : String?
    var end_date : String?
    var module_type : Int?
}

struct HomeCategory: Codable {
    var id = 0
    var image = ""
    var name = ""
}



struct Ad: Codable {
    var id: Int = 0
    var name = ""
    var image = ""
    var description = ""
    var start_date = ""
    var end_date = ""
}



struct Company{
    var logo: UIImage = UIImage()
    var cover: UIImage = UIImage()
    var agentImage: UIImage = UIImage()
    var name: String = ""
    var description: String = ""
    var categories: [Category] = []
}

struct Category {
    var name: String = ""
    var cars: [Car] = []
    var rentCars: [RentCar] = []
    var resellerCars : [ResellerCar] = []
}

struct Car {
    var name: String = ""
    var description: String = ""
    var image: UIImage = UIImage()
    var photos: [UIImage] = []
    
}

struct CarDetails: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var image: String?
}

struct Companies : Codable{
    var id:    Int?
    var desc:  String?
    var image: String?
    var name:  String?
}

struct Store : Codable{
    var id          : Int?
    var desc        : String?
    var image       : String?
    var name        : String?
    var contact     : String?
    var location    : String?
    var is_favorite : Bool?
    var detail_category : [ResellerDetailCategory]?
}

struct StoreImage : Codable{
    var id        : Int?
    var store_id  : Int?
    var store_img : String?
}


struct CompanyDetail : Codable{
    var store      : Store?
    var store_imgs : [StoreImage]?
}

struct Service : Codable{
    var id            : Int?
    var store_id      : Int?
    var price         : Double?
    var name          : String?
    var desc          : String?
    var selectedService: Int?
}

struct Package : Codable{
    var id           : Int?
    var store_id     : Int?
    var price        : Double?
    var name         : String?
    var desc         : String?
    var selectedService: Int?
}


struct Attachment : Codable{
    var id          : Int?
    var name        : String?
    var description : String?
    var imgData     : String?
}

struct RentCompany : Codable{
    var company_id    : Int?
    var company_name  : String?
    var company_img   : String?
    var agencies      : [Agency]?
    var continent_id : Int?
}

struct Agency : Codable{
    var agency_id    : Int?
    var agency_name  : String?
    var agency_img   : String?
}


struct CarRentDesc : Codable{
    var company    : RentCompanyInfo?
    var agency     : RentAgency?
    var cars       : [RentCar]?
}

struct RentAgency : Codable{
    var id            : Int?
    var description   : String?
    var contact       : String?
    var image         : String?
    var latitude      : String?
    var longitude     : String?
    var status        : Int?
    var favorite      : Bool?
    var name          : String?
}

struct RentCompanyInfo : Codable{
    var id             : Int?
    var desc           : String?
    var image          : String?
    var name           : String?
}


struct RentCar : Codable{
    var id        : Int?
    var car_img   : String?
    var car_desc  : String?
    var name      : String?
}


struct RentCarDetails : Codable{
    var car           : RentCarDetail?
    var car_imgs      : [CarImgs]?
}

struct CarImgs: Codable {
    var id        : Int?
    var car_img   : String?
    var car_id    : Int?
}

struct RentCarDetail : Codable{
    var id                      : Int?
    var rent_comp_agency_id     : Int?
    var used_car_comp_agency_id : Int?
    var price                   : Double?
    var car_img                 : String?
    var seats                   : Int?
    var automatic               : String?
    var power                   : Double?
    var max_speed               : Double?
    var name                    : String?
    var car_desc                : String?
    var detail_category : [ResellerDetailCategory]?
}


struct OrderPayment: Codable {
    var message          : String?
    var payment_methods  : [PaymentMethod]?
}

struct PaymentMethod : Codable {
    var PaymentMethodName : String?
    var PaymentMethodUrl  : String?
    var PaymentMethodCode : String?
}

struct ResellerCompany : Codable {
    var id           : Int?
    var continent_id : Int?
    var image        : String?
    var status       : Int?
    var agency       : ResellerAgency?
    var name         : String?
    var description  : String?
}

struct ResellerAgency : Codable {
    var id             : Int?
    var car_company_id : Int?
    var contact        : String?
    var latitude       : String?
    var longtitude     : String?
    var image          : String?
    var name           : String?
    var description    : String?
    var favorite       : Bool?
    var location       : String?
}

struct ResellerDesc : Codable{
    var company     : ResellerCompany?
    var agency      : ResellerAgency?
    var categories  : [ResellerCategories]?
}

struct ResellerCarImgs: Codable {
    var id      : Int?
    var image   : String?
}

struct ResellerCategories : Codable{
    var category_name : String?
    var cars          : [ResellerCar]?
}

struct ResellerCar: Codable {
    var id             : Int?
    var name           : String?
    var description    : String?
    var image          : String?
    var car_company_id : Int?
    var price          : Double?
    var automatic      : Int?
    var seats          : Int?
    var power          : Int?
    var max_speed      : Int?
    var year           : Int?
    var services       : [ResellerService]?
    var colors         : [ResellerColor]?
    var detail_category : [ResellerDetailCategory]?
    
}

struct ResellerService : Codable{
    var id              : Int?
    var name            : String?
    var has_attachments : Bool?
    var price           : Double?
    var image           : String?
    var description     : String?
    var selectedService : Int?
}

struct ResellerDetailCategory : Codable {
    var id                      : Int?
    var reseller_car_id         : Int?
    var detail_category_id      : Int?
    var detail_description      : String?
    var category_name           : String?
    var isSelected              : Int?
    var rent_car_id             : Int?
    var used_car_id             : Int?
    var wash_car_id             : Int?
    var insurance_package_id    : Int?
    
}

struct ResellerColor : Codable{
    var id              : Int?
    var name            : String?
    var code            : String?
    var image           : String?
}

struct ResellerImage : Codable{
    var id           : Int?
    var image        : String?
}


struct InsuranceCompany : Codable {
    var ins_comp_id : Int?
    var contact     : String?
    var latitude    : String?
    var longtitude  : String?
    var image       : String?
    var name        : String?
    var description : String?
    var favorite    : Bool?
}


struct InsuranceType : Codable{
    var id           : Int?
    var name         : String?
}

struct InsuranceShamil     : Codable{
    var ins_pack_id        : Int?
    var ins_comp_id        : Int?
    var insurance_type_id  : Int?
    var pack_title         : String?
    var pack_desc_sm       : String?
    var price              : Double?
    var pack_desc_lg       : String?
    var detail_category : [ResellerDetailCategory]?
}

struct InsuranceServices   : Codable{
    var id                 : Int?
    var ins_comp_id        : Int?
    var serv_name          : String?
    var serv_desc          : String?
    var serv_img           : String?
    var serv_charge        : Double?
}

struct CarType : Codable {
    var id           : Int?
    var type_name    : String?
    var type_img     : String?
}


struct FavCarReseller : Codable {
    var id            : Int?
    var company       : ResellerCompany?
    var agency        : ResellerAgency?
}

struct FavCarWash : Codable {
    var id        : Int?
    var store     : Store?
}

struct FavInsurance : Codable {
    var id          : Int?
    var company     : InsuranceCompany?
}

struct FavCarRent   : Codable {
    var id          : Int?
    var company_id  : Int?
    var agency_id   : Int?
    var user_id     : Int?
    var company     : RentCompanyInfo?
    var agency      : RentAgency?
}

struct Favorites      : Codable {
    var carReseller   : [FavCarReseller]?
    var carWash       : [FavCarWash]?
    var insurance     : [FavInsurance]?
    var carRent       : [FavCarRent]?
    var usedCar       : [FavCarRent]?
}

struct UserOrder   : Codable {
    
    var carWash      : [CarWashOrder]?
    var carReseller  : [CarResellerOrder]?
    var carInsurance : [CarInsuranceOrder]?
    var carRent      : [CarRentOrder]?
    var usedCar      : [UsedCarOrder]?
    var date         : String?
    
}

struct CarWashOrder      : Codable {
    var id               : Int?
    var total_amount     : Double?
    var booking_date     : String?
    var car_type         : CarType?
    var car_wash_store   : Store?
    var car_wash_service : [Service]?
    var user             : User?
    var date             : String?
    var service          : String?
    var payment_type     : String?
    var coupon_code = ""
    var discount = ""
    var discount_amount = 0.0
}

struct CarResellerOrder  : Codable {
    var id               : Int?
    var total_amount     : Double?
    var extra_notes      : String?
    var car              : CarDetails?
    var service          : [Service]?
    var agency           : RentAgency?
    var user             : User?
    var date             : String?
    var service_name     : String?
    var payment_type     : String?
    
}

struct CarInsuranceOrder  : Codable {
    var insurance_id      : Int?
    var total_amount     : Double?
    var insurance_type    : InsuranceType?
    var company           : InsuranceCompany?
    var car_type          : CarType?
    var insurance_package : InsuranceShamil?
    var user              : User?
    var insurance_company_additional_service : [InsuranceServices]?
    var date             : String?
    var service_name     : String?
    var payment_type     : String?
    var coupon_code = ""
    var discount = ""
    var discount_amount = 0.0
}

struct CarRentOrder  : Codable {
    var id               : Int?
    var total_amount     : Double?
    var extra_notes      : String?
    var car              : RentCarDetail?
    var user             : User?
    var agency           : RentAgency?
    var company          : RentCompanyInfo?
    var date             : String?
    var payment_type     : String?
}

struct UsedCarOrder  : Codable {
    var id               : Int?
    var total_amount     : Double?
    var extra_notes      : String?
    var car              : UsedCarDetails?
    var user             : User?
    var date             : String?
    var payment_type     : String?
}

struct UsedCarDetails: Codable {
    var name      : String?
    var agency    : String?
    var comapny   : String?
    var car_img   : String?
    var price     : Double?
}


class Continent: Codable {
    var id = 0
    var name = ""
    //TODO: - Yash changes
    var selectedIndex : Int? = 0
}


struct TermsNConditions: Codable {
    var terms_and_conditions  : String?
}

struct ContactEmail: Codable {
    var email  : String?
}

struct ContactNumber: Codable {
    var contact  : String?
}


struct Countries: Codable{
    var id               : Int?
    var country_code     : Int?
    var name             : String?
}
