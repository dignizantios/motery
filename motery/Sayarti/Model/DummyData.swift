//
//  DummyData.swift
//  Sayarti
//
//  Created by Omar mohammed on 3/21/19.
//  Copyright © 2019 Mina Malak. All rights reserved.
//

import UIKit

class DummyData{
    
    static var shared = DummyData()
    
    func AustonMartin() -> Company{
        
        return Company.init(logo: #imageLiteral(resourceName: "Aston martin logo"),
                     cover: #imageLiteral(resourceName: "Aston Martin"),
                     agentImage: #imageLiteral(resourceName: "Premier Internationa"),
                     name: "Aston Martin",
                     description: "Aston Martin Lagonda Global Holdings plc is a British independent manufacturer of luxury sports cars and grand tourers. It was founded in 1913 by Lionel Martin and Robert Bamford. Steered from 1947 by David Brown, it became associated with expensive grand touring cars in the 1950s and 1960s, and with the fictional character James Bond following his use of a DB5 model in the 1964 film Goldfinger. Their sports cars are regarded as a British cultural icon.[3] Aston Martin has held a Royal Warrant as purveyor of motorcars to the Prince of Wales since 1982.[4] It has over 150 car dealerships in over 50 countries on six continents, making them a global automobile brand.[5] The company is traded at the London Stock Exchange and is a constituent of the FTSE 250 Index.",
                     categories: [
                        Category.init(name: "Grand Tourer", cars: [
                            Car.init(name: "VANQUISH S",
                                     description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                     image: #imageLiteral(resourceName: "Vanquish image"),
                                     photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                            Car.init(name: "DB9 GT",
                                     description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                     image: #imageLiteral(resourceName: "DB9 GT image"),
                                     photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                            
                            ], rentCars: [RentCar](), resellerCars: [ResellerCar]()),
                        Category.init(name: "V12 Engine", cars: [
                            Car.init(name: "VANQUISH S",
                                     description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                     image: #imageLiteral(resourceName: "Vanquish image"),
                                     photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                            Car.init(name: "DB9 GT",
                                     description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                     image: #imageLiteral(resourceName: "DB9 GT image"),
                                     photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                            
                            ], rentCars: [RentCar](), resellerCars: [ResellerCar]())
                       
                        
            ])
                        
        
        
    }
    
    
    func Ferrari() -> Company{
        
        return Company.init(logo: #imageLiteral(resourceName: "Ferrari-1"),
                            cover: #imageLiteral(resourceName: "Ferrari"),
                            agentImage: #imageLiteral(resourceName: "Zayani"),
                            name: "Ferrari",
                            description: "Aston Martin Lagonda Global Holdings plc is a British independent manufacturer of luxury sports cars and grand tourers. It was founded in 1913 by Lionel Martin and Robert Bamford. Steered from 1947 by David Brown, it became associated with expensive grand touring cars in the 1950s and 1960s, and with the fictional character James Bond following his use of a DB5 model in the 1964 film Goldfinger. Their sports cars are regarded as a British cultural icon.[3] Aston Martin has held a Royal Warrant as purveyor of motorcars to the Prince of Wales since 1982.[4] It has over 150 car dealerships in over 50 countries on six continents, making them a global automobile brand.[5] The company is traded at the London Stock Exchange and is a constituent of the FTSE 250 Index.",
                            categories: [
                                Category.init(name: "Grand Tourer", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]()),
                                Category.init(name: "V12 Engine", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]())
                                
                                
            ])
        
        
        
    }
    
    
    
    func Mercedes() -> Company{
        
        return Company.init(logo: #imageLiteral(resourceName: "company1"),
                            cover: #imageLiteral(resourceName: "Ferrari"),
                            agentImage: #imageLiteral(resourceName: "brand1"),
                            name: "Mercedes",
                            description: "Aston Martin Lagonda Global Holdings plc is a British independent manufacturer of luxury sports cars and grand tourers. It was founded in 1913 by Lionel Martin and Robert Bamford. Steered from 1947 by David Brown, it became associated with expensive grand touring cars in the 1950s and 1960s, and with the fictional character James Bond following his use of a DB5 model in the 1964 film Goldfinger. Their sports cars are regarded as a British cultural icon.[3] Aston Martin has held a Royal Warrant as purveyor of motorcars to the Prince of Wales since 1982.[4] It has over 150 car dealerships in over 50 countries on six continents, making them a global automobile brand.[5] The company is traded at the London Stock Exchange and is a constituent of the FTSE 250 Index.",
                            categories: [
                                Category.init(name: "Grand Tourer", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]()),
                                Category.init(name: "V12 Engine", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]())
                                
                                
            ])
        
        
        
    }
    
    func BMW() -> Company{
        
        return Company.init(logo: #imageLiteral(resourceName: "bmw_logo"),
                            cover: #imageLiteral(resourceName: "Ferrari"),
                            agentImage: #imageLiteral(resourceName: "Al-ganem"),
                            name: "BMW",
                            description: "Aston Martin Lagonda Global Holdings plc is a British independent manufacturer of luxury sports cars and grand tourers. It was founded in 1913 by Lionel Martin and Robert Bamford. Steered from 1947 by David Brown, it became associated with expensive grand touring cars in the 1950s and 1960s, and with the fictional character James Bond following his use of a DB5 model in the 1964 film Goldfinger. Their sports cars are regarded as a British cultural icon.[3] Aston Martin has held a Royal Warrant as purveyor of motorcars to the Prince of Wales since 1982.[4] It has over 150 car dealerships in over 50 countries on six continents, making them a global automobile brand.[5] The company is traded at the London Stock Exchange and is a constituent of the FTSE 250 Index.",
                            categories: [
                                Category.init(name: "Grand Tourer", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]()),
                                Category.init(name: "V12 Engine", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]())
                                
                                
            ])
        
        
        
    }
    
    
    func Jaguar() -> Company{
        
        return Company.init(logo: #imageLiteral(resourceName: "Jaguar-logo-2012-1920x1080"),
                            cover: #imageLiteral(resourceName: "Ferrari"),
                            agentImage: #imageLiteral(resourceName: "Zayani"),
                            name: "Jaguar",
                            description: "Aston Martin Lagonda Global Holdings plc is a British independent manufacturer of luxury sports cars and grand tourers. It was founded in 1913 by Lionel Martin and Robert Bamford. Steered from 1947 by David Brown, it became associated with expensive grand touring cars in the 1950s and 1960s, and with the fictional character James Bond following his use of a DB5 model in the 1964 film Goldfinger. Their sports cars are regarded as a British cultural icon.[3] Aston Martin has held a Royal Warrant as purveyor of motorcars to the Prince of Wales since 1982.[4] It has over 150 car dealerships in over 50 countries on six continents, making them a global automobile brand.[5] The company is traded at the London Stock Exchange and is a constituent of the FTSE 250 Index.",
                            categories: [
                                Category.init(name: "Grand Tourer", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]()),
                                Category.init(name: "V12 Engine", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]())
                                
                                
            ])
        
        
        
    }
    
    
    func Nissan() -> Company{
        
        return Company.init(logo: #imageLiteral(resourceName: "Nissan-logo"),
                            cover: #imageLiteral(resourceName: "Ferrari"),
                            agentImage: #imageLiteral(resourceName: "G"),
                            name: "Nissan",
                            description: "Aston Martin Lagonda Global Holdings plc is a British independent manufacturer of luxury sports cars and grand tourers. It was founded in 1913 by Lionel Martin and Robert Bamford. Steered from 1947 by David Brown, it became associated with expensive grand touring cars in the 1950s and 1960s, and with the fictional character James Bond following his use of a DB5 model in the 1964 film Goldfinger. Their sports cars are regarded as a British cultural icon.[3] Aston Martin has held a Royal Warrant as purveyor of motorcars to the Prince of Wales since 1982.[4] It has over 150 car dealerships in over 50 countries on six continents, making them a global automobile brand.[5] The company is traded at the London Stock Exchange and is a constituent of the FTSE 250 Index.",
                            categories: [
                                Category.init(name: "Grand Tourer", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]()),
                                Category.init(name: "V12 Engine", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]())
                                
                                
            ])
        
        
        
    }
    
    func Chevrolete() -> Company{
        
        return Company.init(logo: #imageLiteral(resourceName: "chevrolet-logo-png-download"),
                            cover: #imageLiteral(resourceName: "Ferrari"),
                            agentImage: #imageLiteral(resourceName: "Al-ganem"),
                            name: "Chevrolete",
                            description: "Aston Martin Lagonda Global Holdings plc is a British independent manufacturer of luxury sports cars and grand tourers. It was founded in 1913 by Lionel Martin and Robert Bamford. Steered from 1947 by David Brown, it became associated with expensive grand touring cars in the 1950s and 1960s, and with the fictional character James Bond following his use of a DB5 model in the 1964 film Goldfinger. Their sports cars are regarded as a British cultural icon.[3] Aston Martin has held a Royal Warrant as purveyor of motorcars to the Prince of Wales since 1982.[4] It has over 150 car dealerships in over 50 countries on six continents, making them a global automobile brand.[5] The company is traded at the London Stock Exchange and is a constituent of the FTSE 250 Index.",
                            categories: [
                                Category.init(name: "Grand Tourer", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]()),
                                Category.init(name: "V12 Engine", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]())
                                
                                
            ])
        
        
        
    }
    
    
    func GMC() -> Company{
        
        return Company.init(logo: #imageLiteral(resourceName: "gmc-logo"),
                            cover: #imageLiteral(resourceName: "Ferrari"),
                            agentImage: #imageLiteral(resourceName: "Behbehani"),
                            name: "GMC",
                            description: "Aston Martin Lagonda Global Holdings plc is a British independent manufacturer of luxury sports cars and grand tourers. It was founded in 1913 by Lionel Martin and Robert Bamford. Steered from 1947 by David Brown, it became associated with expensive grand touring cars in the 1950s and 1960s, and with the fictional character James Bond following his use of a DB5 model in the 1964 film Goldfinger. Their sports cars are regarded as a British cultural icon.[3] Aston Martin has held a Royal Warrant as purveyor of motorcars to the Prince of Wales since 1982.[4] It has over 150 car dealerships in over 50 countries on six continents, making them a global automobile brand.[5] The company is traded at the London Stock Exchange and is a constituent of the FTSE 250 Index.",
                            categories: [
                                Category.init(name: "Grand Tourer", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]()),
                                Category.init(name: "V12 Engine", cars: [
                                    Car.init(name: "VANQUISH S",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "Vanquish image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish2"),#imageLiteral(resourceName: "Vanquish1"),#imageLiteral(resourceName: "Vanquish3")]),
                                    Car.init(name: "DB9 GT",
                                             description: "Building on the heritage and success of our flagship Grand Tourer comes the Aston Martin Vanquish S – bringing improved engine power, enhanced dynamics and all new styling to create a car of unprecedented ability. Vanquish S is the ultimate Super GT.",
                                             image: #imageLiteral(resourceName: "DB9 GT image"),
                                             photos: [#imageLiteral(resourceName: "Vanquish3"),#imageLiteral(resourceName: "DB9 GT - 2"),#imageLiteral(resourceName: "DB9 GT - 1")])
                                    
                                    ], rentCars: [RentCar](), resellerCars: [ResellerCar]())
                                
                                
            ])
        
        
        
    }
}
